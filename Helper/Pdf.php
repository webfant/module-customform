<?php

namespace Webfant\Customform\Helper;

use Magento\MediaStorage\Model\File\Uploader;
use Magento\Framework\App\Filesystem\DirectoryList;
use Webfant\Customform\Helper\Data;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Webfant\Customform\Api\FormRepositoryInterface;
use Webfant\Customform\Api\Form\ElementRepositoryInterface;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;

class Pdf extends \Magento\Framework\App\Helper\AbstractHelper {
    
    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;

    /**
     * @var File
     */
    private $fileDriver;
    
    private $pdfContent = array();
    
    private $images = array();
    
    private $attachments = array();
    
    private $aanzichtImgDir = 'https://staging.inmeet.webfant.io/media/inmeet_images/';
    
    const OUTSIDE_LEFT  = 'outside_left.png';
    const OUTSIDE_RIGHT = 'outside_right.png';
    const OUTSIDE_FRONT = 'outside_front.png';
    
    protected $tableData = [];
        
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webfant\Customform\Model\SubmitFactory $submitFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Webfant\Customform\Helper\Data $helper,
        File $fileDriver,
        FormRepositoryInterface $formRepository,
        ElementRepositoryInterface $elementRepository,
        StoreManagerInterface $storeManager
    ) {
        
        parent::__construct($context);
        
        $this->submitFactory = $submitFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->filesystem = $filesystem;
        $this->fileDriver = $fileDriver;
        $this->_helper = $helper;
        $this->formRepository = $formRepository;
        $this->elementRepository = $elementRepository;
        $this->storeManager = $storeManager;
        $this->mediaUrl = $storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $this->aanzichtImgDir = $this->mediaUrl . 'inmeet_images/';
        
        $this->tableData[0] = [];
        $this->tableData[1] = [];
        $this->tableData[2] = [];
        
    }
    
    public function generate($id) {
        
        $this->pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->pdf->SetPrintHeader(false);
        $this->pdf->SetPrintFooter(false);
        $this->pdf->setMargins(10, 10, 10, true);
        $this->pdf->setFooterMargin(10);
        $this->pdf->SetAutoPageBreak(TRUE, 0);
        
        $this->pdf->AddPage('L', 'A4');
        
        $this->gatherPdfContent($id);
        $this->writePdfContent();
        
        $filePath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath() . '/inmeet_pdf';
        return $this->pdf;
        return $this->pdf->Output($filePath . '/test_pdf.pdf', 'I');
        
    }
    
    private function loadFormJson($formJson) {

        $formJson = json_decode($formJson);
        
        $this->elements = array();
        
        foreach($formJson as $element) {
            
            $this->elements[$element->name] = $element;
            
        }
        
    }
    
    private function writePdfContent() {
        
        // echo '<pre>'; var_dump($this->pdfContent); echo '</pre>'; exit;
        
        $this->resultPage = $this->resultPageFactory->create();
        
        for($n = 0; $n <= 13; $n += 3) {
            
            if(isset($this->pdfContent[$n]) && !strstr(strtolower($this->pdfContent[$n]['title']), 'afbeeldingen')) {
                $this->tableData[0][] = $this->pdfContent[$n];
            }
            if(isset($this->pdfContent[$n+1]) && !strstr(strtolower($this->pdfContent[$n+1]['title']), 'afbeeldingen')) {
                $this->tableData[1][] = $this->pdfContent[$n+1];
            }
            if(isset($this->pdfContent[$n+2]) && !strstr(strtolower($this->pdfContent[$n+2]['title']), 'afbeeldingen')) {
                $this->tableData[2][] = $this->pdfContent[$n+2];
            }
        
        }
        
        $blockHtml = $this->resultPage->getLayout()
                  ->createBlock('Webfant\Customform\Block\PdfContent')
                  ->setBlockId('pdf_content_' . $n)
                  ->setTemplate('Webfant_Customform::pdf/content.phtml')
                  ->setPdfData([$this->tableData[0], $this->tableData[1], $this->tableData[2]])
                  ->setSignature((isset($this->extraData['signature']) ? $this->extraData['signature'] : ''))
                  ->toHtml();
                  
        $this->pdf->writeHTML($blockHtml, true, false, true, false, '');
        
        $this->pdf->AddPage('L', 'A4');
        
        $blockHtml = $this->resultPage->getLayout()
                  ->createBlock('Webfant\Customform\Block\PdfContent')
                  ->setBlockId('pdf_content_' . $n)
                  ->setTemplate('Webfant_Customform::pdf/lightplan.phtml')
                  ->setLightPlanData($this->pdfContent[12])
                  ->setLightPlanImage((isset($this->extraData['lightplan']) ? $this->extraData['lightplan'] : ''))
                  ->setFreedrawingImage((isset($this->extraData['freedrawing']) ? $this->extraData['freedrawing'] : ''))
                  ->toHtml();
                  
        $this->pdf->writeHTML($blockHtml, true, false, true, false, '');
        
        $this->addAanzichten();
        $this->addAttachments();
        $this->addLeftsideOptions();
        
    }
    
    private function addLeftsideOptions() {
        
        // echo '<pre>'; var_dump($this->extraData['leftside']); exit;
        
        $this->pdf->AddPage('P', 'A4');
        
        if(isset($this->extraData['leftside'])) {
            
            $blockHtml = $this->resultPage->getLayout()
                      ->createBlock('Webfant\Customform\Block\PdfContent')
                      ->setBlockId('pdf_content_options')
                      ->setTemplate('Webfant_Customform::pdf/options.phtml')
                      ->setPdfData($this->extraData['leftside'])
                      ->setLastPageData($this->extraData['last_page'])
                      ->toHtml();
            
            $this->pdf->writeHTML($blockHtml, true, false, true, false, '');
                      
        }
                  
        
    }
    
    private function addAanzichten() {
        
        // $this->pdf->AddPage('L', 'A4');
        
        // echo '<pre>'; var_dump($this->extraData); echo '</pre>'; exit;
        
        $this->pdf->Image($this->aanzichtImgDir . self::OUTSIDE_LEFT, 10, 100, 64, '', '', '', false, 300);
        $this->pdf->Image($this->aanzichtImgDir . self::OUTSIDE_FRONT, 88, 100, 116, '', '', '', false, 300);
        $this->pdf->Image($this->aanzichtImgDir . self::OUTSIDE_RIGHT, 218, 100, 64, '', '', '', false, 300);
        
        // Linkerkant
        if(isset($this->extraData['aanzichten']->left_top) && strlen(trim($this->extraData['aanzichten']->left_top)) > 0) {
            $this->pdf->Image($this->aanzichtImgDir . $this->extraData['aanzichten']->left_top, 10, 103, 61, '', '', '', false, 300);
            $this->pdf->Image($this->aanzichtImgDir . $this->extraData['aanzichten']->left, 10, 120, 61, '', '', '', false, 300);
        } elseif(isset($this->extraData['aanzichten']->left) && strlen(trim($this->extraData['aanzichten']->left)) > 0) {
            $this->pdf->Image($this->aanzichtImgDir . $this->extraData['aanzichten']->left, 10, 103, 61, '', '', '', false, 300);
        }
        
        // Voorkant
        if(isset($this->extraData['aanzichten']->front) && strlen(trim($this->extraData['aanzichten']->front)) > 0) {
            $this->pdf->Image($this->aanzichtImgDir . $this->extraData['aanzichten']->front, 91, 120, 110, '', '', '', false, 300);
        }
        
        // Staanders
        if(isset($this->extraData['aanzichten']->staanders) && strlen(trim($this->extraData['aanzichten']->staanders)) > 0) {
            $this->pdf->Image($this->aanzichtImgDir . $this->extraData['aanzichten']->staanders, 91, 120, 110, '', '', '', false, 300);
        }
        
        // Rechterkant
        if(isset($this->extraData['aanzichten']->right_top) && strlen(trim($this->extraData['aanzichten']->right_top)) > 0) {
            $this->pdf->Image($this->aanzichtImgDir . $this->extraData['aanzichten']->right_top, 221, 103, 61, '', '', '', false, 300);
            $this->pdf->Image($this->aanzichtImgDir . $this->extraData['aanzichten']->right, 221, 120, 61, '', '', '', false, 300);
        } elseif(isset($this->extraData['aanzichten']->right) && strlen(trim($this->extraData['aanzichten']->right)) > 0) {
            $this->pdf->Image($this->aanzichtImgDir . $this->extraData['aanzichten']->right, 221, 103, 61, '', '', '', false, 300);
        }
        
        // Krinners
        if(isset($this->extraData['krinner'])) {
            $this->pdf->Image($this->extraData['krinner'], 12, 183, 272, '', '', '', false, 300);
        }
        
        $this->pdf->SetFillColor(100, 100, 100);
        $this->pdf->SetTextColor(255,255,255);
        
        // Fundatie links
        if(isset($this->extraData['aanzichten']->fundering_left) && strlen(trim($this->extraData['aanzichten']->fundering_left)) > 0) {
            $this->pdf->MultiCell(64, 7, $this->extraData['aanzichten']->fundering_left, 1, 'C', 1, 0, 10, 176, true, 0, false, false, 7, 'M');
        }
        
        // Fundatie voorkant
        if(isset($this->extraData['aanzichten']->fundering_front) && strlen(trim($this->extraData['aanzichten']->fundering_front)) > 0) {
            $this->pdf->MultiCell(116, 7, $this->extraData['aanzichten']->fundering_front, 1, 'C', 1, 0, 88, 176, true, 0, false, false, 7, 'M');
        }
        
        // Fundatie rechts
        if(isset($this->extraData['aanzichten']->fundering_right) && strlen(trim($this->extraData['aanzichten']->fundering_right)) > 0) {
            $this->pdf->MultiCell(64, 7, $this->extraData['aanzichten']->fundering_right, 1, 'C', 1, 0, 218, 176, true, 0, false, false, 7, 'M');
        }
        
        $this->pdf->SetTextColor(0,0,0);
        $this->pdf->SetFont('','', 10);
        $this->pdf->SetFillColor(255, 255, 255);
        
        ////////////////////
        // Afmetingen
        ///////////////////
        
        // Breedte
        if(isset($this->extraData['afmetingen']->breedte) && strlen(trim($this->extraData['afmetingen']->breedte)) > 0) {
            $this->pdf->MultiCell(64, 7, $this->extraData['afmetingen']->breedte, 0, 'L', 1, 0, 150, 169, true, 0, false, false, 7, 'M');
        }
        
        // Diepte
        if(isset($this->extraData['afmetingen']->diepte) && strlen(trim($this->extraData['afmetingen']->diepte)) > 0) {
            $this->pdf->MultiCell(64, 7, $this->extraData['afmetingen']->diepte, 0, 'C', 1, 0, 10, 169, true, 0, false, false, 7, 'M');
            $this->pdf->MultiCell(64, 7, $this->extraData['afmetingen']->diepte, 0, 'C', 1, 0, 218, 169, true, 0, false, false, 7, 'M');
        }
        
        // Onderkant Muur
        if(isset($this->extraData['afmetingen']->onderkant_muur) && strlen(trim($this->extraData['afmetingen']->onderkant_muur)) > 0) {
            $this->pdf->MultiCell(64, 7, $this->extraData['afmetingen']->onderkant_muur, 0, 'L', 1, 0, 12, 140, true, 0, false, false, 7, 'M');
            $this->pdf->MultiCell(64, 7, $this->extraData['afmetingen']->onderkant_muur, 0, 'R', 1, 0, 215, 140, true, 0, false, false, 7, 'M');
        }
        
        // Onderkant Goot
        if(isset($this->extraData['afmetingen']->onderkant_goot) && strlen(trim($this->extraData['afmetingen']->onderkant_goot)) > 0) {
            $this->pdf->MultiCell(18, 14, $this->extraData['afmetingen']->onderkant_goot, 0, 'L', 1, 0, 74, 140, true, 0, false, false, 14, 'M');
            $this->pdf->MultiCell(18, 14, $this->extraData['afmetingen']->onderkant_goot, 0, 'R', 1, 0, 200, 140, true, 0, false, false, 14, 'M');
        }
        
        // HSV Links
        if(isset($this->extraData['afmetingen']->hsv_links) && strlen(trim($this->extraData['afmetingen']->hsv_links)) > 0) {
            $this->pdf->MultiCell(15, 14, $this->extraData['afmetingen']->hsv_links, 0, 'L', 1, 0, 74, 115, true, 0, false, false, 14, 'M');
        }
        
        // HSA Links
        if(isset($this->extraData['afmetingen']->hsa_links) && strlen(trim($this->extraData['afmetingen']->hsa_links)) > 0) {
            $this->pdf->MultiCell(20, 7, $this->extraData['afmetingen']->hsa_links, 0, 'L', 1, 0, 12, 111, true, 0, false, false, 7, 'M');
        }
        
        // HSV Rechts
        if(isset($this->extraData['afmetingen']->hsv_rechts) && strlen(trim($this->extraData['afmetingen']->hsv_rechts)) > 0) {
            $this->pdf->MultiCell(15, 14, $this->extraData['afmetingen']->hsv_rechts, 0, 'R', 1, 0, 202, 115, true, 0, false, false, 14, 'M');
        }
        
        // HSA Rechts
        if(isset($this->extraData['afmetingen']->hsa_rechts) && strlen(trim($this->extraData['afmetingen']->hsa_rechts)) > 0) {
            $this->pdf->MultiCell(20, 7, $this->extraData['afmetingen']->hsa_rechts, 0, 'R', 1, 0, 260, 111, true, 0, false, false, 7, 'M');
        }
        
    }
    
    private function addAttachments() {
        
        $this->pdf->AddPage('P', 'A4');
        
        $blockHtml = false;
        
        foreach($this->attachments as $key => $attachment) {
            
            
            $blockHtml = $this->resultPage->getLayout()
                  ->createBlock('Webfant\Customform\Block\PdfContent')
                  ->setBlockId('pdf_content_attachment')
                  ->setTemplate('Webfant_Customform::pdf/attachments.phtml')
                  ->setAttachments($this->attachments)
                  ->toHtml();
                  
        }
        
        if($blockHtml) {
            $this->pdf->writeHTML($blockHtml, true, false, true, false, '');
        }
        
        
    }
    
    private function gatherPdfContent($id) {
        
        $json = json_decode($this->_helper->getSubmitJson($id));
        
        foreach($json as $formId => $form) {

            $formModel = false;
            $pdfData = [];
            
            foreach($form as $elementName => $element) {
                
                $elementModel = $this->elementRepository->getByName($elementName);
                if(!$elementModel->getFormId()) continue;
                
                try {
                    
                    $formModel = $this->formRepository->get($elementModel->getFormId());
                    
                } catch(\Magento\Framework\Exception\NoSuchEntityException $e) {
                    
                    continue;
                    
                }
                
            }
            
            if(!$formModel) continue;
        
            $this->loadFormJson($formModel->getFormJson());
            
            $n = 0; foreach($form as $elementName => $element) { $n++;
                
                if(strstr($this->elements[$elementName]->className, 'calculationhidden')
                    || strstr($this->elements[$elementName]->className, 'hiddendropdown')
                ) continue;
                
                if(isset($this->elements[$elementName]->values) && is_array($this->elements[$elementName]->values)) {
                    
                    if(is_array($element->inputvalue)) {
                        
                        foreach($element->inputvalue as $key => $inputvalue) {
                            
                            foreach($this->elements[$elementName]->values as $optionValue) {
                                
                                if($inputvalue == $optionValue->value) {
                                    $element->inputvalue[$key] = $optionValue->label;
                                }
                                
                            }
                            
                        }
                        
                    } else {
                        
                        foreach($this->elements[$elementName]->values as $optionValue) {
                            
                            if($element->inputvalue == $optionValue->value) {
                                $element->inputvalue = $optionValue->label;
                            }
                            
                        }
                        
                    }
                    
                }
                
                if($this->elements[$elementName]->type == 'file') {

                    $value = $this->addPdfAttachment($element->value);
                    $pdfData[] = array('label' => $element->label, 'value' => $value);
                     
                }
                
                if(is_array($element->inputvalue)) {
                    $value = implode(',',$element->inputvalue);
                } else {
                    $value = $element->inputvalue;
                }
                
                if(isset($this->elements[$elementName]->last_page) && $this->elements[$elementName]->last_page == true) {
                    $this->extraData['last_page'][] = array('label' => $element->label, 'value' => $value);
                }
                
                if(strstr($value, '.png') || strstr($value, '.jpg')) {
                    // $image = new \SplFileObject('/var/www/html/jason/pub/media/webfant/amcustomform/inmeet_images/' . $value, 'r');
                    // $imageContents = $image->fread($image->getSize());
                    // $imageExtension = $image->getExtension();
                    // $value = '<img src="data:image/'.$imageExtension.';base64,'.$imageContents.'" />';
                    
                    
                    
                    if(strstr($element->label, 'light plan')) {
                        $value='<img src="' . $this->mediaUrl . 'webfant/amcustomform/inmeet_images/'.$value.'" />';
                        $this->extraData['lightplan'] = $value;
                    } elseif(strstr($element->label, 'krinner')) {
                        $value= $this->mediaUrl . 'webfant/amcustomform/inmeet_images/'.$value;
                        $this->extraData['krinner'] = $value;
                    } elseif(strstr($element->label, 'freedrawing')) {
                        $value='<img src="' . $this->mediaUrl . 'webfant/amcustomform/inmeet_images/'.$value.'" />';
                        $this->extraData['freedrawing'] = $value;
                    } elseif(strstr($element->label, 'signature')) {
                        $value='<img src="' . $this->mediaUrl . 'webfant/amcustomform/inmeet_images/'.$value.'" />';
                        $this->extraData['signature'] = $value;
                    } elseif(strstr(strtolower($element->label), 'aanzichten json')) {
                        $this->extraData['aanzichten'] = json_decode($value);
                    }
                    
                } elseif(strstr(strtolower($element->label), 'leftside json')) {
                    $this->extraData['leftside'] = json_decode($value);
                } elseif(strstr(strtolower($element->label), 'afmetingen json')) {
                    $this->extraData['afmetingen'] = json_decode($value);
                } else {
                    $pdfData[] = array('label' => $element->label, 'value' => $value);
                }
                
            }
            
            $this->pdfContent[] = array(
                'pdfData' => $pdfData,
                'title' => $formModel->getTitle(),
                'id' => $formId
            );
            
        }
        
    }
    
    private function addPdfAttachment($image) {
        
        $this->attachments[] = '<img src="' . $this->mediaUrl . 'webfant/amcustomform/' . $image . '" />';
        
        return 'Bijlage ' . count($this->attachments);
        
    }
    
}