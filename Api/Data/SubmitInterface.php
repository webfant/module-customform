<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Api\Data;

/**
 * @api
 */
interface SubmitInterface
{
    /**#@+
     * Constants defined for keys of data array
     */
    const ID = 'id';      
    const CREATED_AT = 'created_at';
    const FIRSTNAME = 'firstname';
    const LASTNAME = 'lastname';
    const SUBMIT_JSON = 'submit_json';

     

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $Id
     *
     * @return \Webfant\Customform\Api\Data\SubmitInterface
     */
    public function setId($id);
 
    /**
     * @return string
     */
    public function getLastname();

    /**
     * @param string $lastname
     *
     * @return \Webfant\Customform\Api\Data\SubmitInterface
     */
    public function setLastname($lastname);
 
    /**
     * @return string
     */
    public function getFirstname();

    /**
     * @param string $firstname
     *
     * @return \Webfant\Customform\Api\Data\SubmitInterface
     */
    public function setFirstname($firstname);
 
    /**
     * @return sting Json
     */
    public function getSubmitJson();

    /**
     * @param Json sting $submitJson
     *
     * @return \Webfant\Customform\Api\Data\SubmitInterface
     */
    public function setSubmitJson($submitJson);
 

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt
     *
     * @return \Webfant\Customform\Api\Data\SubmitInterface
     */
    public function setCreatedAt($createdAt);
 
}
