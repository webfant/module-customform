<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Api\Data\Form\Element;

interface OptionInterface
{
    /**#@+
     * Constants defined for keys of data array
     */
    const OPTION_ID = 'option_id';
    const ELEMENT_ID = 'element_id';
    const OPTION_NAME = 'name';
    const OPTION_VALUE = 'value';
    /**#@-*/

    /**
     * @return int
     */
    public function getOptionId();

    /**
     * @param int $optionId
     *
     * @return \Webfant\Customform\Api\Data\Form\Element\OptionInterface
     */
    public function setOptionId($optionId);

    /**
     * @return int
     */
    public function getElementId();

    /**
     * @param int $elementId
     *
     * @return \Webfant\Customform\Api\Data\Form\Element\OptionInterface
     */
    public function setElementId($elementId);

    /**
     * @return string
     */
    public function getOptionName();

    /**
     * @param string $optionName
     *
     * @return \Webfant\Customform\Api\Data\Form\Element\OptionInterface
     */
    public function setOptionName($optionName);

    /**
     * @return string
     */
    public function getOptionValue();

    /**
     * @param string $optionValue
     *
     * @return \Webfant\Customform\Api\Data\Form\Element\OptionInterface
     */
    public function setOptionValue($optionValue);
}
