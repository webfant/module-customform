<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Api;

/**
 * Interface AnswerRepositoryInterface
 * @api
 */
interface AnswerRepositoryInterface
{
    /**
     * @param \Webfant\Customform\Api\Data\AnswerInterface $answer
     * @return \Webfant\Customform\Api\Data\AnswerInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webfant\Customform\Api\Data\AnswerInterface $answer);

    /**
     * @param int $answerId
     * @return \Webfant\Customform\Api\Data\AnswerInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($answerId);

    /**
     * @param \Webfant\Customform\Api\Data\AnswerInterface $answer
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Webfant\Customform\Api\Data\AnswerInterface $answer);

    /**
     * @param int $answerId
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($answerId);

    /**
     * Lists
     *
     * @return \Webfant\Customform\Api\Data\AnswerInterface[] Array of items.
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getList();
}
