<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Api;

/**
 * Interface SubmitRepositoryInterface
 * @api
 */
interface SubmitRepositoryInterface
{
    /**
     * @param \Webfant\Customform\Api\Data\SubmitInterface $submit
     * @return \Webfant\Customform\Api\Data\SubmitInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webfant\Customform\Api\Data\SubmitInterface $submit);

    /**
     * @param int $id
     * @return \Webfant\Customform\Api\Data\SubmitInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($id);

    /**
     * @param \Webfant\Customform\Api\Data\SubmitInterface $submit
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Webfant\Customform\Api\Data\SubmitInterface $submit);

    /**
     * @param int $id
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($id);

    /**
     * Lists
     *
     * @return \Webfant\Customform\Api\Data\SubmitInterface[] Array of items.
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getList();

}
