<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Api;

/**
 * Interface FormRepositoryInterface
 * @api
 */
interface FormRepositoryInterface
{
    /**
     * @param \Webfant\Customform\Api\Data\FormInterface $form
     * @return \Webfant\Customform\Api\Data\FormInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webfant\Customform\Api\Data\FormInterface $form);

    /**
     * @param int $formId
     * @return \Webfant\Customform\Api\Data\FormInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($formId);

    /**
     * @param string $formCode
     * @return Data\FormInterface|bool
     */
    public function getByFormCode($formCode);

    /**
     * @param \Webfant\Customform\Api\Data\FormInterface $form
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Webfant\Customform\Api\Data\FormInterface $form);

    /**
     * @param int $formId
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($formId);

    /**
     * Lists
     *
     * @return \Webfant\Customform\Api\Data\FormInterface[] Array of items.
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getList();
}
