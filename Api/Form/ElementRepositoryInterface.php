<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Api\Form;

/**
 * Interface ElementRepositoryInterface
 * @api
 */
interface ElementRepositoryInterface
{
    /**
     * @param \Webfant\Customform\Api\Data\Form\ElementInterface $element
     * @return \Webfant\Customform\Api\Data\Form\ElementInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webfant\Customform\Api\Data\Form\ElementInterface $element);

    /**
     * @param int $elementId
     * @return \Webfant\Customform\Api\Data\Form\ElementInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($elementId);
    
    /**
     * @param string $elementName
     * @return \Webfant\Customform\Api\Data\Form\ElementInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByName($elementName);

    /**
     * @param int $formId
     * @return \Webfant\Customform\Model\ResourceModel\Form\Element\Collection
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCollectionIdsByFormId($formId);

    /**
     * @param \Webfant\Customform\Api\Data\Form\ElementInterface $element
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Webfant\Customform\Api\Data\Form\ElementInterface $element);

    /**
     * @param int $elementId
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($elementId);

    /**
     * @param $formId
     * @param array $excludedElementsIds
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function truncateByFormId($formId, $excludedElementsIds = []);
}
