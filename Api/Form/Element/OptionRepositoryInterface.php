<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Api\Form\Element;

/**
 * Interface OptionRepositoryInterface
 * @api
 */
interface OptionRepositoryInterface
{
    /**
     * @param \Webfant\Customform\Api\Data\Form\Element\OptionInterface $option
     * @return \Webfant\Customform\Api\Data\Form\Element\OptionInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webfant\Customform\Api\Data\Form\Element\OptionInterface $option);

    /**
     * @param int $optionId
     * @return \Webfant\Customform\Api\Data\Form\Element\OptionInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($optionId);

    /**
     * @param \Webfant\Customform\Api\Data\Form\Element\OptionInterface $option
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Webfant\Customform\Api\Data\Form\Element\OptionInterface $option);

    /**
     * @param int $optionId
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($optionId);
}
