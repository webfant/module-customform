<?php

/**
 *
 */

namespace Webfant\Customform\Block;

use Webfant\Customform\Helper\Data;
use \Magento\Customer\Block\Form\Login;
class Inmeet extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Webfant\Customform\Helper\Data
     */
    protected $_helper;

    /**
     * @var param $submitId
     */
    protected $submitId;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webfant\Customform\Helper\Data $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
	    $this->_storeManager = $storeManager;
        $this->_helper = $helper;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }
    
    
    public function renderForm($id){
        
        return $this->_helper->renderForm($id);
        
        
    }
     /**
     * @return string
     */
    public function getFormAction()
    {
        return $this->_helper->getSubmitUrl();
    }
	
	
	/**
	 * @return string
	 */
	public function getImageUrl()
	{
		return $this->_helper->getImageUrl();
	}
	
    public function getSubmitJson(){

        $this->submitId = $this->getRequest()->getParam('id');
        if($this->submitId){
            return $this->_helper->getSubmitJson($this->submitId);
        }
        return false;

    }

    public function getSubmitId(){
        return $this->getRequest()->getParam('id');;
    }
    
    public function getFormMediaUrl(){
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).Data::MEDIA_PATH;
    }
    
    public function getInmeetImageUrl() {
        
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/';
        
    }
    
    public function getAanzichtenImagesJson() {
        
        $aanichten = array();
        
        $aanzichten['gsw_r3']['left'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r3/left');
        $aanzichten['gsw_r3']['front'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r3/front');
        $aanzichten['gsw_r3']['right'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r3/right');
        
        $aanzichten['gsw_r4']['left'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r4/left');
        $aanzichten['gsw_r4']['front'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r4/front');
        $aanzichten['gsw_r4']['right'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r4/right');
        
        $aanzichten['gsw_r5']['left'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r5/left');
        $aanzichten['gsw_r5']['front'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r5/front');
        $aanzichten['gsw_r5']['right'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/gsw_r5/right');
        
        $aanzichten['rabat']['left'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/rabat/left');
        $aanzichten['rabat']['front'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/rabat/front');
        $aanzichten['rabat']['right'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'inmeet_images/' . $this->_scopeConfig->getValue('customform_images/rabat/right');
        
        return json_encode($aanzichten);
        
        
    }
    
    
    
}