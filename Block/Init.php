<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Block;

use Magento\Backend\Block\Widget\Grid\Column\Filter\Store;
use Magento\Framework\Exception\NoSuchEntityException;
use Webfant\Customform\Helper\Data;

class Init extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    protected $_template = 'init.phtml';

    /**
     * @var \Webfant\Customform\Model\Form
     */
    protected $currentForm;

    /**
     * @var \Webfant\Customform\Helper\Data
     */
    private $helper;

    /**
     * @var \Webfant\Customform\Model\FormRepository
     */
    private $formRepository;

    /**
     * @var \Magento\Customer\Model\SessionFactory
     */
    private $sessionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Data $helper,
        \Webfant\Customform\Model\FormRepository $formRepository,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Customer\Model\SessionFactory $sessionFactory,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->request = $request;
        $this->formRepository = $formRepository;
        $this->sessionFactory = $sessionFactory;

        parent::__construct($context, $data);
    }

    public function _construct()
    {
        $id = $this->getFormId();
        if ($id) {
            try {
                $this->currentForm = $this->formRepository->get($id);
            } catch (NoSuchEntityException $e) {
                $this->currentForm = false;
            }
        }
        parent::_construct();
    }

    /**
     * @return \Webfant\Customform\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * @return string
     */
    public function toHtml()
    {
        if ($this->validate()) {
            return parent::toHtml();
        }

        return '';
    }

    protected function validate()
    {
    
       
        if (!$this->currentForm || !$this->currentForm->isEnabled()) {
            return false;
        }
       
        /* check for store ids*/
        $stores = $this->currentForm->getStoreId();
        $stores = explode(',', $stores);
        $currentStoreId = $this->_storeManager->getStore()->getId();
        if (!in_array(Store::ALL_STORE_VIEWS, $stores) && !in_array($currentStoreId, $stores)) {
            return false;
        }
  
        /* check for customer groups*/
        $availableGroups = $this->currentForm->getCustomerGroup();
         
        $availableGroups = explode(',', $availableGroups);
        $currentGroup = $this->sessionFactory->create()->getCustomerGroupId();
        if (!in_array($currentGroup, $availableGroups)) {
            // return false;
        }

        return true;
    }

    /**
     * @return \Webfant\Customform\Model\Form
     */
    public function getCurrentForm()
    {
        return $this->currentForm;
    }

    /**
     * @return \Magento\Framework\Phrase|mixed|string
     */
    public function getButtonTitle()
    {
        $title = $this->currentForm->getSubmitButton();
        if (!$title) {
            $title = __('Submit');
        }

        return $title;
    }

    /**
     * @return string
     */
    public function getFormDataJson()
    {
        $formData =  $this->getCurrentForm()->getFormJson();
        
        if ($this->getCurrentForm()->isHideEmailField()
            && !empty($this->sessionFactory->create()->getCustomer()->getEmail())
        ) {
            $formData = $this->helper->decode($formData);
            foreach ($formData as $key => $value) {
                if ($value['name'] === $this->getCurrentForm()->getEmailField()) {
                    unset($formData[$key]);
                    break;
                }
            }

            $formData = $this->helper->encode(array_values($formData));
        }
        
        $result = [
            'dataType' => 'json',
            'formData' => $formData,
            'src_image_progress' => $this->getViewFileUrl('Webfant_Customform::images/loading.gif'),
            'ajax_submit' => $this->getCurrentForm()->getSuccessUrl() == Data::REDIRECT_PREVIOUS_PAGE ? 1 : 0
        ];

        return $this->helper->encode($result);
    }

	/**
	 * @return string
	 */
	public function getFormAction()
	{
		return $this->helper->getSubmitUrl();
	}
	

    /**
     * Check if GDPR consent enabled
     *
     * @return bool
     */
    public function isGDPREnabled()
    {
        return $this->helper->isGDPREnabled();
    }
    
    /**
     * Check if check if button for form is enabled
     *
     * @return bool
     */
    public function getSubmitStatus()
    {
        return (bool)$this->currentForm->getSubmitEnabled();
    }

    /**
     * Get text for GDPR
     *
     * @return string
     */
    public function getGDPRText()
    {
        return $this->helper->getGDPRText();
    }
}
