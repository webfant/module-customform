<?php
/**
 * @author Webfant Team
 */

namespace Webfant\Customform\Block\Adminhtml\Form\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('form_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Form Information'));
    }
}
