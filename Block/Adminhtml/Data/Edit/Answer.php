<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Block\Adminhtml\Data\Edit;

use Webfant\Customform\Helper\Data;
use Magento\MediaStorage\Model\File\Uploader;

class Answer extends \Magento\Backend\Block\Template
{
    protected $currentResponse;

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Webfant\Customform\Model\FormRepository
     */
    private $formRepository;

    /**
     * @var \Magento\Framework\Json\DecoderInterface
     */
    private $jsonDecoder;

    /**
     * @var \Webfant\Customform\Model\ResourceModel\Form\Element\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var Data
     */
    private $helper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Webfant\Customform\Model\FormRepository $formRepository,
        \Magento\Framework\Json\DecoderInterface $jsonDecoder,
        Data $helper,
        \Webfant\Customform\Model\ResourceModel\Form\Element\CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->customerRepository = $customerRepository;
        $this->storeManager = $context->getStoreManager();
        $this->formRepository = $formRepository;
        $this->jsonDecoder = $jsonDecoder;
        $this->collectionFactory = $collectionFactory;
        $this->helper = $helper;

        parent::__construct($context, $data);
    }

    public function _construct()
    {
        $this->currentResponse = $this->coreRegistry->registry(
            \Webfant\Customform\Controller\Adminhtml\Answer::CURRENT_ANSWER_MODEL
        );
        parent::_construct();
    }
	
	/**
	 * Add buttons on request view page
	 * @return $this
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
    protected function _prepareLayout()
    {
        $this->getToolbar()->addChild(
            'back_button',
            \Magento\Backend\Block\Widget\Button::class,
            [
                'label' => __('Back'),
                'onclick' => 'setLocation("' . $this->getUrl('*/*/index') . '")',
                'class' => 'back'
            ]
        );
        $this->getToolbar()->addChild(
            'delete_button',
            \Magento\Backend\Block\Widget\Button::class,
            [
                'label' => __('Delete Data'),
                'onclick' => 'setLocation("' . $this->getUrl('*/*/delete', [
                        'id' => $this->currentResponse->getAnswerId()
                    ]) . '")',
                'class' => 'delete'
            ]
        );
        $sendEmailButton = $this->getLayout()
            ->createBlock(\Magento\Backend\Block\Widget\Button::class)
            ->setData(
                [
                    'label' => __('Send Email'),
                    'class' => 'action-save action-secondary',
                    'onclick' => 'this.form.submit();'
                ]
            );
        $this->setChild('submit_button', $sendEmailButton);

        parent::_prepareLayout();
        return $this;
    }

    /**
     * Submit URL getter
     *
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('*/*/send', ['answer_id' => $this->getCurrentResponse()->getAnswerId()]);
    }

    /**
     * @return array
     */
    public function getInformationData()
    {
        /** @var \Webfant\Customform\Model\Answer $model */
        $model = $this->getCurrentResponse();

        try {
            $form = $this->formRepository->get($model->getFormId());
        } catch (\Exception $ex) {
            $form = null;
        }
        $formName = $form ? $form->getCode(): __('This Form #%1 was removed', $model->getFormId());

        $customerName = $this->helper->getCustomerName($model->getCustomerId(), true);
        $customerName = (array_key_exists('customer_link', $customerName) && $customerName['customer_link'])
            ? $customerName['customer_link'] : $customerName['customer_name'];
        $store = $this->storeManager->getStore($model->getStoreId())->getName();

        $result =  [
            ['label' => __('Form'), 'value' => $formName],
            ['label' => __('Submitted'), 'value' => $model->getCreatedAt()],
            ['label' => __('IP'), 'value' => $model->getIp()],
            ['label' => __('Customer'), 'value' => $customerName],
            ['label' => __('Store'), 'value' => $store]
        ];

        return $result;
    }

    /**
     * @return array
     */
    public function getResponseData()
    {
        /** @var \Webfant\Customform\Model\Answer $model */
        $model = $this->getCurrentResponse();
        $result = [];
        $formData = $model->getResponseJson();
        if ($formData) {
            $fields = $this->jsonDecoder->decode($formData);

            foreach ($fields as $field) {
                $value = $field['value'];
                if (is_array($value)) {
                    $value = implode(', ', $value);
                }

                $value = $this->escapeHtml($value);

                if ($field['type'] == 'file') {
                    $value = Uploader::getCorrectFileName($value);
                    $url = $this->_storeManager->getStore()
                            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
                            . Data::MEDIA_PATH . $value;
                    $value = '<a download href="' . $url .  '">' . __('Download: ') . $value . '</a>';
                }

                $result[] = [
                    'label' => $field['label'],
                    'value' => $value
                ];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getAdminResponseData()
    {
        /** @var \Webfant\Customform\Model\Answer $model */
        $model = $this->getCurrentResponse();
        $responseEmail = $model->getAdminResponseEmail();
        $status =  $responseEmail ? __('Sent') : __('Pending');
        $result =  [['label' => __('Response Status'), 'value' => $status]];
        if ($responseEmail) {
            $result[] = ['label' => __('Recipient'), 'value' => $responseEmail];
            $result[] = ['label' => __('Response Message'), 'value' => $this->escapeHtml($model->getResponseMessage())];
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isShowEmailSendingForm()
    {
        return empty($this->getCurrentResponse()->getAdminResponseEmail());
    }

    /**
     * @return \Webfant\Customform\Model\Answer
     */
    public function getCurrentResponse()
    {
        return $this->currentResponse;
    }
}
