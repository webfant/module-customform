<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Time extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Time');
    }

    public function generateContent()
    {
        return '<input class="form-control" type="time"/>';
    }
}
