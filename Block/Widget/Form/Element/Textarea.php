<?php
/**
 * @author Webfant Team

 *
 */


namespace Webfant\Customform\Block\Widget\Form\Element;

class Textarea extends Textinput
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Textarea');
        $this->options['image_href'] = 'Webfant_Customform::images/textarea.png';
    }

    public function generateContent()
    {
        return '<textarea class="form-control" type="text"/>';
    }
}
