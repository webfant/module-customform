<?php
/**
 * @author Webfant Team

 *
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Checkboxtwo extends Checkbox
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Checkbox v.2');
    }

    public function getLabelClassName()
    {
        return '';
    }

    public function getBr()
    {
        return '';
    }
}
