<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Checkboxthree extends Checkbox
{
   public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Checkbox v.3');
    }


    public function generateContent()
    {
        return '
           <span ' . $this->getLabelClassName() . ' for="checkbox1">'. __('Checkbox unselected') . '</span>
        <input value="option1" type="checkbox" name="checkbox1[]" id="checkbox1">
         
            ' . $this->getBr() . '
            <input value="option-345345" type="Checkbox" name="checkbox1[]" checked id="checkbox11"> 
            <label ' . $this->getLabelClassName() . ' for="checkbox11">'. __('Checkbox selected') . '</label>';
    }
    
    
    public function getLabelClassName()
    {
        return 'class="amform-versiontwo-label"';
    }

    public function getBr()
    {
        return '';
    }
}
