<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Hone extends Text
{
    public function _construct()
    {
        parent::_construct();

        $this->options['title'] = __('H1');
    }

    public function generateContent()
    {
        return '<h1 class="title">' . $this->getExamplePhrase() . '</h1>';
    }
}
