<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Htwo extends Text
{
    public function _construct()
    {
        parent::_construct();

        $this->options['title'] = __('H2');
    }

    public function generateContent()
    {
        return '<h2 class="title">' . $this->getExamplePhrase() . '</h2>';
    }
}
