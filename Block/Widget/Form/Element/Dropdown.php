<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Dropdown extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('DropDown');
        $this->options['image_href'] = 'Webfant_Customform::images/dropdown.png';
    }

    public function generateContent()
    {
        return '<select><option value="">' . $this->getTestOptionText() . '</option></select>';
    }

    protected function getTestOptionText()
    {
        return __('--Select an option--');
    }
}
