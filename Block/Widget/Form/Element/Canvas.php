<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Block\Widget\Form\Element;

class Canvas extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();

        $this->options['title'] = __('Canvas');
        $this->options['image_href'] = 'Webfant_Customform::images/text.png';
    }

    public function generateContent()
    {
        return '<canvas style="border:1px solid black;">' . $this->getExamplePhrase() . '</canvas>';
    }

    public function getExamplePhrase()
    {
        return __('Canvas drawing');
    }
}
