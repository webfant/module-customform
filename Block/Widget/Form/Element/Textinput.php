<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Block\Widget\Form\Element;

class Textinput extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Text Input');
    }

    public function generateContent()
    {
        return '<input class="form-control" type="text"/>';
    }
}
