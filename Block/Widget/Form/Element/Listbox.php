<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Listbox extends Dropdown
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('ListBox');
        $this->options['image_href'] = 'Webfant_Customform::images/listbox.png';
    }

    public function generateContent()
    {
        return '<select class="select multiselect admin__control-multiselect" multiple="multiple"><option value="">'
            . $this->getTestOptionText()
            . '</option></select>';
    }
}
