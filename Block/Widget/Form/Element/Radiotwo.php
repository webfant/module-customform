<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Radiotwo extends Radio
{
    public function _construct()
    {
        parent::_construct();

        $this->options['title'] = __('Radio v.2');
    }

    public function getLabelClassName()
    {
        return 'class="amform-versiontwo-label"';
    }

    public function getBr()
    {
        return '';
    }
}
