<?php
/**
 * @author Webfant Team

 *
 */


namespace Webfant\Customform\Block\Widget\Form\Element;

class Number extends Textinput
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Number Input');
        $this->options['image_href'] = 'Webfant_Customform::images/textarea.png';
    }

    public function generateContent()
    {
        return '<input class="form-control" type="number"/>';
    }
}
