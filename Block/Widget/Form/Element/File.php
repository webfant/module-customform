<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class File extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('File');
        $this->options['image_href'] = 'Webfant_Customform::images/upload.png';
    }

    public function generateContent()
    {
        return '<input class="form-control" type="file"/>';
    }
}
