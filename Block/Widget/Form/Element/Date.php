<?php
/**
 * @author Webfant Team

 *
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Date extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Date');
        $this->options['image_href'] = 'Webfant_Customform::images/date.png';
    }

    public function generateContent()
    {
        return '<input class="form-control" type="date"/>';
    }
}
