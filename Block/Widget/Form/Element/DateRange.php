<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class DateRange extends Date
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Date Range');
    }
}
