<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright В© 2016 Webfant. All rights reserved.
 */
namespace Webfant\Customform\Block\Widget\Form\Element;

class Hthree extends Text
{
    public function _construct()
    {
        parent::_construct();

        $this->options['title'] = __('H3');
    }

    public function generateContent()
    {
        return '<h3 class="title">' . $this->getExamplePhrase() . '</h3>';
    }
}
