<?php

namespace Webfant\Customform\Block;


use Webfant\Customform\Helper\Data;

class Grid extends \Magento\Framework\View\Element\Template

    {
    
    /**
    
    * @var \Magento\Checkout\Model\CompositeConfigProvider
    
    */
    
    protected $configProvider;
    /**
    
    * @var array
    
    */
    
    protected $_layoutProcessors;
    
    protected $currentForm;
    
        /**
         * @var \Webfant\Customform\Helper\Data
         */
        private $helper;
    
    /**
    
    * Lists constructor.
    
    * @param \Magento\Framework\View\Element\Template\Context $context
    
    * @param array $layoutProcessors
    
    * @param array $data
    
    */
    
     /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;
    
    private $users = [];
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\CompositeConfigProvider $configProvider,
        Data $helper,
        \Webfant\Customform\Model\SubmitFactory $SubmitFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Customer\Model\Session $session,
        array $layoutProcessors = [],
        array $data = []
    ) {
    
     parent::__construct($context, $data);
        $this->helper = $helper;
        $this->configProvider = $configProvider;
        $this->_submitFactory = $SubmitFactory;
        $this->_layoutProcessors = $layoutProcessors;
        $this->session = $session;
        $this->url = $url;
    }
    
    

    public function getNewUrl(){
        return $this->url->getUrl('inmeet/grid/index');
    }
    
    public function getLogOutUrl(){
        return $this->url->getUrl('customer/account/logout');
    }
        
    public function getCollection(){
        
        $submits = $this->_submitFactory->create();
		$collection = $submits->getCollection();
		foreach($collection as $item){
		 
		 if(!empty($item['firstname']) && !empty($item['lastname'])){
		    $url = $this->url->getUrl('inmeet/grid/index/id/'.$item['id']);
		    $deleteUrl = $this->url->getUrl('inmeet/grid/delete/id/'.$item['id']);
		    $pdfUrl = $this->url->getUrl('inmeet/form/generatePdf/id/' . $item['id']);
		    $mailUrl = $this->url->getUrl('inmeet/form/sendMail/id/' . $item['id']);
		    $data[] = array(
                'id' => $item['id'],
                'name' => $item['firstname'] . ' ' . $item['lastname'],
                'customer_name' => $item['customer_name'],
                'status' => $item['status'],
                'edit' => "<a href='{$url}'>Openen</a>",
                'delete' => ($item['status'] == 'open') ? "<a href='{$deleteUrl}'>Verwijderen</a>" : '',
                'pdf' => "<a href='{$pdfUrl}'>Download PDF</a>",
                'sendMail' => "<a href='{$mailUrl}'>E-Mail</a>",
                'customer_id' => $item['customer_id'],
            );
            
            $this->users[$item['customer_id']] = array('id' => $item['customer_id'], 'name' => $item['customer_name']);
            
		 }
			 
		}
		if(!empty($data)){
		    return $this->helper->encode($data);
		}
	 
    }
    
    public function getUsers() {
        
        return $this->users;
        
    }
    
    public function getCurrentUser() {
        
        return $this->session->getCustomer()->getId();
        
    }
    
    /**
    * @return string
    */
    public function getJsLayout() {
       
     
        foreach ($this->_layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
    
        $this->jsLayout["components"]["sample-grid"]['data'] = $this->getCollection();
        $this->jsLayout["components"]["sample-grid"]['users'] = $this->getUsers();
        $this->jsLayout["components"]["sample-grid"]['current_user'] = $this->getCurrentUser();

        return parent::getJsLayout();
    
    }

}