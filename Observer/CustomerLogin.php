<?php

namespace Webfant\Customform\Observer;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\HTTP\PhpEnvironment\Request;
use Magento\Customer\Api\GroupRepositoryInterface;

class CustomerLogin implements \Magento\Framework\Event\ObserverInterface
{

    protected $_remoteAddress;
    protected $_request;
    protected $_geoipFactory;
    
    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    private $responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    public function __construct(
        RemoteAddress $remoteAddress,
        Request $request,
        GroupRepositoryInterface $groupRepository,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url
    )
    {
        $this->_remoteAddress = $remoteAddress;
        $this->_request = $request;
        $this->_groupRepository = $groupRepository;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        
        $data['customer_id'] = $observer->getEvent()->getCustomer()->getId();
         
        $groupId = $observer->getEvent()->getCustomer()->getGroupId();
        $group = $this->_groupRepository->getById($groupId);
        $groupsName = $group->getCode();
    
        if($groupsName === 'inmeet'){
                
            $redirectionUrl = $this->url->getUrl('inmeet/index/index'); 
            $this->responseFactory->create()->setRedirect($redirectionUrl)->sendResponse();
            die();
         
        }
   
    }
}