<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Webfant\Customform\Model\Config\Source\Status;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;


class SubmitCounter extends Column
{


    /** Url path */
    const SUBMIT_URL_PATH_EDIT = 'customform/submit/edit';
    const SUBMIT_URL_PATH_DELETE = 'customform/submit/delete';


    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::SUBMIT_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['id'])) {
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl($this->editUrl, ['id' => $item['id']]),
                        'label' => __('Edit')
                    ];
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::SUBMIT_URL_PATH_DELETE, ['id' => $item['id']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete "${ $.$data.id }"'),
                            'message' => __('Are you sure you wan\'t to delete a "${ $.$data.id }" record?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
