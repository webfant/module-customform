<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Ui\Component\Listing\Column;

class AnswerActions extends Actions
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')]['edit'] = [
                    'href' => $this->urlBuilder->getUrl(
                        'webfant_customform/answer/edit',
                        [
                            'id' => $item['answer_id']
                        ]
                    ),
                    'label' => __('View'),
                    'hidden' => false,
                ];
            }
        }
        return $dataSource;
    }
}
