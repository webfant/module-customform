<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Ui\Component\Listing\Column;
//use Magento\Framework\Url;
class SubmitActions extends Actions
{
	
	const DELETE_UTLPATH = "webfant_customform/submit/delete";
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
	            
	            $url = $this->urlBuilder->getBaseUrl().'inmeet/index/index/id/'.$item['id'];
				$item[$this->getData('name')]['edit'] = [
                    'href' => $url,
                    'label' => __('View/ Edit'),
                    'hidden' => false,
                ];
	            $item[$this->getData('name')]['delete'] = [
		            'href' => $this->urlBuilder->getUrl(self::DELETE_UTLPATH, ['id' => $item['id']]),
		            'label' => __('Delete'),
		            'confirm' => [
			            'title' => __('Delete "${ $.$data.firstname }" "${ $.$data.lastname }"'),
			            'message' => __('Are you sure you wan\'t to delete a "${ $.$data.firstname } ${ $.$data.lastname }" record?')
		            ]
	            ];
            }
        }
        return $dataSource;
    }
}
