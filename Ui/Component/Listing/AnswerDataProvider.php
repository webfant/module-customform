<?php
/**
 * @author Webfant Team

 *
 */


namespace Webfant\Customform\Ui\Component\Listing;

use Webfant\Customform\Model\Config\Source\Status;

class AnswerDataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{
    /**
     * @param \Magento\Framework\Api\Search\SearchResultInterface $searchResult
     * @return array
     */
    protected function searchResultToOutput(\Magento\Framework\Api\Search\SearchResultInterface $searchResult)
    {
        $result = [
            'items'        => [],
            'totalRecords' => $searchResult->getTotalCount(),
        ];

        foreach ($searchResult->getItems() as $item) {
            $status = $item->getAdminResponseStatus();
            switch ($status) {
                case Status::PENDING:
                    $status = __('Pending');
                    break;
                case Status::ANSWERED:
                    $status = __('Answered');
            }
            $item->setData('admin_response_status', $status);
            $result['items'][] = $item->getData();
        }

        return $result;
    }
}
