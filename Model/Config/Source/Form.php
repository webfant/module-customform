<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model\Config\Source;

class Form implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Webfant\Customform\Model\ResourceModel\Form\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Webfant\Customform\Model\ResourceModel\Form\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    public function toOptionArray()
    {
        $result = [];
        $collection = $this->collectionFactory->create();
        foreach ($collection as $item) {
            $result[] = ['value' => $item->getFormId(), 'label' => $item->getTitle()];
        }

        return $result;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $optionArray = $this->toOptionArray();
        $labels =  array_column($optionArray, 'label');
        $values =  array_column($optionArray, 'value');

        return array_combine($values, $labels);
    }
}
