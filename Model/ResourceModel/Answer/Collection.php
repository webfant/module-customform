<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model\ResourceModel\Answer;

class Collection extends \Magento\Cms\Model\ResourceModel\Page\Collection
{
    protected $_idFieldName = 'answer_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webfant\Customform\Model\Answer', 'Webfant\Customform\Model\ResourceModel\Answer');
    }
}
