<?php
/**
 * @author Webfant Team
 */

/**
 * Copyright © 2015 Webfant. All rights reserved.
 */

namespace Webfant\Customform\Model\ResourceModel;

class Answer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('wf_customform_answer', 'answer_id');
    }
}
