<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model\ResourceModel\Submit;

class Collection extends \Magento\Cms\Model\ResourceModel\Page\Collection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webfant\Customform\Model\Submit', 'Webfant\Customform\Model\ResourceModel\Submit');
        $this->_setIdFieldName('id');
    }
}
