<?php
/**
 * @author Webfant Team
 */

namespace Webfant\Customform\Model\ResourceModel;

class Form extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('wf_customform_form', 'form_id');
    }
}
