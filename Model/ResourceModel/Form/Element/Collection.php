<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model\ResourceModel\Form\Element;

class Collection extends \Magento\Cms\Model\ResourceModel\Page\Collection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Webfant\Customform\Model\Form\Element',
            'Webfant\Customform\Model\ResourceModel\Form\Element'
        );
    }

    public function getFieldsByFormId($formId)
    {
        $this->addFieldToFilter('form_id', $formId);
        return $this;
    }
}
