<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model\ResourceModel\Form\Element;

class Option extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('wf_customform_options', 'option_id');
    }
}
