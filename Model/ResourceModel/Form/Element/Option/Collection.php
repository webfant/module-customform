<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model\ResourceModel\Form\Element\Option;

class Collection extends \Magento\Cms\Model\ResourceModel\Page\Collection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Webfant\Customform\Model\Form\Element\Option',
            'Webfant\Customform\Model\ResourceModel\Form\Element\Option'
        );
    }

    public function getFieldsByElementId($elementId)
    {
        $this->addFieldToFilter('element_id', $elementId);
        return $this;
    }
}
