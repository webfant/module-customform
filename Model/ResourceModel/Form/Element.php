<?php
/**
 * @author Webfant Team

 *
 */


namespace Webfant\Customform\Model\ResourceModel\Form;

class Element extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('wf_customform_elements', 'element_id');
    }
}
