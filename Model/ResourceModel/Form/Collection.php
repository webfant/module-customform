<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model\ResourceModel\Form;

class Collection extends \Magento\Cms\Model\ResourceModel\Page\Collection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webfant\Customform\Model\Form', 'Webfant\Customform\Model\ResourceModel\Form');
        $this->_setIdFieldName('form_id');
    }
}
