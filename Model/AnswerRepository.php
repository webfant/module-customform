<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model;

use Webfant\Customform\Api\Data;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Webfant\Customform\Model\ResourceModel\Answer as AnswerResource;
use Webfant\Customform\Model\AnswerFactory;
use Webfant\Customform\Model\ResourceModel\Answer\CollectionFactory as AnswerCollectionFactory;

class AnswerRepository implements \Webfant\Customform\Api\AnswerRepositoryInterface
{
    /**
     * @var array
     */
    protected $answer = [];

    /**
     * @var ResourceModel\Answer
     */
    private $answerResource;

    /**
     * @var AnswerFactory
     */
    private $answerFactory;

    /**
     * @var ResourceModel\Answer\CollectionFactory
     */
    private $answerCollectionFactory;

    public function __construct(
        AnswerResource $answerResource,
        AnswerFactory $answerFactory,
        AnswerCollectionFactory $answerCollectionFactory
    ) {
        $this->answerResource = $answerResource;
        $this->answerFactory = $answerFactory;
        $this->answerCollectionFactory = $answerCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Data\AnswerInterface $answer)
    {
        if ($answer->getAnswerId()) {
            $answer = $this->get($answer->getAnswerId())->addData($answer->getData());
        }

        try {
            $this->answerResource->save($answer);
            unset($this->answer[$answer->getAnswerId()]);
        } catch (\Exception $e) {
            if ($answer->getAnswerId()) {
                throw new CouldNotSaveException(
                    __('Unable to save answer with ID %1. Error: %2', [$answer->getAnswerId(), $e->getMessage()])
                );
            }
            throw new CouldNotSaveException(__('Unable to save new answer. Error: %1', $e->getMessage()));
        }
        
        return $answer;
    }

    /**
     * {@inheritdoc}
     */
    public function get($answerId)
    {
        if (!isset($this->answer[$answerId])) {
            /** @var \Webfant\Customform\Model\Answer $answer */
            $answer = $this->answerFactory->create();
            $this->answerResource->load($answer, $answerId);
            if (!$answer->getAnswerId()) {
                throw new NoSuchEntityException(__('Answer with specified ID "%1" was not found.', $answerId));
            }
            $this->answer[$answerId] = $answer;
        }
        return $this->answer[$answerId];
    }

    /**
     * {@inheritdoc}
     */
    public function delete(Data\AnswerInterface $answer)
    {
        try {
            $this->answerResource->delete($answer);
            unset($this->answer[$answer->getAnswerId()]);
        } catch (\Exception $e) {
            if ($answer->getAnswerId()) {
                throw new CouldNotDeleteException(
                    __('Unable to remove answer with ID %1. Error: %2', [$answer->getAnswerId(), $e->getMessage()])
                );
            }
            throw new CouldNotDeleteException(__('Unable to remove answer. Error: %1', $e->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($answerId)
    {
        $model = $this->get($answerId);
        $this->delete($model);
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        $answerCollection = $this->answerCollectionFactory->create();
        $answerList = [];

        foreach ($answerCollection as $answer) {
            $answerList[] = $answer;
        }

        return $answerList;
    }
}
