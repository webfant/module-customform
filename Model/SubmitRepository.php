<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model;

use Webfant\Customform\Api\Data;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Webfant\Customform\Model\ResourceModel\Submit as SubmitResource;
use Webfant\Customform\Model\SubmitFactory;
use Webfant\Customform\Model\ResourceModel\Submit\CollectionFactory as SubmitCollectionFactory;

class SubmitRepository implements \Webfant\Customform\Api\SubmitRepositoryInterface
{
    /**
     * @var array
     */
    protected $submit = [];

    /**
     * @var ResourceModel\Submit
     */
    private $submitResource;

    /**
     * @var SubmitFactory
     */
    private $submitFactory;

    /**
     * @var ResourceModel\Submit\CollectionFactory
     */
    private $submitCollectionFactory;

    public function __construct(
        SubmitResource $submitResource,
        SubmitFactory $submitFactory,
        SubmitCollectionFactory $submitCollectionFactory
    ) {
        $this->submitResource = $submitResource;
        $this->submitFactory = $submitFactory;
        $this->submitCollectionFactory = $submitCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Data\SubmitInterface $submit)
    {
        if ($submit->getId()) {
            $submit = $this->get($submit->getId())->addData($submit->getData());
        }

        try {
            $this->submitResource->save($submit);
            unset($this->submit[$submit->getId()]);
        } catch (\Exception $e) {
            if ($submit->getId()) {
                throw new CouldNotSaveException(
                    __('Unable to save form with ID %1. Error: %2', [$submit->getSubmitId(), $e->getMessage()])
                );
            }
            throw new CouldNotSaveException(__('Unable to save form. Error: %1', $e->getMessage()));
        }
        
        return $submit;
    }

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        if (!isset($this->submit[$id])) {
            /** @var \Webfant\Customform\Model\Submit $submit */
            $submit = $this->submitFactory->create();
            $this->submitResource->load($submit, $id);
            if (!$submit->getId()) {
                throw new NoSuchEntityException(__('form with specified ID "%1" was not found.', $id));
            }
            $this->submit[$id] = $submit;
        }
        return $this->submit[$id];
    }
 


    /**
     * {@inheritdoc}
     */
    public function delete(Data\SubmitInterface $submit)
    {
        try {
            $this->submitResource->delete($submit);
            unset($this->submit[$submit->getId()]);
        } catch (\Exception $e) {
            if ($submit->getId()) {
                throw new CouldNotDeleteException(
                    __('Unable to remove form with ID %1. Error: %2', [$submit->getId(), $e->getMessage()])
                );
            }
            throw new CouldNotDeleteException(__('Unable to remove answer. Error: %1', $e->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($id)
    {
        $model = $this->get($id);
        $this->delete($model);
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        $submitCollection = $this->submitCollectionFactory->create();
        $submitList = [];

        foreach ($submitCollection as $submit) {
            $submitList[] = $submit;
        }

        return $submitList;
    }
}
