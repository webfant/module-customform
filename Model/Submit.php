<?php
/**
 * @author Webfant Team

 *
 */


namespace Webfant\Customform\Model;

use Webfant\Customform\Api\Data\SubmitInterface; 
use Magento\Framework\Exception\NoSuchEntityException;

class Submit extends \Magento\Framework\Model\AbstractModel implements SubmitInterface
{
    /**
     * @var \Magento\Framework\Json\DecoderInterface
     */
    private $jsonDecoder;
 
    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    private $jsonEncoder;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Json\DecoderInterface $jsonDecoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->jsonDecoder = $jsonDecoder; 
        $this->jsonEncoder = $jsonEncoder;
    }

    protected function _construct()
    {
        parent::_construct();
        $this->_init('Webfant\Customform\Model\ResourceModel\Submit');
        $this->setIdFieldName('id');
    }
    
    /* Getters and setters implementation:*/

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->_getData(SubmitInterface::ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->setData(SubmitInterface::ID, $id);

        return $this;
    }
 
    /**
     * {@inheritdoc}
     */
    public function getLastname()
    {
        return $this->_getData(SubmitInterface::LASTNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setLastname($lastname)
    {
        $this->setData(SubmitInterface::LASTNAME, $lastname);

        return $this;
    }
 
    /**
     * {@inheritdoc}
     */
    public function getFirstname()
    {
        return $this->_getData(SubmitInterface::FIRSTNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setFirstname($firstname)
    {
        $this->setData(SubmitInterface::FIRSTNAME, $firstname);

        return $this;
    }
 
 
    /**
     * {@inheritdoc}
     */
    public function getSubmitJson()
    {
        return $this->_getData(SubmitInterface::SUBMIT_JSON);
    }

    /**
     * {@inheritdoc}
     */
    public function setSubmitJson($submitJson)
    {
        $this->setData(SubmitInterface::SUBMIT_JSON, $submitJson);

        return $this;
    } 

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->_getData(SubmitInterface::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(SubmitInterface::CREATED_AT, $createdAt);

        return $this;
    }
 
}
