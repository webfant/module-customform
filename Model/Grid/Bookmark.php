<?php
/**
 * @author Webfant Team

 *
 */


namespace Webfant\Customform\Model\Grid;

use Magento\Ui\Api\BookmarkManagementInterface;
use Magento\Ui\Api\BookmarkRepositoryInterface;
use Magento\Ui\Api\Data\BookmarkInterface;
use Magento\Framework\Json\EncoderInterface;

class Bookmark
{
    /**
     * @var BookmarkManagementInterface
     */
    private $bookmarkManagement;

    /**
     * @var BookmarkRepositoryInterface
     */
    private $bookmarkRepository;

    /**
     * @var EncoderInterface
     */
    private $encoder;

    /**
     * @var BookmarkInterface
     */
    private $bookmark;

    public function __construct(
        BookmarkManagementInterface $bookmarkManagement,
        BookmarkRepositoryInterface $bookmarkRepository,
        EncoderInterface $encoder
    ) {
        $this->bookmarkManagement = $bookmarkManagement;
        $this->bookmarkRepository = $bookmarkRepository;
        $this->encoder = $encoder;
    }

    /**
     * @param string $namespace
     * @return Bookmark $this
     */
    public function load($namespace)
    {
        $this->bookmark = $this->bookmarkManagement->getByIdentifierNamespace(
            BookmarkInterface::CURRENT,
            $namespace
        );

        return $this;
    }

    public function clear()
    {
        $this->bookmark = null;
    }

    /**
     * $filters array like [code => value]
     *
     * @param string $namespace
     * @param array $filters
     */
    public function applyFilter($namespace, $filters)
    {
        
        return false;
        if (!$this->bookmark) {
            $this->load($namespace);
        }

        $config = $this->bookmark->getConfig();
        // clear previous filters
        $config[BookmarkInterface::CURRENT]['filters'] = ['applied' => []];

        foreach ($filters as $filterCode => $filterValue) {
            $this->injectFilter($config, $filterCode, $filterValue);
        }
        $this->bookmark->setConfig($this->encoder->encode($config));

        $this->bookmarkRepository->save($this->bookmark);
    }

    /**
     * @param array $config
     * @param string $filterCode
     * @param string $filterValue
     */
    private function injectFilter(&$config, $filterCode, $filterValue)
    {
        if ($filterValue && isset($config[BookmarkInterface::CURRENT]['filters']['applied'])) {
            $config[BookmarkInterface::CURRENT]['filters']['applied'][$filterCode] = $filterValue;
        }
    }
}
