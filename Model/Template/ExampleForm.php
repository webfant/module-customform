<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Model\Template;

class ExampleForm
{
    private $repeatFields = [
        'group',
        'field',
        'option'
    ];

    private $requiredFields = [
        'title',
        'code',
        'store_id',
        'submit_button'
    ];

    /**
     * @var \Webfant\Customform\Model\FormFactory
     */
    private $form;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Webfant\Customform\Model\FormRepository
     */
    private $formRepository;

    /**
     * @var \Webfant\Customform\Helper\Data
     */
    private $helper;

    public function __construct(
        \Webfant\Customform\Helper\Data $helper,
        \Webfant\Customform\Model\FormRepository $formRepository,
        \Webfant\Customform\Model\FormFactory $form,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->form = $form;
        $this->date = $date;
        $this->formRepository = $formRepository;
        $this->helper = $helper;
    }

    public function createExampleForms()
    {
        $paths = $this->getXmlTemplatesPaths();
        foreach ($paths as $path) {
            $xmlDoc = simplexml_load_file($path);
            $templateData = $this->parseNode($xmlDoc);
            if (count($templateData) == 3) {
                $templateData = array_merge($templateData[0], $templateData[1], $templateData[2]);
            }
            $this->createForm($templateData);
        }
    }

    /**
     * @param \SimpleXMLElement $node
     * @return array|string
     */
    private function parseNode($node)
    {
        $data = [];
        foreach ($node as $keyNode => $childNode) {
            if (is_object($childNode)) {
                if (in_array($keyNode, $this->repeatFields)) {
                    $data[] = $this->parseNode($childNode);
                } else {
                    $data[$keyNode] = $this->parseNode($childNode);
                }
            }
        }

        if (count($node) == 0) {
            $data = (string)$node;
            if ($data == 'true') {
                $data = true;
            }
        }

        return $data;
    }

    /**
     * @param array $data
     */
    private function createForm($data)
    {
        if ($this->isTemplateDataValid($data)) {
            $form = $this->form->create();
            foreach ($data as $key => $item) {
                if (is_array($item)) {
                    $form->setData($key, $this->helper->encode($item));
                } else {
                    $form->setData($key, $item);
                }
            }
            $form->setCreatedAt($this->date->gmtDate());
            $this->formRepository->save($form);
        }
    }

    /**
     * @return array
     */
    private function getXmlTemplatesPaths()
    {
        $p = strrpos(__DIR__, DIRECTORY_SEPARATOR);
        $directoryPath = $p ? substr(__DIR__, 0, $p) : __DIR__;
        $directoryPath .= '/../etc/adminhtml/Example/';

        return glob($directoryPath . '*.xml');
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isTemplateDataValid($data = [])
    {
        $result = true;
        foreach ($this->requiredFields as $fieldName) {
            if (!array_key_exists($fieldName, $data)) {
                $result = false;
            }
        }

        return $result;
    }
}
