<?php
/**
 *
 */
namespace Webfant\Customform\Controller\Grid;


use \Magento\Framework\App\Action\Action;


class Delete extends Action
{
    /** @var  \Magento\Framework\View\Result\Page */
    protected $resultPageFactory;
    
    protected $_session;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Webfant\Customform\Model\SubmitFactory $submitFactory,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->submitFactory = $submitFactory;
        $this->_session = $session;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }
    
    /**
     * 
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        
        if(!$this->_session->isLoggedIn()) {
            $this->_redirect('*/login/index');
           // customer login action
        }
        
        $groupId = $this->_scopeConfig->getValue('customform/account/group_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        $customer = $this->_session->getCustomer();
        if($customer->getGroupId() != $groupId) {
            $this->_redirect('/');
        }
        
        $id = $this->getRequest()->getParam('id');
        $submitModel = $this->submitFactory->create()->load($id);
        $submitModel->delete();
        
        $this->_redirect('inmeet/index/index');
    }
}