<?php

namespace Webfant\Customform\Controller\Form;

use \Magento\Framework\App\Action\Action;

class GeneratePdf extends Action
{
    
    protected $resultPageFactory;
    
    protected $pdfHelper;
    
    public function __construct( 
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Webfant\Customform\Helper\Pdf $pdfHelper
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->pdfHelper = $pdfHelper;
        
    }
     /**
     * 
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute() { 
        
        $pdf = $this->pdfHelper->generate($this->getRequest()->getParam('id'));
        $pdf->Output('inmeet_formulier.pdf', 'I');

    }
    
}