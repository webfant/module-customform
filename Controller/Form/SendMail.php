<?php

namespace Webfant\Customform\Controller\Form;

use \Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class SendMail extends Action
{
    
    protected $resultPageFactory;
    
    protected $pdfHelper;
    
    public function __construct( 
        \Magento\Framework\App\Action\Context $context,
        \Webfant\Customform\Helper\Pdf $pdfHelper,
        \Webfant\Customform\Model\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Customer\Model\Session $session
    ) {
        parent::__construct($context);
        $this->pdfHelper = $pdfHelper;
        $this->transportBuilder = $transportBuilder;
        $this->session = $session;
        
    }
     /**
     * 
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute() { 
        
        $pdf = $this->pdfHelper->generate($this->getRequest()->getParam('id'));
        $content = $pdf->Output('inmeet_formulier.pdf', 'S');
        
        $this->sendEmail($content);
        
        $this->messageManager->addSuccessMessage('Email verstuurd!');
        
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('inmeet');
        
        return $resultRedirect;
        

    }
    
    public function sendEmail($content)
    {
        
        try {
            
            $sender = [
                'name' => 'Bram Webfant',
                'email' => 'bram@webfant.io',
            ];
            
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('webfant_sendpdf_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'customer_name'  => $this->session->getCustomer()->getCustomerName(),
                ])
                ->setFrom($sender)
                ->addTo($this->session->getCustomer()->getEmail())
                ->addAttachment($content, 'Inmeetformulier.pdf', 'application/pdf')
                ->getTransport();
                
            
                
            $transport->sendMessage();
            
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            exit;
        }
        
    }
    
}