<?php
/**
 *
 */

namespace Webfant\Customform\Controller\Form;


use \Magento\Framework\App\Action\Action;


class Index extends Action
{
    /** @var  \Magento\Framework\View\Result\Page */
    protected $resultPageFactory;
    
    protected $_session;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_session = $session;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * 
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        
        if(!$this->_session->isLoggedIn()) {
            $this->_redirect('*/login/index');
           // customer login action
        }
        
        $groupId = $this->_scopeConfig->getValue('customform/account/group_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        $customer = $this->_session->getCustomer();
        if($customer->getGroupId() != $groupId) {
            $this->_redirect('/');
        }
        
        return $this->resultPageFactory->create();
    }
}