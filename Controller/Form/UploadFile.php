<?php
/**
 *
 */

namespace Webfant\Customform\Controller\Form;


use \Magento\Framework\App\Action\Action;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\Framework\App\Filesystem\DirectoryList;
use Webfant\Customform\Helper\Data;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Filesystem\Driver\File;

class UploadFile extends Action
{
     /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;

    /**
     * @var File
     */
    private $fileDriver;
    
    public function __construct( 
        \Magento\Framework\App\Action\Context $context,
        \Webfant\Customform\Model\Template\TransportBuilder $transportBuilder,
        \Webfant\Customform\Model\SubmitFactory $submitFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Filesystem $filesystem,
        File $fileDriver
    ) {
        parent::__construct($context);
        $this->transportBuilder = $transportBuilder;
        $this->submitFactory = $submitFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->filesystem = $filesystem;
        $this->fileDriver = $fileDriver;
    
        
    }
     /**
     * 
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    { 
        
            
        $submitModel = $this->submitFactory->create();    
        $id = $this->getRequest()->getParam('submit_id');
        
        if($id){
            $submitModel = $submitModel->load($id);
        }
        
        $filePath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath()
                . Data::MEDIA_PATH . 'inmeet_images';
        
        $uploader = $this->_fileUploaderFactory->create(['fileId' => 'imagedata']);
        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
        $uploader->setAllowRenameFiles(false);
        $result = $uploader->save($filePath);
        
        $data = ['success' => true];
        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
           
    }
    
    
}