<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Controller\Form;

use Webfant\Customform\Model\Answer;
use Webfant\Customform\Model\Form;
use Webfant\Customform\Model\Form\Element;
use Webfant\Customform\Model\ResourceModel\Form\Element\Option\CollectionFactory as OptionCollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Webfant\Customform\Helper\Data;
use Magento\Framework\App\Filesystem\DirectoryList;
use Webfant\Customform\Api\Data\Form\ElementInterface;
use Webfant\Customform\Api\Data\Form\Element\OptionInterface;
use Webfant\Customform\Model\Form\ElementRepository;
use Magento\Framework\Filesystem\Driver\File;

class Submit extends \Magento\Framework\App\Action\Action
{
    const SUCCESS_RESULT = 'success';

    const ERROR_RESULT = 'error';
	
	
    /**
     * @var \Webfant\Customform\Model\SubmitFactory
     */

    protected $submitModel = array();

    protected $submit_json = array();

    protected $collectionId = array();
    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    private $formKeyValidator;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Webfant\Customform\Helper\Data
     */
    private $helper;

    /**
     * @var \Webfant\Customform\Model\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Webfant\Customform\Model\FormRepository
     */
    private $formRepository;

    /**
     * @var \Webfant\Customform\Model\AnswerFactory
     */
    private $answerFactory;

    /**
     * @var \Webfant\Customform\Model\AnswerRepository
     */
    private $answerRepository;

    /**
     * @var \Webfant\Customform\Model\ResourceModel\Form\Element\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var OptionCollectionFactory
     */
    private $optionCollectionFactory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;

    /**
     * @var File
     */
    private $fileDriver;

   /**
    * @var \Webfant\Customform\Model\SubmitRepository
    */
    private $submitRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Webfant\Customform\Model\FormRepository $formRepository,
        \Webfant\Customform\Model\AnswerRepository $answerRepository,
        \Webfant\Customform\Model\SubmitRepository $submitRepository,
        \Webfant\Customform\Model\SubmitFactory $submitFactory,
        \Webfant\Customform\Model\AnswerFactory $answerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webfant\Customform\Model\Template\TransportBuilder $transportBuilder,
        \Webfant\Customform\Model\ResourceModel\Form\Element\CollectionFactory $collectionFactory,
        OptionCollectionFactory $optionCollectionFactory,
        \Magento\Customer\Model\Session $session,
        Data $helper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        File $fileDriver,
        ElementRepository $elementRepository
    ) {
        parent::__construct($context);
        $this->formKeyValidator = $formKeyValidator;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $logger;
        $this->formRepository = $formRepository;
        $this->submitRepository = $submitRepository;
        $this->answerFactory = $answerFactory;
        $this->submitFactory = $submitFactory;
        $this->answerRepository = $answerRepository;
        $this->collectionFactory = $collectionFactory;
        $this->optionCollectionFactory = $optionCollectionFactory;
        $this->filesystem = $filesystem;
        $this->fileDriver = $fileDriver;
        $this->session = $session;
        $this->elementRepository = $elementRepository;
    }

    public function execute()
    {
	     
        $formIds = $this->getRequest()->getParam('form_id');
	    $freeDrawingImage = $this->getRequest()->getParam('freedrawingimage');
	    $imgFirstName = $this->getRequest()->getParam('firstname');
	    $imgLastName = $this->getRequest()->getParam('lastname');
	    $id = $this->getRequest()->getParam('submit_id');
	    $deffSave = $this->getRequest()->getParam('save_deff');
	    
        $url = Data::REDIRECT_PREVIOUS_PAGE;
        $type = self::ERROR_RESULT;
        $submitModel = $this->submitFactory->create();
        
        foreach($formIds as $formId){
            
            if ($this->getRequest()->isPost() && $formId) {
               
                try {

                    $this->validateData();

                    /** @var Form $formModel */
                    $formModel = $this->formRepository->get($formId);
                   
                    /** @var  Answer $model */
                    $model = $this->answerFactory->create();
                    $answerData = $this->generateAnswerData($formModel); 
                    $model->addData($answerData);           
                     
                    $this->answerRepository->save($model);

                    $type = self::SUCCESS_RESULT;
                    $url = $formModel->getSuccessUrl();
                    if ($url && $url != '/') {
                        $url = trim($url, '/');
                    }
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(
                        $e->getMessage()
                    );
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                    $this->messageManager->addErrorMessage(
                        __('Sorry. There is a problem with Your Form Request. Please try again or use Contact Us link in the menu.')
                    );
                }
            }
        }
		
        $this->submitModel['submit_json'] = $this->helper->encode($this->submit_json);
        $this->submitModel['customer_id'] = $this->session->getCustomer()->getId();
        $this->submitModel['customer_name'] = $this->session->getCustomer()->getName();
        $this->submitModel['status'] = ($deffSave == 'true') ? 'afgerond' : 'open';
        
        if($id){
           $submitModel = $submitModel->load($id);
        }
       
        $submitModel->addData($this->submitModel);  
       
        $this->submitRepository->save($submitModel);
	    $submitId = $submitModel->getData('id');
	    if($freeDrawingImage){
		    $imgName = $imgFirstName.$imgLastName.'_'.$submitId.".jpeg";
		    $this->helper->UploadImage($freeDrawingImage,$imgName);
	    }
	    
	    $this->messageManager->addSuccessMessage('Succesvol opgeslagen');
	    
        if ($this->getRequest()->isAjax()) {
            $response = $this->getResponse()->representJson(
                $this->helper->encode(
                    [
                        'result' => $type,
                        'submit_id' => $submitId
                        
                    ]
                )
            );
        } else {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            if ($url === Data::REDIRECT_PREVIOUS_PAGE) {
                $url = $this->_redirect->getRefererUrl();
            }
            $resultRedirect->setPath('inmeet');

            $response = $resultRedirect;
        }

        return $response;
    }
    
    /**
     * @throws LocalizedException
     */
    private function validateData()
    {
        $data = $this->getRequest()->getParams();
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            throw new LocalizedException(
                __('Form key is not valid. Please try to reload the page.')
            );
        }

        if ($this->helper->isGDPREnabled() && (!isset($data['gdpr']) || !$data['gdpr'])) {
            throw new LocalizedException(__('Please agree to the Privacy Policy'));
        }
    }

    private function generateAnswerData($formModel)
    {
        $json = $this->generateJson($formModel);

        return [
            'form_id'  => $formModel->getId(),
            'store_id' => $this->storeManager->getStore()->getId(),
            'ip'       => $this->helper->getCurrentIp(),
            'customer_id' => (int)$this->helper->getCurrentCustomerId(),
            'response_json' => $json
        ];
    }

    /**
     * @param $formModel
     * @return string
     * @throws LocalizedException
     */
    private function generateJson($formModel)
    {
        $formJson = $formModel->getFormJson();

        $fields = $this->helper->decode($formJson);
        $data = [];
        foreach ($fields as $field) {
	       
            $name = $field['name'];
            $value = $this->getValidValue($field, $name);
            
            if ($value) {
                $inputValue = $value;

                $this->collectionId[] =  $formModel->getId();
                $type = $field['type'];
                
                switch ($type) {
                    case 'checkbox':
                    case 'checkboxtwo':
                    case 'checkboxthree':

                        $data[$name]['label'] = $field['label'];
                        $data[$name]['type'] = $type;
                        $inputValue = $this->helper->escapeHtml($inputValue);
                        $data[$name]['inputvalue'] = $inputValue;
                        break;
                    case 'dropdown':
                    case 'listbox':
                    case 'radio':
                    case 'radiotwo':
                        $nameArray = explode('-', $name);
                        $elementId = (isset($field['entity_id']) && $field['entity_id'])
                            ? $field['entity_id']
                            : end($nameArray);

                        $options = $this->optionCollectionFactory->create()
                            ->getFieldsByElementId($elementId);
                        $tmpValue = [];
                        /** @var \Webfant\Customform\Model\Form\Element\Option $option */
                        foreach ($options as $option) {
                            if (is_array($value) && in_array($option->getValue(), $value)) {
                                $tmpValue[] = $option->getName();
                            }
                        }
                        $data[$name]['inputvalue'] = $inputValue;
                        $data[$name]['value'] = $tmpValue ? implode(', ', $tmpValue) : $value;
                        break;
                    case 'file':
                        $data[$name]['value'] = $value;
                        $data[$name]['inputvalue'] = $inputValue;
                        break;
                    default:
                        $value = $this->helper->escapeHtml($value);
                        $data[$name]['value'] = $value;
                        $data[$name]['inputvalue'] = $inputValue;
                        
                       
                }

                $data[$name]['label'] = $field['label'];
                $data[$name]['type'] = $type;            
                
            }
	       
        } 

        if(array_key_exists("firstname",$data) &&  !in_array($data["firstname"],$this->submitModel)){
            $this->submitModel['firstname'] = $data["firstname"]['value'];
        }
        if(array_key_exists("lastname",$data) &&  !in_array($data["lastname"],$this->submitModel)){
            $this->submitModel['lastname'] = $data["lastname"]['value'];
        }
        $this->submitModel['submit_json'][] = $this->helper->encode($data);

        array_push($this->submit_json,$data);
        return $this->helper->encode($data);
    }

    /**
     * @param $field
     * @param $name
     * @return array|mixed
     * @throws LocalizedException
     */
    private function getValidValue($field, $name)
    {
        $result = $this->getRequest()->getParam($name, '');
        $fileValidation = [];
        $validation = $this->getRow($field, ElementInterface::VALIDATION);
        $fieldType = $this->getRow($field, ElementInterface::TYPE);
        
        $isFile = strcmp($fieldType, 'file') === 0;
	 
        if ($validation && $validation !== 'None') {
            $validation = $this->helper->decode($validation);
            $valueNotExist = (!$isFile && !$result)
                || ($isFile && !array_key_exists($name, $this->getRequest()->getFiles()->toArray()));

            if (!array_key_exists(ElementInterface::REQUIRED, $validation)
                && $valueNotExist
            ) {
                return $result;
            }

            foreach ($validation as $key => $item) {
                switch ($key) {
                    case 'required':
                        if ($result === '' && ($fieldType != 'file')) {
                            if (isset($field['dependency']) && $field['dependency']) {
                                $isHidden = false;
                                foreach ($field['dependency'] as $dependency) {
                                    if (isset($dependency['field'])
                                        && isset($dependency['value'])
                                        && $this->getRequest()->getParam($dependency['field']) != $dependency['value']
                                    ) {
                                        $isHidden = true;
                                    }
                                }

                                if ($isHidden) {
                                    continue;
                                }
                            }

                            $name = isset($field['title']) ? $field['title'] : $field['label'];
                            throw new LocalizedException(__('Please enter a %1.', $name));
                        }
                        break;
                    case 'validation':
                        if ($item == 'validate-email') {
                            $result = filter_var($result, FILTER_SANITIZE_EMAIL);
                            if (!\Zend_Validate::is($result, 'EmailAddress')) {
                                throw new LocalizedException(__('Please enter a valid email address.'));
                            }
                        }
                        break;
                    case 'allowed_extension':
                    case 'max_file_size':
                        $fileValidation[$key] = $item;
                        break;
                }
            }
        }

        if ($isFile && !$this->getRequest()->isAjax()) {
            
            if($this->getRequest()->getParam($name)) {
                return $this->getRequest()->getParam($name);
            }
            
            $result = $this->helper->saveFileField($name, $fileValidation);
        }
	   
        return $result;
    }

    /**
     * @param Form $formModel
     * @param Answer $model
     */
    private function sendAdminNotification(Form $formModel, Answer $model)
    {
        $emailTo = trim($formModel->getSendTo());
        if (!$emailTo) {
            $emailTo = trim($this->helper->getModuleConfig('email/recipient_email'));
        }

        if ($emailTo && $this->helper->getModuleConfig('email/enabled') && $formModel->getSendNotification()) {
            $sender = $this->helper->getModuleConfig('email/sender_email_identity');
            $template = $formModel->getEmailTemplate();
            if (!$template) {
                $template = $this->helper->getModuleConfig('email/template');
            }

            $model->setFormTitle($formModel->getTitle());
            $customerData = $this->helper->getCustomerName($model->getCustomerId());

            try {
                $store = $this->storeManager->getStore();
                $data =  [
                    'website_name'  => $store->getWebsite()->getName(),
                    'group_name'    => $store->getGroup()->getName(),
                    'store_name'    => $store->getName(),
                    'response'      => $model,
                    'link'          => $this->helper->getAnswerViewUrl($model->getAnswerId()),
                    'submit_fields' => $this->getSubmitFields($model),
                    'customer_name' => $customerData['customer_name'],
                    'customer_link' => $customerData['customer_link'],
                ];

                if (strpos($emailTo, ',') !== false) {
                    $emailTo = explode(',', $emailTo);
                }

                $transport = $this->transportBuilder->setTemplateIdentifier(
                    $template
                )->setTemplateOptions(
                    ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $store->getId()]
                )->setTemplateVars(
                    $data
                )->setFrom(
                    $sender
                )->addTo(
                    $emailTo
                )->getTransport();

                $transport->sendMessage();
            } catch (\Exception $e) {
                $this->logger->critical($e);
                $this->messageManager->addErrorMessage(__('Unable to send the email.'));
            }
        }
    }

    /**
     * @param Form $formModel
     * @param Answer $model
     * @throws LocalizedException
     */
    private function sendAutoReply(Form $formModel, Answer $model)
    {
        if (!$this->helper->isAutoReplyEnabled()) {
            return;
        }

        $emailTo = $model->getRecipientEmail();
        if ($emailTo) {
            $sender = $this->helper->getAutoReplySender();
            $template =  $this->helper->getAutoReplyTemplate();

            $model->setFormTitle($formModel->getTitle());
            $customerData = $this->helper->getCustomerName($model->getCustomerId());

            try {
                $store = $this->storeManager->getStore();
                $data =  [
                    'website_name'  => $store->getWebsite()->getName(),
                    'group_name'    => $store->getGroup()->getName(),
                    'store_name'    => $store->getName(),
                    'response'      => $model,
                    'customer_name' => $customerData['customer_name'],
                    'form_name'     => $formModel->getTitle()
                ];

                $transport = $this->transportBuilder->setTemplateIdentifier(
                    $template
                )->setTemplateOptions(
                    ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $store->getId()]
                )->setTemplateVars(
                    $data
                )->setFrom(
                    $sender
                )->addTo(
                    $emailTo
                )->getTransport();

                $transport->sendMessage();
            } catch (\Exception $e) {
                $this->logger->critical($e);
                $this->messageManager->addErrorMessage(__('Unable to send the email.'));
            }
        }
    }

    private function getSubmitFields(Answer &$model)
    {
        $html = '<table cellpadding="7">';
        $formData = $model->getResponseJson();
        
        if ($formData) {
            $fields = $this->helper->decode($formData);

            foreach ($fields as $field) {
                $value = $this->getRow($field, OptionInterface::OPTION_VALUE);
                $fieldType = $this->getRow($field, ElementInterface::TYPE);
                if ($fieldType == 'file') {
                	 
                    $filePath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath()
                        . Data::MEDIA_PATH . Uploader::getCorrectFileName($value);
                    $this->transportBuilder->addAttachment(
                        $this->fileDriver->fileGetContents($filePath),
                        $value
                    );
                }

                $html .= '<tr>' .
                    '<td style="width: 50%;">' . $field['label'] . '</td>' .
                    '<td>' . $value . '</td>' .
                    '</tr>';
            }
          
        }
        $html .= '</table>';

        return $html;
    }

    private function getRow($field, $type)
    {
        return isset($field[$type]) ? $field[$type] : null;
    }
}
