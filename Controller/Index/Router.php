<?php

namespace Webfant\Customform\Controller\Inmeet;

/**
 * @todo class description
 */
class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory

    ) {

        $this->actionFactory = $actionFactory;

    }

    public function match(\Magento\Framework\App\RequestInterface $request)
    {

        $request->setModuleName('customform')
            ->setControllerName('inmeet')
            ->setActionName('grid');


        return $this->actionFactory->create('Magento\Framework\App\Action\Forward');
    }
}
