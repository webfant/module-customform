<?php
/**
 *
 */

namespace Webfant\Customform\Controller\Login;

 

use \Magento\Framework\App\Action\Action;


use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_session;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $session
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_session = $session;
        parent::__construct($context);
    }

    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        if($this->_session->isLoggedIn()) {
            $this->_redirect('*/index/index');
           // customer login action
        }
        return $this->resultPageFactory->create();
    }
}