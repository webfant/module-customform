<?php
	/**
	 * @author Webfant Team
	 */
	
	
	namespace Webfant\Customform\Controller\Adminhtml\Submit;
	
	
	use Magento\Framework\Exception\NoSuchEntityException;
	
	class Delete extends \Webfant\Customform\Controller\Adminhtml\Submit
	{
		protected $_publicActions = ['edit'];
		
		
		/**
		 * {@inheritdoc}
		 */
		protected function _isAllowed()
		{
			return $this->_authorization->isAllowed('Webfant_Customform::delete');
		}
		
		public function execute()
		{
			$id = (int)$this->getRequest()->getParam('id');
			
			try {
				if (!$id) {
					throw new NoSuchEntityException(__('Response was not found.'));
				}
				$model = $this->submitRepository->get($id);
				$model->delete();
				$this->_redirect('*/*/');
				
			} catch (NoSuchEntityException $exception) {
				$this->messageManager->addErrorMessage(__('This Response no longer exists.'));
				$this->_redirect('webfant_customform/submit/index');
				return;
			}
			
			
			
			$this->_initAction();
			$this->_view->getPage()->getConfig()->getTitle()->prepend(
				__('Submitted Data #') . $model->getId()
			);
			
			$this->_view->renderLayout();
		}
	}
