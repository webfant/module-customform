<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Controller\Adminhtml\Submit;

use Webfant\Customform\Controller\Adminhtml\Submit;
use Magento\Framework\Exception\NoSuchEntityException;

class Edit extends Submit
{
    protected $_publicActions = ['edit'];

    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id');
        try {
            if (!$id) {
                throw new NoSuchEntityException(__('Response was not found.'));
            }
            $model = $this->submitRepository->get($id);

        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage(__('This Response no longer exists.'));
            $this->_redirect('webfant_customform/submit/index');
            return;
        }

        $this->coreRegistry->register(Submit::CURRENT_SUBMIT_MODEL, $model);

        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(
            __('Submitted Data #') . $model->getId()
        );

        $this->_view->renderLayout();
    }
}
