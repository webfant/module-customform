<?php
/**
 * @author Webfant Team

 *
 */


namespace Webfant\Customform\Controller\Adminhtml\Submit;

use Webfant\Customform\Api\Data\SubmitInterface;

class Index extends \Webfant\Customform\Controller\Adminhtml\Submit
{
    public function execute()
    {

        if ($id = (int)$this->getRequest()->getParam('id', null)) {
            $this->bookmark->applyFilter(
                'webfant_customform_submit_listing',
                [
                    'id' => $id
                ]
            );
            $this->bookmark->clear();
        }

        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Submitted Data'));
        $this->_view->renderLayout();
    }
}
