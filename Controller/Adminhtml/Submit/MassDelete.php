<?php
	/**
	 * @author Webfant Team
	 */
	
	
	namespace Webfant\Customform\Controller\Adminhtml\Submit;
	
	use Magento\Backend\App\Action\Context;
	use Magento\Ui\Component\MassAction\Filter;
	use Webfant\Customform\Model\ResourceModel\Submit\CollectionFactory;
	use Magento\Framework\Controller\ResultFactory;
	use Magento\Framework\Exception\NoSuchEntityException;
	
	class MassDelete  extends \Magento\Backend\App\Action
	{
		/**
		 * @var Filter
		 */
		protected $filter;
		
		/**
		 * @var CollectionFactory
		 */
		protected $collectionFactory;
		
		/**
		 * @param Context $context
		 * @param Filter $filter
		 * @param CollectionFactory $collectionFactory
		 */
		public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory)
		{
			$this->filter = $filter;
			$this->collectionFactory = $collectionFactory;
			parent::__construct($context);
		}
		
		public function execute()
		{
			
			$collection = $this->filter->getCollection($this->collectionFactory->create());
			$collectionSize = $collection->getSize();
			
			foreach ($collection as $item) {
				$item->delete();
			}
			
			$this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));
			
			/** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			return $resultRedirect->setPath('*/*/');
		}
		
	}
