<?php
/**
 * @author Webfant Team

 *
 */


namespace Webfant\Customform\Controller\Adminhtml\Answer;

use Webfant\Customform\Api\Data\AnswerInterface;

class Index extends \Webfant\Customform\Controller\Adminhtml\Answer
{
    public function execute()
    {
        if ($formId = (int)$this->getRequest()->getParam('form_id', null)) {
            $this->bookmark->applyFilter(
                'webfant_customform_answer_listing',
                [
                    'form_id' => $formId,
                    AnswerInterface::ADMIN_RESPONSE_STATUS => $this->getRequest()->getParam('status', null)
                ]
            );
            $this->bookmark->clear();
        }

        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Submitted Data'));
        $this->_view->renderLayout();
    }
}
