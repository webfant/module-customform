<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Controller\Adminhtml;

use Magento\Framework\View\Result\PageFactory;
use Webfant\Customform\Model\Grid\Bookmark;

/**
 *  controller
 */
abstract class Submit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webfant_Customform::data';

    const CURRENT_SUBMIT_MODEL = 'webfant_customform_request_model';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Webfant\Customform\Model\AnswerRepository
     */
    protected $submitRepository;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Bookmark
     */
    protected $bookmark;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Webfant\Customform\Model\SubmitRepository $submitRepository,
        \Magento\Framework\Registry $coreRegistry,
        PageFactory $resultPageFactory,
        \Psr\Log\LoggerInterface $logger,
        Bookmark $bookmark
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->submitRepository = $submitRepository;
        $this->logger = $logger;
        $this->resultPageFactory = $resultPageFactory;
        $this->bookmark = $bookmark;
    }

    /**
     * Initiate action
     *
     * @return $this
     */
    protected function _initAction()
    {
        $this->_view->loadLayout();
        $this->_setActiveMenu(self::ADMIN_RESOURCE)
            ->_addBreadcrumb(__('Webfant: Custom Forms'), __('Submitted Data'));

        return $this;
    }
}
