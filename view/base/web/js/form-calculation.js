define([
	"jquery",
	'jquery/ui',
	'Webfant_Customform/js/jquery.canvasjs.min',
	'Webfant_Customform/js/form-canvas',
], function ($, ui, fabric, canvasBuild) {

	return {

		_create: function () {

			// set variables to global
			// @var this.glasspie is hoogte achterzijde spie
			// @var this.vvk_linkerzijkant_berekende_lengte  is VVK linkerzijkant berekende lengte
			this.classes = [];
			this.correctie = (this.correctie) ? this.correctie : 0;
			this.diepte = (this.diepte) ? this.diepte : 0;
			this.breedte = 0;
			this.HoogteMuur = 0;
			this.profiline = 0;
			this.greenline = 0;
			this.terrasVerval = 0;
			this.typeProductDependencyValue = '';
			this.h_onderkant_muur = 0;
			this.afschot_veranda = 0;
			this.standaard_afschot = 0;
			this.lengte_staanders = 0;
			this.omWarning = false;
			this.hoogte_onderkant_goot = 0;
			this.xs_gootHoogte = 0;
			this.xs_gootBreedte = 0;
			this.glasspie = 0;
			this.corr_lengtemt_glasspie = 0;
			this.corr_lengtemt_pc_spie = 0;
			this.corr_lengtemt_glasspie_recht = 0;
			this.corr_lengtemt_pc_spie_recht = 0;
			this.corrLengtemt = 0;
			this.corrLengtemtLinks = 0;
			this.corrLengtemtRechts = 0 ;
			this.uitvuldiepte_zijwanden_links = 0;
			this.uitvuldiepte_zijwanden_rechts = 0;
			this.uitvulling_muurprofiel = 0;
			this.uitvulling_muurprofiel_rechts = 0;
			this.extra_diepte_overstek_zijwanden = 0;
			this.extra_diepte_overstek_zijwanden_rechtspie = 0;
			this.onderzijde_gootHoogte = 0;
			this.onderzijde_gootBreedte = 0;
			this.voorwandTot = 0;
			this.voorwandVanaf = 0;
			this.voorwandCorrlengtemt = 0;
			this.doorloophoogte_voorzijde = (this.doorloophoogte_voorzijde) ? this.doorloophoogte_voorzijde : 0;
			this.vvk_linkerzijkant_berekende_lengte = 0;
			this.aantal_staanders = (this.aantal_staanders) ? this.aantal_staanders : 0;
			this.fundatie_voorzijdeHoogte = 0;
			this.fundatie_voorzijdeBreedte = 0;
			this.fundatie_voorzijdeCorrlengtemt = 0;
			this.zijwand_rechtsVan = 0;
			this.zijwand_rechtsTot = 0;
			this.zijwand_rechtsCorrlengtemt = 0;
			this.zijwand_linksTot = 0;
			this.zijwand_linksvan = 0;
			this.zijwand_linksCorrlengtemt = 0;
			
			this.hoogteVoorzijdeSpieLinks = 0;
			this.hoogteVoorzijdeSpieRechts = 0;
			this.minHoogteVoorzijdeSpieLinks = 0;
			this.minHoogteVoorzijdeSpieRechts = 0;
			
			this.vvk_linkerzijkantHoogte = 0;
			this.vvk_linkerzijkantBreedte = 0;
			
			this.vvk_rechterzijkantHoogte = 0;
			this.vvk_rechterzijkantBreedte = 0;
			
			this.fundatie_linkerzijkantHoogte = 0;

			this.fundatie_linkerzijkantBreedte = 0;
			this.fundatie_rechterzijkantHoogte = 0;
			
			this.tussenAanbouw = 0;
			this.maatvoering_tussen_aanbouw = 0;
			
			this.type_zijwand_berekende_hoogte = 0;

			this.fundatie_rechterzijkantBreedte = 0;
			this.hoogteonderkantgootIgnore = true;
			this.zijdlingse_montage = 0;
			this.corr_breedtemaat = (this.corr_breedtemaat) ? this.corr_breedtemaat : 0;
			this.corr_dieptemaat = 0;
			this.corr_vrijehoogte = 0;
			this.diepte_deur_raam = 0;
			this.hoogte_deur_raam = 0;
			this.doorloophoogte_deur_raam = 0;
			this.doorloophoogteAction = '';
			this.hoogte_hwa_uitloop = 0;
			this.afstand_hwa_uitloop = 0;
			this.hoofproduct_hoogte_muurprofiel = 0;
			this.staander_breedte = 0;
			this.staander_diepte = 0;
			
			this.dakvlakken_bruto_breedte = 0;
			this.dakvlakken_netto_breedte = 0;
			this.dakvlakken_correction = 0;
			this.aantal_dakvlakken = 0;
			this.dakvlak_type = 0;
			this.dakvlakkenEven = 0;
			this.dakbeplating_breedte = 0;
			this.dakbeplating_diepte = 0;
			this.type_glasAantal = 0;
			this.type_glasCorrlengtemt = 0;
			this.bestelmaat_diepte_diepte_veranda = 0;
			this.zonnedoeken_breedte = (this.zonnedoeken_breedte) ? this.zonnedoeken_breedte : 0;
			this.dakplat_netto_breedte = (this.dakplat_netto_breedte) ? this.dakplat_netto_breedte : 0;
			this.canvas = false;
			this.aantal_spots = 0;
			this.led_verlichting = 0;
			this.lights_value = 0;
			this.glasspie_recht = 0;
			this.afschot_veranda_recht = 0;
			this.spie_berekende_lengte_recht = 0;
			this.hoogtehwauitloop = 0;
			this.spie_uitvullen_onder_zijwand = false;
			this.spie_uitvullen_onder_zijwand_recht = false;
			// warninge variable
			this.profilinewarning = false;
			this.hwauitloopwaring = false;
			this.typevoorwandwarning = false;
			this.spiezijkantwarning = false;
			this.zonneweringwarning = false;
			this.aantal_staanderswarning = false;

			this.aanzichtenJson = {
				'left': '',
				'left_top': '',
				'front': '',
				'right': '',
				'right_top': '',
				'staanders': '',
				'fundering_left': '',
				'fundering_front': '',
				'fundering_right': '',
			};

			this.allowedCalculation = false;

			console.clear();
			
			// set all function for init
			this.initCalculation();
			this.setEventsInput();
			this.setEventsDropdown();
			this.setClickEvent();
			this.initAanzichtEvents();
			this.allowedCalculation = true;
			this.calculation();
			canvasBuild._krinners(this);
			canvasBuild._signature(this);
			
			this.warnings('extra_hwa_toevoegen');


		},
		initCalculation: function () {

			// init variable: getting all the pre fill values these values wont change within de code;
			this.correctie = parseFloat(document.getElementById('correctie_tussen').value);
			this.profiline = parseFloat(document.getElementsByClassName('profiline')[0].value);
			this.greenline = parseFloat(document.getElementsByClassName('greenline')[0].value);
			this.breedte = parseFloat(document.getElementsByClassName('afmeeting-breedte')[0].value);
			this.diepte = parseFloat(document.getElementsByClassName('afmeeting-diepte')[0].value);
			this.HoogteMuur = parseFloat(document.getElementsByClassName('h_onderkant_muur')[0].value);
			this.hoogte_onderkant_goot = parseFloat(document.getElementsByClassName('hoogte_onderkant_goot')[0].value);
			this.hoogte_hwa_uitloop = parseFloat(document.getElementsByClassName('hoogte_hwa_uitloop')[0].value);
			// $(document.getElementsByClassName('afmeeting-breedte')[0]).trigger('change');
			// $(document.getElementsByClassName('dakvlak_type')[0]).trigger('change');
			this.glasspie_recht = parseFloat(document.getElementsByClassName('hoogte_spie_rechts')[0].value);
			this.glasspie = parseFloat(document.getElementsByClassName('hoogte_spie_links')[0].value);
			this.hoogteVoorzijdeSpieRechts = parseFloat(document.getElementsByClassName('hoogte_voorzijde_spie_rechts')[0].value);
			this.hoogteVoorzijdeSpieLinks = parseFloat(document.getElementsByClassName('hoogte_voorzijde_spie_links')[0].value);
			this.uitvulling_muurprofiel_rechts = parseFloat(document.getElementsByClassName('uitvulling_muurprofiel_rechts')[0].value);
			this.uitvulling_muurprofiel = parseFloat(document.getElementsByClassName('uitvulling_muurprofiel')[0].value);
			this.extra_diepte_overstek_zijwanden = parseFloat(document.getElementsByClassName('extra_diepte_overstek_zijwanden')[0].value);
			this.extra_diepte_overstek_zijwanden_rechtspie = parseFloat(document.getElementsByClassName('extra_diepte_overstek_zijwanden_rechts')[0].value);

			var self = this;
				
			document.getElementsByClassName('hoogte_hwa_uitloop')[0].addEventListener('keyup', function () {
				self.hoogte_hwa_uitloop = parseFloat($(this).val());
				self.calculation();
			});	
				
			let light_plan = document.getElementsByClassName("light_plan")[0];
	    	if(light_plan.value.length > 0){
	    	 
	    		$('#light_plan_image').attr('src',BASE_URL +"media/webfant/amcustomform/inmeet_images/"+light_plan.value);
	    		$('#light_plan_image').css('border',"2px black solid");
	    		$('#build-light-plan').css('display','none');
	    		$('#build-edit-light').css('display','block');
	    		$('#light_plan').closest('div.canvas-field').css('display','none');
	    		  
	    	}
	    	
	    	 let krinner_map = document.getElementsByClassName("krinner_map")[0];
	    	   
	    	 if(krinner_map.value.length > 0){
	    	 
	    		$('#krinner_map_image').attr('src',BASE_URL +"media/webfant/amcustomform/inmeet_images/"+krinner_map.value);
	    		$('#krinner_map_image').css('border',"2px black solid");
	    		$('#add-krinner').css('display','none');
	    		$('#add-krinner-edit').css('display','block');
	    		$('#krinner_map_canvas').closest('div.canvas-field').css('display','none');
	    	}
	    	
	    	let freedrawing_image = document.getElementsByClassName("freedrawing_image")[0];
	    	   
	    	 if(freedrawing_image.value.length > 0){
	    	 
	    		$('#freedrawing_image_image').attr('src',BASE_URL +"media/webfant/amcustomform/inmeet_images/"+freedrawing_image.value);
	    		$('#freedrawing_image_image').css('border',"2px black solid");
	    		$('#build-free-drawing').css('display','none');
	    		$('#build-edit-drawing').css('display','block');
	    		$('#free_drawing').closest('div.canvas-field').css('display','none');
	    		  
	    	}
	    	
	    	let signature_image = document.getElementsByClassName("signature")[0];
	    	   
	    	 if(signature_image.value.length > 0){
	    	 
	    		$('#signature_image').attr('src',BASE_URL +"media/webfant/amcustomform/inmeet_images/"+signature_image.value);
	    		$('#signature_image').css('border',"2px black solid");
	    		$('#build-signature').css('display','none');
	    		$('#build-edit-signature').css('display','block');
	    		$('#canvas_signature').closest('div.canvas-field').css('display','none');
	    		  
	    	}
	    	
	    	
			$('#build-light-plan').on('click', function() {
					$('#light_plan_opslaan').css('display','none');
				canvasBuild._lightPlan(self);
			});
			
			
			$('#build-free-drawing').on('click', function() {
					$('#freedrawing_image_opslaan').css('display','none');
				canvasBuild._freeDrawing(self);
			});
			$('#button-formfield-all').on('click', function () {
				canvasBuild._create(self);
			});
			
			
			 $('#build-edit-drawing').on('click', function(){
		      
		     	$('#build-free-drawing').css('display','block');
	    		$('#build-edit-drawing').css('display','none');
	    		$('#freedrawing_image_image').attr('src', '').css('border', '');
	    		$('#free_drawing').closest('div.canvas-field').css('display','block');
	    		document.getElementsByClassName("freedrawing_image")[0].value='';
		     	canvasBuild._freeDrawing(self);
		     });
		     
		     $('#build-edit-light').on('click', function(){
		     	$('#build-light-plan').css('display','block');
	    		$('#build-edit-light').css('display','none');
	    		$('#light_plan_image').attr('src', '').css('border', '');
	    		$('#light_plan').closest('div.canvas-field').css('display','block');
	    	 
	    		document.getElementsByClassName("light_plan")[0].value='';
		     	canvasBuild._lightPlan(self);
		     });
		     
		     $('#add-krinner-edit').on('click', function(){
		     	
			     $('#krinner_map_image').attr('src', '').css('border', '');
			     $('#krinner_map_canvas').closest('div.canvas-field').css('display','block');
		         $('#add-krinner').css('display','block');
		         $('#add-krinner-edit').css('display','none');
			     canvasBuild._krinners(self);
	    		// $('#build-edit-light').css('display','none');
	    		// $('#light_plan_image').attr('src', '').css('border', '');
	    		// $('#light_plan').closest('div.canvas-field').css('display','block');
	    	 
	    		// document.getElementsByClassName("light_plan")[0].value='';
		     
		     });
		     
		     $('#build-edit-signature').on('click', function(){
		      
		     	$('#build-signature').css('display','block');
	    		$('#build-edit-signature').css('display','none');
	    		$('#signature_image').attr('src', '').css('border', '');
	    		$('#canvas_signature').closest('div.canvas-field').css('display','block');
	    		document.getElementsByClassName("signature")[0].value='';
		     	canvasBuild._signature(self);
		     });
		     
			this.initValidation();
			
			// .toFixed(2)
		},
		
		initAanzichtEvents: function() {
			
			/* global aanzichtenImages */
			/* global inmeetImageUrl */
			
			var self = this;
			
			$('.aanzicht_left').on('change', function() {
				
				var typeWand = $(this).val();
				if(typeWand == 'option-1') return;
				
				if(typeWand.indexOf('gsw') != -1) {
					
					var typeWandImage = inmeetImageUrl + typeWand + '_left.png';
					$('#aanzicht_left .bottom-part').css('height', '230px');
					$('#aanzicht_left .top-part').css('height', '70px');
					$('#aanzicht_left .bottom-part').css('margin-top', '-7px');
					$('#aanzicht_left .bottom-part').attr('src', typeWandImage).css('visibility', 'visible');
					$('#gsw_direction_overlay_left img').css('visibility', 'visible');
					
					self.aanzichtenJson.left = typeWand + '_left.png';
					
					
				} else if(typeWand) {
					var typeWandImage = inmeetImageUrl + typeWand + '_left.png';
					$('#aanzicht_left .bottom-part').css('height', '300px');
					$('#aanzicht_left .top-part').css('height', '0px');
					$('#aanzicht_left .bottom-part').css('margin-top', '-18px');
					
					$('#aanzicht_left .bottom-part').attr('src', typeWandImage).css('visibility', 'visible');
					$('#gsw_direction_overlay_left img').css('visibility', 'hidden');
					
					self.aanzichtenJson.left = typeWand + '_left.png';
					self.aanzichtenJson['left_top'] = '';
					
				} else {
					$('#aanzicht_left .bottom-part').attr('src', '').css('visibility', 'hidden');
					$('#gsw_direction_overlay_left img').css('visibility', 'hidden');
					self.aanzichtenJson.left = '';
					self.aanzichtenJson['left_top'] = '';
				}
				
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				$('.aanzichten_json').val(aanzichtenImages);
				
			}).change();
			
			$('.aanzicht_front').on('change', function() {
				
				var typeWand = $(this).val();
				if(typeWand == 'option-1') return;
				
				if(typeWand) {
					var typeWandImage = inmeetImageUrl + typeWand + '_front.png';
					$('#aanzicht_front img').attr('src', typeWandImage).css('visibility', 'visible');
					self.aanzichtenJson.front = typeWand + '_front.png';
				} else {
					$('#aanzicht_front img').attr('src', '').css('visibility', 'hidden');
					self.aanzichtenJson.front = '';
				}
				
				if(typeWand.indexOf('gsw') != -1) {
					$('#gsw_direction_overlay_front img').css('visibility', 'visible');
				} else {
					$('#gsw_direction_overlay_front img').css('visibility', 'hidden');
				}
				
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				$('.aanzichten_json').val(aanzichtenImages);
				
			}).change();
			
			$('.aanzicht_right').on('change', function() {
				
				var typeWand = $(this).val();
				if(typeWand == 'option-1') return;
				
				if(typeWand.indexOf('gsw') != -1) {
					
					var typeWandImage = inmeetImageUrl + typeWand + '_right.png';
					$('#aanzicht_right .bottom-part').css('height', '230px');
					$('#aanzicht_right .top-part').css('height', '70px');
					$('#aanzicht_right .bottom-part').css('margin-top', '-7px');
					$('#aanzicht_right .bottom-part').attr('src', typeWandImage).css('visibility', 'visible');
					$('#gsw_direction_overlay_right img').css('visibility', 'visible');
					
					self.aanzichtenJson.right = typeWand + '_right.png';
					
				} else if(typeWand) {
					var typeWandImage = inmeetImageUrl + typeWand + '_right.png';
					$('#aanzicht_right .bottom-part').css('height', '300px');
					$('#aanzicht_right .top-part').css('height', '0px');
					$('#aanzicht_right .bottom-part').css('margin-top', '-18px');
					
					$('#aanzicht_right .bottom-part').attr('src', typeWandImage).css('visibility', 'visible');
					$('#gsw_direction_overlay_right img').css('visibility', 'hidden');
					
					self.aanzichtenJson.right = typeWand + '_right.png';
					self.aanzichtenJson['right_top'] = '';
					
				} else {
					$('#aanzicht_right .bottom-part').attr('src', '').css('visibility', 'hidden');
					$('#gsw_direction_overlay_right img').css('visibility', 'hidden');
					self.aanzichtenJson.right = '';
					self.aanzichtenJson['right_top'] = '';
				}
				
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				$('.aanzichten_json').val(aanzichtenImages);
				
			}).change();
			
			$('.aantal_staanders').on('change', function() {
				
				var aantal = $(this).val();
				if(aantal >= 3 && aantal <= 5) {
					var frontImage = inmeetImageUrl + aantal + '_staanders_front.png';
					$('#staander_overlay_front img').attr('src', frontImage).css('visibility', 'visible');
					self.aanzichtenJson['staanders'] = aantal + '_staanders_front.png';
				} else {
					$('#staander_overlay_front img').attr('src', '').css('visibility', 'hidden');
					self.aanzichtenJson['staanders'] = '';
				}
				
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				$('.aanzichten_json').val(aanzichtenImages);
				
			}).change();
			
			$('.spie-l').on('change', function() {
				
				var pcSpieImage = inmeetImageUrl + 'pc_spie_left.png';
				var glasSpieImage = inmeetImageUrl + 'glas_spie_left.png';
				
				if($(this).val() == 'option-1') {
					$('#aanzicht_left .top-part').attr('src', '').css('visibility', 'hidden');
					self.aanzichtenJson['left_top'] = '';
				} else if($(this).val() == 'option-2') {
					$('#aanzicht_left .top-part').attr('src', glasSpieImage).css('visibility', 'visible');
					self.aanzichtenJson['left_top'] = 'glas_spie_left.png';
				} else {
					$('#aanzicht_left .top-part').attr('src', pcSpieImage).css('visibility', 'visible');
					self.aanzichtenJson['left_top'] = 'pc_spie_left.png';
				}
				
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				$('.aanzichten_json').val(aanzichtenImages);
				
			}).change();
			
			$('.spie-r').on('change', function() {
				
				var pcSpieImage = inmeetImageUrl + 'pc_spie_right.png';
				var glasSpieImage = inmeetImageUrl + 'glas_spie_right.png';
				
				if($(this).val() == 'option-1') {
					$('#aanzicht_right .top-part').attr('src', '').css('visibility', 'hidden');
					self.aanzichtenJson['right_top'] = '';
				} else if($(this).val() == 'option-2') {
					$('#aanzicht_right .top-part').attr('src', glasSpieImage).css('visibility', 'visible');
					self.aanzichtenJson['right_top'] = 'pc_spie_right.png';
				} else {
					$('#aanzicht_right .top-part').attr('src', pcSpieImage).css('visibility', 'visible');
					self.aanzichtenJson['right_top'] = 'glas_spie_right.png';
				}
				
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				$('.aanzichten_json').val(aanzichtenImages);
				
			}).change();
			
			$('.gsw_direction_left').on('change', function() {
				
				var dirImage = inmeetImageUrl + 'gsw_direction_' + $(this).val() + '.png';
				$('#gsw_direction_overlay_left img').attr('src', dirImage);
				
			}).change();
			
			$('.gsw_direction_front').on('change', function() {
				
				var dirImage = inmeetImageUrl + 'gsw_direction_' + $(this).val() + '.png';
				$('#gsw_direction_overlay_front img').attr('src', dirImage);
				
			}).change();
			
			$('.gsw_direction_right').on('change', function() {
				
				var dirImage = inmeetImageUrl + 'gsw_direction_' + $(this).val() + '.png';
				$('#gsw_direction_overlay_right img').attr('src', dirImage);
				
			}).change();
			
		},
		
		initValidation: function() {
		
			var self = this;
		
			window.setInterval(function() { self.validateSteps(); }, 1000);
			
		},
		
		validateSteps: function() {
			
			var self = this;
		
			$('.rendered-form.amform-form').each(function(index, value) {
				
				var validationPassed = true;
				
				$(value).find('div.field').each(function(index, value) {
					
					var fieldDiv = $(value);
					
					if(fieldDiv.css('display') == 'none') return;
					
					if(!self.validateField(fieldDiv)) {
						validationPassed = false;
						return false;
					}
					
					if($(fieldDiv).hasClass('validation-fail')) {
						$(fieldDiv).removeClass('validation-fail');
						$(fieldDiv).find('.error-message').remove();
					}
					
				});
				
				if(validationPassed) {
					$(value).find('.step-validation').addClass('passed');
				} else {
					$(value).find('.step-validation').removeClass('passed');
				}
				
			});
			
			return false;
			
		},
		
		validateDeff: function() {
		
			var self = this;
			var scrollFound = false;
			
			$('.rendered-form.amform-form').each(function(index, value) {
				
				$(value).find('div.field').each(function(index, value) {
					
					var fieldDiv = $(value);
					
					if(fieldDiv.css('display') == 'none') return;
					
					if(!self.validateField(fieldDiv)) {
						
						if(!$(fieldDiv).hasClass('validation-fail')) {
							$(fieldDiv).append('<span class="error-message">Dit veld is verplicht</span>');
							$(fieldDiv).addClass('validation-fail');
						}
						
						if(!scrollFound) {
						    $([document.documentElement, document.body]).animate({
						        scrollTop: $(fieldDiv).offset().top
						    }, 2000);
						    
						    scrollFound = true;
						}
						
					}
					
				});
				
				
			});
			
			return !scrollFound;
			
		},
		
		validateField: function(fieldDiv) {
			
			if(fieldDiv.hasClass('fb-text') || fieldDiv.hasClass('fb-number')) {
				
				return (fieldDiv.find('input').val().length > 0 || fieldDiv.find('input').attr('optional'));
				
			} else if(fieldDiv.hasClass('fb-checkbox') || fieldDiv.hasClass('fb-radio')){
			
				return (fieldDiv.find('input:checked').length || fieldDiv.find('input').attr('optional'));
				
			} else if(fieldDiv.hasClass('fb-select')) {
				
				return (fieldDiv.find('select').val().length > 0 || fieldDiv.find('select').attr('optional'));
				
			} else if(fieldDiv.hasClass('fb-file')) {

				return (fieldDiv.find('input').val().length > 0 || fieldDiv.find('img').length > 0 || fieldDiv.find('input').attr('optional'));
				
			} else {
				
				return true;
				
			}
			
			
		},

		initChangeFields: function () {
			// corr lengtemt for spie
			this.corr_lengtemt_glasspie = parseFloat(document.getElementsByClassName('corr_lengtemt_glasspie')[0].value);
			this.corr_lengtemt_pc_spie = parseFloat(document.getElementsByClassName('corr_lengtemt_pc_spie')[0].value);

			this.corr_lengtemt_glasspie_recht = parseFloat(document.getElementsByClassName('corr_lengtemt_glasspie_rechts')[0].value);
			this.corr_lengtemt_pc_spie_recht = parseFloat(document.getElementsByClassName('corr_lengtemt_pc_spie_rechts')[0].value);

			this.setStatement(); 
		},

		setEventsDropdown: function () {

			var self = this;
			
			// option spie linkerkant
			var spieLChange = function () {
				if ($(this).val() == 'option-1') {
					$("#spie_option_fields").hide();
					$('.doorloophoogte_links').hide();
					$('.hoogte_achterzijde_links').show();
				} else {
					$("#spie_option_fields").show();
					$('.doorloophoogte_links').show();
					$('.hoogte_achterzijde_links').hide();
				}
			};
			$('.spie-l').on('change', spieLChange);

			// option spie rechterkant
			var spieRChange = function () {
				if ($(this).val() == 'option-1') {
					$("#spie_option_fields_rechts").hide();
					$('.doorloophoogte_rechts').hide();
					$('.hoogte_achterzijde_rechts').show();
				} else {
					$("#spie_option_fields_rechts").show();
					$('.doorloophoogte_rechts').show();
					$('.hoogte_achterzijde_rechts').hide();
				}
			};
			$('.spie-r').on('change', spieRChange);

			// option onderzijde goot
			var vvkOnderZijdeGootChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$("#onderzijde_goot_option_fields").hide();
				} else {
					$("#onderzijde_goot_option_fields").show();
				}
				
				var dependency = elem.attr('name').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();
				
				var hoogteFound = false;
				
				if (dependencyvalue) {
					var value = $('input[dependency-field="' + dependency + '');
					value.each(function (index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue && $(value).hasClass('calculationhidden')) {
							hoogteFound = true;
							self.onderzijde_gootHoogte = parseFloat($(value).attr('max'));
							self.onderzijde_gootBreedte = parseFloat($(value).attr('min'));
						}
					});
				}
				
				if(!hoogteFound) {
					self.onderzijde_gootHoogte = 0;
					self.onderzijde_gootBreedte = 0;
				}
			 
				self.calculation();
				
			};
			$('.vvk_onderzijde_goot').on('change', vvkOnderZijdeGootChange);
			vvkOnderZijdeGootChange($('.vvk_onderzijde_goot'));
			
			var vvkLinkerzijkantChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$("#vvk_linkerzijkant_option_fields").hide();
				} else {
					$("#vvk_linkerzijkant_option_fields").show();
				}
				
				var dependency = elem.attr('name').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();
				
				if (dependencyvalue) {
					var value = $('input[dependency-field="' + dependency + '');
				
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue) {
							self.vvk_linkerzijkantHoogte = parseFloat($(value).attr('max'));
							self.vvk_linkerzijkantBreedte = parseFloat($(value).attr('min'));
						}
						 
					});
				}
				self.calculation();
				self.warnings('spie_links_goot');
				
			};
			$('.vvk_linkerzijkant').on('change', vvkLinkerzijkantChange);
			vvkLinkerzijkantChange($('.vvk_linkerzijkant'));
			
			var vvkRechterzijkantChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$("#vvk_rechterzijkant_option_fields").hide();
				} else {
					$("#vvk_rechterzijkant_option_fields").show();
				}
				
				var dependency = elem.attr('name').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();
				
				if (dependencyvalue) {
					var value = $('input[dependency-field="' + dependency + '');
				
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue) {
							self.vvk_rechterzijkantHoogte = parseFloat($(value).attr('max'));
							self.vvk_rechterzijkantBreedte = parseFloat($(value).attr('min'));
						}
						 
					});
				}
				self.calculation();
				self.warnings('spie_rechts_goot');
				
			};
			$('.vvk_rechterzijkant').on('change', vvkRechterzijkantChange)
			vvkRechterzijkantChange($('.vvk_rechterzijkant'));

			var typeVoorwandChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}				
				
				if (elem.val() == 'option-1') {
					$("#type_voorwand_option_fields").hide();
				} else {
					$("#type_voorwand_option_fields").show();
				}
				
				var dependency = elem.attr('name').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();
				
				var voorwandName = elem.find('option:selected').text();
				$('.type_voorwand_naam').html(voorwandName);
				
				if (dependencyvalue) {
					var value = $('input[dependency-field="' + dependency + '');
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue) {
							self.voorwandTot = parseFloat($(value).attr('max'));
							self.voorwandVanaf = parseFloat($(value).attr('min'));
							self.voorwandCorrlengtemt = parseInt($(value).attr('corrlengtemt'));
						}
					});
				}
				
				var value = $('input[dependency-field="' + dependency + '"][dependency-value="' + dependencyvalue + '"]');
				if (value.length > 0) {
					let valMin = $(value).attr('min');

					//max breedte
					let valMax = $(value).attr('max');

					// warning geven
					if (valMax < self.breedte) {
						self.warnings('type_voorwand');
					}
				}
				
				self.calculation();
				self.warnings('type_voorwand_maat');
				
			};
			$('.type_voorwand').on('change', typeVoorwandChange);
			typeVoorwandChange($('.type_voorwand'));

			// option VVK fundatie voorzijde
			var vvkFundatieVoorzijdeChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$("#vvk_fundatie_voorzijde_option_fields").hide();
					$('.aanzicht_vvk_fundatie_voor span').html('');
					$('.aanzicht_vvk_fundatie_voor').css('visibility', 'hidden');
					self.aanzichtenJson['fundering_front'] = '';
				} else {
					$("#vvk_fundatie_voorzijde_option_fields").show();
					var optionText = elem.find('option:selected').text();
					$('.aanzicht_vvk_fundatie_voor span').html(optionText);
					$('.aanzicht_vvk_fundatie_voor').css('visibility', 'visible');
					self.aanzichtenJson['fundering_front'] = optionText;
				}
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				
				var dependency = elem.attr('name').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();

				if (dependencyvalue) {
					var value = $('input[dependency-field="' + dependency + '');
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue) {
							self.fundatie_voorzijdeHoogte = parseFloat($(value).attr('max'));
							self.fundatie_voorzijdeBreedte = parseFloat($(value).attr('min'));
							self.fundatie_voorzijdeCorrlengtemt = parseFloat($(value).attr('corrlengtemt'));
						}
					});
				}
				self.calculation();
				
			};
			$('.vvk_fundatie_voorzijde').on('change', vvkFundatieVoorzijdeChange);
			vvkFundatieVoorzijdeChange($('.vvk_fundatie_voorzijde'));
			
			// option VVK fundatie voorzijde
			var fundatieLinkerzijkantChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$('.aanzicht_vvk_fundatie_links span').html('');
					$('.aanzicht_vvk_fundatie_links').css('visibility', 'hidden');
					$('#vvk_fundatie_links').hide();
					self.aanzichtenJson['fundering_left'] = '';
				} else {
					var optionText = elem.find('option:selected').text();
					$('.aanzicht_vvk_fundatie_links span').html(optionText);
					$('.aanzicht_vvk_fundatie_links').css('visibility', 'visible');
					$('#vvk_fundatie_links').show();
					self.aanzichtenJson['fundering_left'] = optionText;
				}
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				
				var dependency = elem.attr('action').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();

				if (dependencyvalue) {
					var value = $('input[action="' + dependency + '');
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue) {
							self.fundatie_rechterzijkantHoogte = parseFloat($(value).attr('max'));
							self.fundatie_rechterzijkantBreedte = parseFloat($(value).attr('min'));
						}
					});
				}
				self.calculation();
				
			};
			$('.fundatie_linkerzijkant').on('change', fundatieLinkerzijkantChange);
			fundatieLinkerzijkantChange($('.fundatie_linkerzijkant'));
			
			// option VVK fundatie voorzijde
			var fundatieRechterzijkantChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$('.aanzicht_vvk_fundatie_rechts span').html('');
					$('.aanzicht_vvk_fundatie_rechts').css('visibility', 'hidden');
					$('#vvk_fundatie_rechts').hide();
					self.aanzichtenJson['fundering_right'] = '';
				} else {
					var optionText = elem.find('option:selected').text();
					$('.aanzicht_vvk_fundatie_rechts span').html(optionText);
					$('.aanzicht_vvk_fundatie_rechts').css('visibility', 'visible');
					$('#vvk_fundatie_rechts').show();
					self.aanzichtenJson['fundering_right'] = optionText;
				}
				var aanzichtenImages = JSON.stringify(self.aanzichtenJson);
				
				var dependency = elem.attr('action').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();

				if (dependencyvalue) {
					var value = $('input[action="' + dependency + '');
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue) {
							self.fundatie_linkerzijkantHoogte = parseFloat($(value));
							self.fundatie_linkerzijkantBreedte = parseFloat($(value).attr('min'));
							
						
						}
					});
				}
				self.calculation();
				
			};
			$('.fundatie_rechterzijkant').on('change', fundatieRechterzijkantChange);
			fundatieRechterzijkantChange($('.fundatie_rechterzijkant'));
			
			var zijwandLinksChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$("#type_zijwand_links").hide();
				} else {
					$("#type_zijwand_links").show();
				}
				
				var dependency = elem.attr('action').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();
				
				var linkerwandName = elem.find('option:selected').text();
				$('.type_linkerwand_naam').html(linkerwandName);
				this.zijwand_links_selected = dependencyvalue;

				if (dependencyvalue) {
					var value = $('input[action="' + dependency + '');
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue) {
							self.zijwand_linksTot = parseFloat($(value).attr('max'));
							self.zijwand_linksVan = parseFloat($(value).attr('min'));
							self.zijwand_linksCorrlengtemt = parseFloat($(value).attr('corrlengtemt'));
						}
					});
				}
				
				self.warnings('type_linkerwand_maat');
				self.warnings('spie_opmerking_links')
				self.calculation();
				
			};
			$('.zijwand_links').on('change', zijwandLinksChange);
			zijwandLinksChange($('.zijwand_links'));
			
			var zijwandRechtsChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$("#type_zijwand_rechts").hide();
				} else {
					$("#type_zijwand_rechts").show();
				}
				
				var dependency = elem.attr('action').replace(/[\[\]']/g, '');
				var dependencyvalue = elem.val();
				
				var rechterwandName = elem.find('option:selected').text();
				$('.type_rechterwand_naam').html(rechterwandName);
				this.zijwand_links_selected = dependencyvalue;

				if (dependencyvalue) {
					var value = $('input[action="' + dependency + '');
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyvalue) {
							self.zijwand_rechtsTot = parseFloat($(value).attr('max'));
							self.zijwand_rechtsVan = parseFloat($(value).attr('min'));
							self.zijwand_rechtsCorrlengtemt = parseFloat($(value).attr('corrlengtemt'));
						}
					});
				}
				self.warnings('type_rechterwand_maat');
				self.warnings('spie_opmerking_rechts')
				self.calculation();
				
			};
			$('.zijwand_rechts').on('change', zijwandRechtsChange);
			zijwandRechtsChange($('.zijwand_rechts'));

			// option zonnewering
			var zonneweringChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if (elem.val() == 'option-1') {
					$("#zonnewering").hide();
				} else {
					$("#zonnewering").show();
				}
				self.warnings('zonnewering_licht', elem.val());
			};
			$('.zonnewering').on('change', zonneweringChange);
			zonneweringChange($('.zonnewering'));

			// option VVK fundatie Linkerzijkant en Rechterzijkant
			// var zonnedoekenChange = function(elem) {
				
			// 	if(typeof(elem.currentTarget) != 'undefined') {
			// 		elem = $(this);
			// 	}
				
			// 	if (elem.val() == 'option-1') {
			// 		$("#zonnedoeken").hide();
			// 	} else {
			// 		$("#zonnedoeken").show();
			// 	}
			// };
			// $('.zonnedoeken').on('change', zonnedoekenChange);
			// zonnedoekenChange($('.zonnedoeken'));

			var uitvuldiepteZijwandenChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				self.uitvuldiepte_zijwanden_links = parseInt(elem.val());
				self.calculationOptions();
			};
			$('.uitvuldiepte_zijwanden').on('change', uitvuldiepteZijwandenChange);
			uitvuldiepteZijwandenChange($('.uitvuldiepte_zijwanden'));

			var uitvuldiepteZijwandenRechtsChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				self.uitvuldiepte_zijwanden_rechts = parseInt(elem.val());
				self.calculationOptions();
			};
			$('.uitvuldiepte_zijwanden_rechts').on('change', uitvuldiepteZijwandenRechtsChange);
			uitvuldiepteZijwandenRechtsChange($('.uitvuldiepte_zijwanden_rechts'));

			// // event voor zijdlingse_montage
			var zijdlingseMontageChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}

				self.zijdlingse_montage = parseInt(elem.val());
				self.calculation();

			};
			$('.zijdlingse_montage').on('change', zijdlingseMontageChange);
			zijdlingseMontageChange($('.zijdlingse_montage'));
			
			var tussenAanbouwChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
			
				self.tussenAanbouw = elem.prop('checked');
				self.calculation();
				if(self.tussenAanbouw) {
					$('.correctie-toggle').show();
				} else {
					$('.correctie-toggle').hide();
				}
				
			};
			$('.tussen_aanbouw').on('change', tussenAanbouwChange);
			tussenAanbouwChange($('.tussen_aanbouw'));
			
			var maatvoeringTussenAanbouwKeyup = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
			
				if(elem.val() > self.breedte) {
					elem.val(self.breedte);
				}
			
				self.maatvoering_tussen_aanbouw = parseInt(elem.val());
				self.calculation();
				
				
			};
			$('.maatvoering_tussen_aanbouw').on('keyup', maatvoeringTussenAanbouwKeyup);
			maatvoeringTussenAanbouwKeyup($('.maatvoering_tussen_aanbouw'));
			
			var lightsValueChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
			
				if(elem.hasClass('aantal_spots')){
					self.aantal_spots = parseInt(elem.val()); 
				} else{
					self.led_verlichting =  parseInt(elem.val());
				}
				self.lights_value = self.led_verlichting + self.aantal_spots;
				
			};
			$('.lights-value').on('change', lightsValueChange);
			lightsValueChange($('.lights-value'));

			// event voor input diepte
			// var xsGootChange = function(elem) {
				
			// 	if(typeof(elem.currentTarget) != 'undefined') {
			// 		elem = $(this);
			// 	}
				
			// 	console.log(elem);
				
			// 	var dependency = elem.attr('name').replace(/[\[\]']/g, '');
			// 	var dependencyvalue = elem.val();

			// 	if (elem.attr('checked')) {
			// 		var value = $('input[dependency-field="' + dependency + '');
			// 		value.each(function () {
			// 			if (elem.attr('dependency-value') === dependencyvalue) {
			// 				self.xs_gootHoogte = parseFloat(elem.attr('max'));
			// 				self.xs_gootBreedte = parseFloat(elem.attr('min'));
			// 			}
			// 		});
			// 	}
			// 	self.calculation();
			// };
			// $('.xs-goot').on('change', xsGootChange);
			// xsGootChange($('.xs-goot'));


			// // // event voor dropdown type glas
			// var typeGlasChange = function(elem) {
				
			// 	if(typeof(elem.currentTarget) != 'undefined') {
			// 		elem = $(this);
			// 	}
				
			// 	var dependency = elem.attr('name').replace(/[\[\]']/g, '');
			// 	var dependencyvalue = elem.val();

			// 	if (dependencyvalue) {
			// 		var value = $('input[dependency-field="' + dependency + '');
			// 		value.each(function () {
			// 			if (elem.attr('dependency-value') === dependencyvalue) {
			// 				// self.type_glasAantal = parseFloat($(this).attr('min'));
			// 				// self.type_glasCorrlengtemt = parseFloat($(this).attr('corrlengtemt'));
						
			// 			}
			// 		});
			// 	}
			// 	// self.calculation();
			// };
			// $('.type_glas').on('change', typeGlasChange);

			// // event voor input zonnewering
			var zonneweringChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				var dependency = elem.attr('action').replace(/[\[\]']/g, '');
				var dependencyValue = elem.val();

				var zonneweringSelected = false;

				if (dependencyValue) {
					var value = $('input[action="' + dependency + '');
					
					value.each(function(index, value) {
						if ($(value).attr('dependency-value') === dependencyValue) {
							
							zonneweringSelected = true;
							
							// Corr Breedtemaat
							self.corr_breedtemaat = parseFloat($(value).attr('min'));
							// corr_dieptemaat
							self.corr_dieptemaat = parseFloat($(value).attr('max'));
							// corr_vrijehoogte corrlengtemt
							self.corr_vrijehoogte = parseFloat($(value).attr('corrlengtemt'));
							
							self.dakvlakkenEven = ($(value).attr('action_or') == 'dakvlakken_even') ? 1 : 0;
						}
					});
				}
				
				if(!zonneweringSelected) {
					self.corr_breedtemaat = 0;
					self.corr_dieptemaat = 0;
					self.corr_vrijehoogte = 0;
					self.dakvlakkenEven = 0;
				}
				
				console.log(self.dakvlakkenEven);
				
				if (self.corrLengtemt) {
					self.warnings('zonnewering');
				}
				self.calculation();
			};
			$('.zonnewering').on('change', zonneweringChange);
			zonneweringChange($('.zonnewering'));

			// // event voor input zonnedoeken_breedte
			// var zonnedoekenChange = function(elem) {
				
			// 	if(typeof(elem.currentTarget) != 'undefined') {
			// 		elem = $(this);
			// 	}
				
			// 	console.log(elem);
				
			// 	var dependency = elem.attr('action').replace(/[\[\]']/g, '');
			// 	var dependencyvalue = elem.val();

			// 	if (dependencyvalue) {
			// 		var value = $('input[action="' + dependency + '');
			// 		value.each(function () {
			// 			if (elem.attr('dependency-value') === dependencyvalue) {
			// 				// zonnedoeken_breedte
			// 				self.zonnedoeken_breedte = parseFloat(elem.attr('min'));
			// 			}
			// 		});
			// 	}
			// 	self.calculation();
			// };
			// $('.zonnedoeken').on('change', zonnedoekenChange);
			// zonnedoekenChange($('.zonnedoeken'));

			// // event voor input zonnedoeken_breedte
			var typeProductChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if(elem.prop('checked') != true) return;

				var dependency = elem.attr('dependency-value').replace(/[\[\]']/g, '');
				self.typeProductDependencyValue = elem.val();
				
				if(self.typeProductDependencyValue == 'greenline') {
					$('.color_greenline').change();
				} else if(self.typeProductDependencyValue == 'profiline') {
					$('.color_profiline').change();
				}
				
				if(!elem.prop('checked')) return;

				if (self.typeProductDependencyValue) {
					var value = $('input[action="' + self.typeProductDependencyValue + '');
					if (value.length > 0) {
						value.each(function(index, value) {
							if ($(value).attr('dependency-value') === self.typeProductDependencyValue) {
								self.hoofproduct_hoogte_muurprofiel = parseFloat($(value).val());
								self.staander_breedte = parseFloat($(value).attr('min'));
								self.staander_diepte = parseFloat($(value).attr('max'));
								if(typeof(self.hoofproduct_hoogte_muurprofiel) == 'undefined') {
									self.hoofproduct_hoogte_muurprofiel = 0;
								}
							}
						});
					} else {
						let val = $('.highline_gsw').val();
						if(typeof(val) != 'undefined') {
							self.hoofproduct_hoogte_muurprofiel = val;
						}
					}

				}

				self.calculation();
				self.warnings('extra_hwa_toevoegen');
				
			};
			$('.type_product').on('change', typeProductChange);
			typeProductChange($('.type_product'));

			// // dakvlak_type-dakbedekking
			var dakvlakTypeDakbedekkingClick = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}

				var dependency = elem.attr('action').replace(/[\[\]']/g, '');
				let radioOption = parseInt(elem.val().replace(/[^0-9]/g, ''));
				let dependencyRadioOption = $('.' + dependency);

				if (radioOption === 1 || radioOption === 4) {
					$(dependencyRadioOption).val('option-3');
				} else if (radioOption === 2 || radioOption === 3) {


					$(dependencyRadioOption).val('option-2');
				} else {
					$(dependencyRadioOption).val('option-1');
				}

				let dependencyName = elem.attr('name');
				let dependencyValue = elem.val();

				let dakVlakTypeMinValue = $('input[dependency-field="' + dependencyName + '"][dependency-value="' + dependencyValue + '"]');
			
				if (dakVlakTypeMinValue) {
					self.type_glasCorrlengtemt = parseFloat(dakVlakTypeMinValue.attr('corrlengtemt'));
					self.type_glasAantal = parseFloat(dakVlakTypeMinValue.attr('min'));
				}
				$(dependencyRadioOption).change();
			};
			$('.dakvlak_type-Dakbedekking').on('click', dakvlakTypeDakbedekkingClick);

			var dakvlakTypeChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				// dakvlak type
				if(elem.prop('checked')) {
					
					self.dakvlak_type = parseInt(elem.val().replace(/[^0-9]/g, ''));
					var label = elem.next().html();
					
					if(label.indexOf('44.2') != -1) {
						self.type_glasAantal = '44.2';
					} else if(label.indexOf('55.2') != -1) {
						self.type_glasAantal = '55.2';
					} else {
						self.type_glasAantal = '';
					}
					
					self.calculation();
					self.warnings('profiline');
					
				}
				
			};
			$('.dakvlak_type').on('change', dakvlakTypeChange);
			dakvlakTypeChange($('.dakvlak_type:checked'));
			
			var colorProfilineGreenlineChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
			
				var colorProfiline = elem.val();
				
				if(colorProfiline == 'option-7' || colorProfiline == 'option-8') {
					
					$('.type_gootsierlijst').each(function(index, value) {
					
						if($(value).val() == 'option-3') {
							$(value).prop('checked', 'true');
						} else {
							$(value).parent().hide();
						}
						
					});
					
				} else {
					
					$('.type_gootsierlijst').each(function(index, value) {
					
						$(value).parent().show();
						
					});
					
				}
				
			};
			$('.color_profiline, .color_greenline').on('change', colorProfilineGreenlineChange);
			
			var obstakelsChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if(elem.val() == 'option-8') {
					if(elem.prop('checked')) {
						$('#hoogte_hwa_uitloop_display').show();
					} else {
						$('#hoogte_hwa_uitloop_display').hide();
					}
				}
				
			};
			$('.obstakels').on('change', obstakelsChange);
			obstakelsChange($('.obstakels'));
			
			var zonneDoekenChange = function(elem) {
				
				if(typeof(elem.currentTarget) != 'undefined') {
					elem = $(this);
				}
				
				if(elem.val() == 'option-2') {
					$('#zonnedoeken').show();
				} else {
					$('#zonnedoeken').hide();
				}
				
			};
			$('.zonne_doeken').on('change', zonneDoekenChange);
			zonneDoekenChange($('.zonne_doeken'));
			
			
			$('.keuze_poeren').on('change', function() {
				self.warnings('extra_hwa_toevoegen');	
			});
			
		},

		setEventsInput: function () {

			var self = this;

			// event voor input breedte
			// Hoogte onderkant muurprofiel class h_onderkant_muur
			// @var this.HoogteMuur
			
			$('.h_onderkant_muur').on('change', function() {
				self.HoogteMuur = parseFloat($(this).val());
				
				if(!$('.hoogte_onderkant_goot').val() || confirm("Onderkant goot maat herberekenen?")) {
					self.hoogte_onderkant_goot = Math.round((self.HoogteMuur - ((self.diepte / 100) * 13.5) + self.terrasVerval) * 100) / 100;
					$('.hoogte_onderkant_goot').val(self.hoogte_onderkant_goot);
					$('#aanzicht_hoogte_links').html('OG: ' + self.hoogte_onderkant_goot);
					$('#aanzicht_hoogte_rechts').html('OG: ' + self.hoogte_onderkant_goot);
					
				}
				
				$('#hoogte_onderkant_muur_links').html('OM: ' + self.HoogteMuur);
				$('#hoogte_onderkant_muur_rechts').html('OM: ' + self.HoogteMuur);
				
				if(self.glasspie && self.afschot_veranda) {
					self.hoogteVoorzijdeSpieLinks = self.glasspie - self.afschot_veranda;
					$('.hoogte_voorzijde_spie_links').val(self.hoogteVoorzijdeSpieLinks);
					$('#hoogte_voorzijde_links').val(self.hoogteVoorzijdeSpieLinks);
					if(!isNaN(self.hoogteVoorzijdeSpieLinks)) {
						$('#hoogte_spie_voorzijde_links').html('HSV: ' + self.hoogteVoorzijdeSpieLinks);
					} else {
						$('#hoogte_spie_voorzijde_links').html('');
					}
				}
			
				if(self.glasspie_recht && self.afschot_veranda) {
					self.hoogteVoorzijdeSpieRechts = self.glasspie_recht - self.afschot_veranda;
					$('.hoogte_voorzijde_spie_rechts').val(self.hoogteVoorzijdeSpieRechts);
					$('#hoogte_voorzijde_rechts').val(self.hoogteVoorzijdeSpieRechts);
					if(!isNaN(self.hoogteVoorzijdeSpieRechts)) {
						$('#hoogte_spie_voorzijde_rechts').html('HSV: ' + self.hoogteVoorzijdeSpieRechts);
					} else {
						$('#hoogte_spie_voorzijde_rechts').html('');
					}
				}
				
				self.calculation();
			});
			
			$('.hoogte_onderkant_goot').on('change', function() {
				self.hoogte_onderkant_goot = parseFloat($(this).val());
				
				if(!$('.h_onderkant_muur').val() || confirm("Onderkant muur maat herberekenen?")) {
					self.HoogteMuur = Math.round((self.hoogte_onderkant_goot + ((self.diepte / 100) * 13.5) - self.terrasVerval) * 100) / 100;
					$('.h_onderkant_muur').val(self.HoogteMuur);
					$('#hoogte_onderkant_muur_links').html('OM: ' + self.HoogteMuur);
					$('#hoogte_onderkant_muur_rechts').html('OM: ' + self.HoogteMuur);
				}
				
				$('#aanzicht_hoogte_links').html('OG: ' + $(this).val());
				$('#aanzicht_hoogte_rechts').html('OG: ' + $(this).val());
				
				if(self.glasspie && self.afschot_veranda) {
					self.hoogteVoorzijdeSpieLinks = self.glasspie - self.afschot_veranda;
					$('.hoogte_voorzijde_spie_links').val(self.hoogteVoorzijdeSpieLinks);
					$('#hoogte_voorzijde_links').val(self.hoogteVoorzijdeSpieLinks);
					if(!isNaN(self.hoogteVoorzijdeSpieLinks)) {
						$('#hoogte_spie_voorzijde_links').html('HSV: ' + self.hoogteVoorzijdeSpieLinks);
					} else {
						$('#hoogte_spie_voorzijde_links').html('');
					}
				}
			
				if(self.glasspie_recht && self.afschot_veranda) {
					self.hoogteVoorzijdeSpieRechts = self.glasspie_recht - self.afschot_veranda;
					$('.hoogte_voorzijde_spie_rechts').val(self.hoogteVoorzijdeSpieRechts);
					$('#hoogte_voorzijde_rechts').val(self.hoogteVoorzijdeSpieRechts);
					if(!isNaN(self.hoogteVoorzijdeSpieRechts)) {
						$('#hoogte_spie_voorzijde_rechts').html('HSV: ' + self.hoogteVoorzijdeSpieRechts);
					} else {
						$('#hoogte_spie_voorzijde_rechts').html('');
					}
				}
				
				
				self.calculation();
			});
			
			$('#aanzicht_hoogte_links').html('OG: ' + $('.hoogte_onderkant_goot').val());
			$('#aanzicht_hoogte_rechts').html('OG: ' + $('.hoogte_onderkant_goot').val());
			$('#hoogte_onderkant_muur_links').html('OM: ' + $('.h_onderkant_muur').val());
			$('#hoogte_onderkant_muur_rechts').html('OM: ' + $('.h_onderkant_muur').val());
			
			// event voor input breedte
			document.getElementsByClassName('afmeeting-breedte')[0].addEventListener('keyup', function () {

				self.breedte = parseFloat($(this).val());
				$('#aanzicht_breedte_voor').html('Br.:' + self.breedte);
				self.calculation();
				self.warnings('extra_hwa_toevoegen');
				self.warnings('type_voorwand_maat');
			});
			// event voor input diepte
			document.getElementsByClassName('afmeeting-diepte')[0].addEventListener('keyup', function () {
				self.diepte = parseFloat($(this).val());
				$('#aanzicht_diepte_links').html(self.diepte + ' t/m hart staander');
				$('#aanzicht_diepte_rechts').html(self.diepte + ' t/m hart staander');
				
				self.calculation();
				
				self.warnings('extra_hwa_toevoegen');
				self.warnings('type_linkerwand_maat');
				self.warnings('type_rechterwand_maat');
			});

			// event voor Diepte uitvulling muurprofiel
			document.getElementsByClassName('uitvulling_muurprofiel')[0].addEventListener('keyup', function () {
				self.uitvulling_muurprofiel = parseFloat($(this).val());
				self.calculation();
			});
			
			// event voor Diepte uitvulling muurprofiel
			document.getElementsByClassName('uitvulling_muurprofiel_rechts')[0].addEventListener('keyup', function () {
				self.uitvulling_muurprofiel_rechts = parseFloat($(this).val());
				self.calculation();
			});

			// event voor input spie linkerzijdkand glasspie
			// TODO: !--- change self.glasspie variable---!
			document.getElementsByClassName('hoogte_spie_links')[0].addEventListener('keyup', function (e) {

				if(e.keyCode == 9) return;
				
				self.glasspie = parseInt($(this).val());
				self.warnings('spie_links_goot');
				if(isNaN(self.glasspie)) {
					self.glasspie = 0;
				}
				
				self.hoogteVoorzijdeSpieLinks = self.glasspie - self.afschot_veranda;
				$('.hoogte_voorzijde_spie_links').val(self.hoogteVoorzijdeSpieLinks);
				$('#hoogte_voorzijde_links').val(self.hoogteVoorzijdeSpieLinks);
				$('#hoogte_spie_voorzijde_links').html('HSV: ' + self.hoogteVoorzijdeSpieLinks);
				
				if(!isNaN(self.glasspie)) {
					$('#hoogte_spie_achterzijde_links').html('HSA: ' + self.glasspie);
				} else {
					$('#hoogte_spie_achterzijde_links').html('');
				}
				
				self.calculation();
				self.warnings('spie_links_null');
			});

			document.getElementsByClassName('hoogte_spie_rechts')[0].addEventListener('keyup', function (e) {

				if(e.keyCode == 9) return;

				self.glasspie_recht = parseFloat($(this).val());
				self.warnings('spie_rechts_goot');
				
				if(isNaN(self.glasspie_recht)) {
					self.glasspie_recht = 0;
				}
				
				self.hoogteVoorzijdeSpieRechts = self.glasspie_recht - self.afschot_veranda;
				$('.hoogte_voorzijde_spie_rechts').val(self.hoogteVoorzijdeSpieRechts);
				$('#hoogte_voorzijde_rechts').val(self.hoogteVoorzijdeSpieRechts);
				$('#hoogte_spie_voorzijde_rechts').html('HSV: ' + self.hoogteVoorzijdeSpieRechts);
				$('#hoogte_spie_achterzijde_rechts').html('HSA: ' + self.glasspie_recht);
				
				if(!isNaN(self.glasspie_recht)) {
					$('#hoogte_spie_achterzijde_rechts').html('HSA: ' + self.glasspie_recht);
				} else {
					$('#hoogte_spie_achterzijde_rechts').html('');
				}

				self.calculation();
				self.warnings('spie_rechts_null');
			});

			document.getElementsByClassName('hoogte_voorzijde_spie_links')[0].addEventListener('keyup', function (e) {
				
				if(e.keyCode == 9) return;
				
				self.hoogteVoorzijdeSpieLinks = parseInt($(this).val());
				if(isNaN(self.hoogteVoorzijdeSpieLinks)) {
					self.hoogteVoorzijdeSpieLinks = 0;
				}
				
				$('.hoogte_voorzijde_spie_links').val(self.hoogteVoorzijdeSpieLinks);
				self.glasspie = self.hoogteVoorzijdeSpieLinks + self.afschot_veranda;
				$('.hoogte_spie_links').val(self.glasspie);
				$('#hoogte_spie_voorzijde_links').html('HSV: ' + self.hoogteVoorzijdeSpieLinks);
				if(!isNaN(self.glasspie)) {
					$('#hoogte_spie_achterzijde_links').html('HSA: ' + self.glasspie);
				} else {
					$('#hoogte_spie_achterzijde_links').html('');
				}
				
				self.calculation();
				self.warnings('spie_links_null');
			});
			
			document.getElementsByClassName('hoogte_voorzijde_spie_rechts')[0].addEventListener('keyup', function (e) {
				
				if(e.keyCode == 9) return;
				
				self.hoogteVoorzijdeSpieRechts = parseInt($(this).val());
				if(isNaN(self.hoogteVoorzijdeSpieRechts)) {
					self.hoogteVoorzijdeSpieRechts = 0;
				}
				$('#hoogte_voorzijde_rechts').val(self.hoogteVoorzijdeSpieRechts);
				self.glasspie_rechts = self.hoogteVoorzijdeSpieRechts + self.afschot_veranda;
				$('.hoogte_spie_rechts').val(self.glasspie_rechts);
				$('#hoogte_spie_voorzijde_rechts').html('HSV: ' + self.hoogteVoorzijdeSpieRechts);
				if(!isNaN(self.glasspie_recht)) {
					$('#hoogte_spie_achterzijde_rechts').html('HSA: ' + self.glasspie_recht);
				} else {
					$('#hoogte_spie_achterzijde_rechts').html('');
				}
				
				self.calculation();
				self.warnings('spie_rechts_null');
			});
			
			if(!isNaN(self.hoogteVoorzijdeSpieLinks)) {
				$('#hoogte_spie_voorzijde_links').html('HSV: ' + self.hoogteVoorzijdeSpieLinks);
			}
			if(!isNaN(self.glasspie)) {
				$('#hoogte_spie_achterzijde_links').html('HSA: ' + self.glasspie);
			}
			if(!isNaN(self.hoogteVoorzijdeSpieRechts)) {
				$('#hoogte_spie_voorzijde_rechts').html('HSV: ' + self.hoogteVoorzijdeSpieRechts);
			}
			if(!isNaN(self.glasspie_recht)) {
				$('#hoogte_spie_achterzijde_rechts').html('HSA: ' + self.glasspie_recht);
			}

			document.getElementsByClassName('extra_diepte_overstek_zijwanden')[0].addEventListener('keyup', function () {
				self.extra_diepte_overstek_zijwanden = parseFloat($(this).val());
				self.calculation();
			});
			
			
			document.getElementsByClassName('extra_diepte_overstek_zijwanden_rechts')[0].addEventListener('keyup', function () {
				self.extra_diepte_overstek_zijwanden_rechtspie = parseFloat($(this).val());
				self.calculation();
			});
			
			document.getElementsByClassName('terrasverval')[0].addEventListener('keyup', function () {
				self.terrasVerval = parseFloat($(this).val());
				self.calculation();
			});

			document.getElementsByClassName('hoogte_hwa_uitloop')[0].addEventListener('keyup', function () {
				self.hoogte_hwa_uitloop = parseFloat($(this).val());
				self.calculation();
			});

			document.getElementsByClassName('afstand_hwa_uitloop')[0].addEventListener('keyup', function () {
				self.afstand_hwa_uitloop = parseFloat($(this).val());
				self.calculation();
			});

			document.getElementsByClassName('lengte_staanders')[0].addEventListener('keyup', function () {
				
				if(self.lengte_staanders > parseFloat($(this).val())){
					document.getElementsByClassName('lengte_staanders')[0] = self.lengte_staanders;
					self.calculation();
				}
			});

			// document.getElementById('aantal_staanders').addEventListener('keyup', function () {
			// 	// Aantal staanders
			// 	self.aantal_staanders = parseInt(document.getElementById('aantal_staanders').value);
			// 	self.warnings('aantal_staanders');
			// 	self.calculation();
			// });

			// document.getElementsByClassName('led_verlichting')[0].addEventListener('keyup',function (){
			//     self.led_verlichting = parseInt($(this).val());
			//     self.calculation();
			// });


			// document.getElementsByClassName('aantal_spots')[0].addEventListener('keyup',function (){
			//     this.aantal_spots = parseInt($(this).val());
			//     self.calculation();
			// });

			$('body').on('keyup', '.diepte_deur_raam', function () {

				self.doorloophoogteAction = $(this).attr('action');
				self.calculation();
			});

			$('body').on('keyup', '.hoogte_deur_raam', function () {

				self.hoogte_deur_raam = parseFloat($(this).val());
				self.doorloophoogteAction = $(this).attr('action');
				self.calculation();
			});
			
			$('body').on('keyup', '.afstand_hoogte_deur_raam', function () {

				self.diepte_deur_raam = parseFloat($(this).val());
				self.doorloophoogteAction = $(this).attr('action');
				self.calculation();
			});
			
			$('.afmeeting-breedte').on('change', function() {
				$('#aanzicht_breedte_voor').html('Br.:' +  $(this).val());
			}).change();
			
			$('.afmeeting-diepte').on('change', function() {
				$('#aanzicht_diepte_links').html($(this).val() + ' t/m hart staander');
				$('#aanzicht_diepte_rechts').html($(this).val() + ' t/m hart staander');
			}).change();
			
		 
		},

		setStatement: function () {

			var self = this;
			// Lengte staanders statementshoogte_onderkant_goot
			if (self.hoogte_onderkant_goot > 235) {
				self.lengte_staanders = self.hoogte_onderkant_goot + 15;
			} else {
				self.lengte_staanders = 250;
			}

			$('.spie-l').on('change', function () {

				if ($(this).val() == 'option-2') {
					self.corrLengtemtLinks = self.corr_lengtemt_glasspie;
					self.spie_links = 1;
					self.minHoogteVoorzijdeSpieLinks = 0;
				} else if ($(this).val() == 'option-3') {
					self.corrLengtemtLinks = self.corr_lengtemt_pc_spie;
					self.spie_links = 1;
					self.minHoogteVoorzijdeSpieLinks = 5;
				} else {
					self.spie_links = 0;
				}
				
				self.warnings('spie_opmerking_links');
				self.warnings('spie_links_null');
				self.warnings('spie_links_goot');
				
			});


			$('.spie-r').on('change', function () {

				if ($(this).val() == 'option-2') {
					self.corrLengtemtRechts = self.corr_lengtemt_glasspie_recht;
					self.spie_rechts = 1;
					self.minHoogteVoorzijdeSpieRechts = 0;
				} else if ($(this).val() == 'option-3') {
					self.corrLengtemtRechts = self.corr_lengtemt_pc_spie_recht;
					self.spie_rechts = 1;
					self.minHoogteVoorzijdeSpieRechts = 5;
				} else {
					self.spie_rechts = 0;
				}
				
				self.warnings('spie_opmerking_rechts');
				self.warnings('spie_rechts_null');
				self.warnings('spie_rechts_goot');

			});
			
			
			$('.spie_uitvullen_onder_zijwand').change(function() {
			 
				if($(this).val() === "option-2"){
					self.spie_uitvullen_onder_zijwand = parseInt($('#terras_verval').val());
				}else{
					self.spie_uitvullen_onder_zijwand = 0;
				}
			
					
			});
			
			$('.spie_uitvullen_onder_zijwand_rechts').change(function() {
			 
				if($(this).val() === "option-2"){
					self.spie_uitvullen_onder_zijwand_recht = parseInt($('#terras_verval').val());
				}else{
					self.spie_uitvullen_onder_zijwand_recht = 0;
				}
			
					
			});
			var koppelstukSelect = $('.koppelstuk_benodigd');
			if (this.bestelmaat_breedte_veranda > 700) {
				
				koppelstukSelect.find('option').eq(0).attr('disabled', true);
				koppelstukSelect.find('option').eq(1).attr('disabled', false);
				koppelstukSelect.val('option-2').change();
				 
			}else{
			 
				koppelstukSelect.find('option').eq(0).attr('disabled', false);
				koppelstukSelect.find('option').eq(1).attr('disabled', true);
				koppelstukSelect.val('option-1').change();
				
			}
			
			if (this.dakvlak_type === 2 || this.dakvlak_type === 3) {
				this.aantal_dakvlakken = Math.ceil(this.bestelmaat_breedte_veranda / 75);
				if(this.dakvlakkenEven == 1 && this.aantal_dakvlakken % 2 == 1) {
					this.aantal_dakvlakken++;
				}
				$('.dakbeplating').show();
				$('.dakbeplating-pc').hide();
				this.dakvlakken_correction = 2.1;
			} else if (this.dakvlak_type === 1 || this.dakvlak_type === 4) {
				$('.dakbeplating').hide();
				$('.dakbeplating-pc').show();
				this.aantal_dakvlakken = Math.ceil(this.bestelmaat_breedte_veranda / 100);
				if(this.dakvlakkenEven == 1 && this.aantal_dakvlakken % 2 == 1) {
					this.aantal_dakvlakken++;
				}
				this.dakvlakken_correction = 2;
			} else {
				$('.dakbeplating').hide();
				$('.dakbeplating-pc').hide();
				this.aantal_dakvlakken = 0;
			}


			if (self.typeProductDependencyValue && self.diepte) {
				const type_glas = $('.type_glas');
				let profiline_diepte = parseFloat($('.profiline_diepte').val());


				if (self.typeProductDependencyValue === 'greenline') {
					$(type_glas).val('option-2');
				} else if (self.typeProductDependencyValue === 'profiline' && self.diepte < profiline_diepte) {
					$(type_glas).val('option-3');
				} else if (self.typeProductDependencyValue === 'profiline' && self.diepte >= profiline_diepte) {

					$(type_glas).val('option-4');
				} else {
					$(type_glas).val('option-1');
				}
				self.warnings('profiline');
				$(type_glas).change();
			}
			 

		},

		calculation: function () {
			
			if(!this.allowedCalculation) return;
			
			//get new values
			this.initChangeFields();
			
			var self = this;

			// set de Bestelmaat breedte veranda
			if(this.tussenAanbouw) {
				if(this.maatvoering_tussen_aanbouw > 0) {
					this.bestelmaat_breedte_veranda = this.maatvoering_tussen_aanbouw + this.correctie;
				} else {
					this.bestelmaat_breedte_veranda = this.breedte + this.correctie;					
				}
			} else {
				this.bestelmaat_breedte_veranda = this.breedte;
			}
			if (this.bestelmaat_breedte_veranda) {
				document.getElementById('bestelmaat_breedte_veranda').value = parseFloat(this.bestelmaat_breedte_veranda);
			}
			// set afschot veranda value
			this.afschot_veranda = Math.ceil((this.HoogteMuur - this.hoogte_onderkant_goot + (this.terrasVerval * 100)));
			this.warnings('spie_rechts_goot');
			this.warnings('spie_links_goot');

			// afschot_veranda
			if (this.afschot_veranda) {
				$('.afschot_veranda').val(parseFloat(this.afschot_veranda));
			}
			
			//  Standaard afschot
			this.standaard_afschot = Math.round((this.diepte * 0.135) * 100) / 100;
			if (this.standaard_afschot) {
				document.getElementById('standaard_afschot').value = parseFloat(this.standaard_afschot);
			}

			// set Lengte staanders
			if (this.lengte_staanders) {
				document.getElementsByClassName('lengte_staanders')[0].value = parseFloat(this.lengte_staanders);
			}

			// Bestelmaat diepte veranda (hart staander)
			this.bestelmaat_diepte_diepte_veranda = Math.round((Math.sqrt((Math.sqrt((this.diepte * this.diepte) + (this.afschot_veranda * this.afschot_veranda)) * Math.sqrt((this.diepte * this.diepte) + (this.afschot_veranda * this.afschot_veranda))) - (this.afschot_veranda * this.afschot_veranda))) * 100) / 100;

			if (this.bestelmaat_diepte_diepte_veranda) {
				document.getElementById('bestelmaat_diepte_diepte_veranda').value = parseFloat(this.bestelmaat_diepte_diepte_veranda);
			}

			//  Doorloophoogte voorzijde
		
			this.doorloophoogte_voorzijde = this.hoogte_onderkant_goot - this.onderzijde_gootHoogte;
			if (this.doorloophoogte_voorzijde) {
				document.getElementById('doorloophoogte_voorzijde').value = parseFloat(this.doorloophoogte_voorzijde);
			}
			
			
			// =+G22-D84-D93+ALS(G28="Nee";G27;0)
			this.doorloophoogte_rechterzijkant = this.HoogteMuur - this.glasspie_recht - this.vvk_rechterzijkantHoogte + this.spie_uitvullen_onder_zijwand_recht;
			
			if(this.doorloophoogte_rechterzijkant){
				document.getElementById('doorloophoogte_rechterzijkant').value = parseFloat(this.doorloophoogte_rechterzijkant);
			}



			// =+G22-D84-D93+ALS(G28="Nee";G27;0)
			this.doorloophoogte_linkerzijkant = this.HoogteMuur - this.glasspie - this.vvk_linkerzijkantHoogte + this.spie_uitvullen_onder_zijwand;
			if(this.doorloophoogte_linkerzijkant){
				document.getElementById('doorloophoogte_linkerzijkant').value = parseFloat(this.doorloophoogte_linkerzijkant);
			}



			// Berekende doorloophoogte bij openstaande deur / raam
			//G22-(G41/100*(G24/(G7/100)))-G39


			var doorloophoogte_deur_raam = parseFloat(this.HoogteMuur - (this.diepte_deur_raam / 100 * (this.afschot_veranda / (this.diepte / 100))) - this.corr_vrijehoogte).toFixed(2);

			$('.doorloophoogte_deur_raam_' + this.doorloophoogteAction).val(doorloophoogte_deur_raam);

			// Berekende hoogte op positie HWA uitloop
			// hoogte_hwa_uitloop
			this.hoogtehwauitloop = Math.round(parseFloat(this.HoogteMuur - (this.afstand_hwa_uitloop / 100 * (this.afschot_veranda / (this.diepte / 100))) + this.hoofproduct_hoogte_muurprofiel));
			document.getElementById('berekende_hoogte_hwa_uitloop').value = this.hoogtehwauitloop;
			this.warnings('hwa_warning');

			// Hoofdproduct hoogte muurprofiel
			if (this.hoofproduct_hoogte_muurprofiel) {
				document.getElementById('hoofproduct_hoogte_muurprofiel').value = parseFloat(this.hoofproduct_hoogte_muurprofiel);
			}

			// check statements
			this.setStatement();
			if (this.aantal_dakvlakken) {
				document.getElementById('aantal_dakvlakken').value = Math.ceil(this.aantal_dakvlakken);
			}

			// klopt
			this.dakvlakken_bruto_breedte = Math.round((this.bestelmaat_breedte_veranda / this.aantal_dakvlakken) * 100) / 100;
			if (!isNaN(this.dakvlakken_bruto_breedte)) {
				document.getElementById('dakvlakken_bruto_breedte').value = parseFloat(this.dakvlakken_bruto_breedte);
			}

			// klopt
			this.dakvlakken_netto_breedte = this.dakvlakken_bruto_breedte - this.dakvlakken_correction;
			if (parseFloat(this.dakvlakken_netto_breedte) > 0) {
				document.getElementById('dakvlakken_netto_breedte').value = Math.round(parseFloat(this.dakvlakken_netto_breedte), 1);
				document.getElementById('dakbeplating_breedte').value = Math.round(parseFloat(this.dakvlakken_netto_breedte), 1);
			}

			// klopt
			var dakbeplating_diepte = parseFloat(this.bestelmaat_diepte_diepte_veranda + this.type_glasCorrlengtemt);
			if (dakbeplating_diepte) {
				document.getElementById('dakbeplating_diepte').value = dakbeplating_diepte.toFixed(2);
			}
			// calculation product options
			this.calculationOptions();
		},

		calculationOptions: function () {

			if(this.uitvuldiepte_zijwanden_links === 2) {
				this.spie_berekende_lengte = this.diepte - this.staander_diepte + this.corrLengtemtLinks + this.extra_diepte_overstek_zijwanden;
			} else {
				this.spie_berekende_lengte = this.diepte - this.staander_diepte + this.corrLengtemtLinks + this.uitvulling_muurprofiel + this.extra_diepte_overstek_zijwanden;
			}
			if (this.spie_berekende_lengte) {
				document.getElementById('spie_berekende_lengte').value = parseFloat(this.spie_berekende_lengte);
			}
			
			if(this.uitvuldiepte_zijwanden_rechts === 2) {
				this.spie_berekende_lengte_rechts = this.diepte - this.staander_diepte + this.corrLengtemtRechts + this.extra_diepte_overstek_zijwanden_rechtspie;
			} else {
				this.spie_berekende_lengte_rechts = this.diepte - this.staander_diepte + this.corrLengtemtRechts + this.uitvulling_muurprofiel_rechts + this.extra_diepte_overstek_zijwanden_rechtspie;
			}
			if (this.spie_berekende_lengte_rechts) {
				document.getElementById('spie_berekende_lengte_rechts').value = parseFloat(this.spie_berekende_lengte_rechts);
			}

			// Berekende doorloophogte spie
			// @var spie_berekende_doorloophogte

			var spie_berekende_doorloophogte = this.HoogteMuur - this.glasspie - this.vvk_linkerzijkantHoogte;
			if (spie_berekende_doorloophogte) {
				document.getElementById('spie_berekende_doorloophogte').value = parseFloat(spie_berekende_doorloophogte);
			}
			//  eind calculationOptions spie linkerzijkant //


			//*  begin calculationOptions spie rechterzijkant //*
			// glasspie 
			var hoogte_voorzijde_rechts = this.glasspie_recht - this.afschot_veranda;
			if (hoogte_voorzijde_rechts) {
				document.getElementById('hoogte_voorzijde_rechts').value = parseFloat(hoogte_voorzijde_rechts);
			}
			
			// this.spie_berekende_lengte = this.diepte - 5.5 + this.corrLengtemt + this.uitvulling_muurprofiel + this.extra_diepte_overstek_zijwanden;

			// if (this.spie_berekende_lengte) {
			// 	document.getElementById('spie_berekende_lengte').value = parseFloat(this.spie_berekende_lengte);
			// }

			// Berekende doorloophogte spie
			// @var spie_berekende_doorloophogte_recht

			var spie_berekende_doorloophogte_rechts = this.HoogteMuur - this.glasspie - this.vvk_rechterzijkantHoogte;
			if (spie_berekende_doorloophogte_rechts) {
				document.getElementById('spie_berekende_doorloophogte_rechts').value = parseFloat(spie_berekende_doorloophogte_rechts);
			}
			//  eind calculationOptions spie rechterzijkant //

			//*  begin calculationOptions onderzijde goot //*

			// onderzijde goot berekende lengte
			// TODO bestelmaat_breedte_veranda - 2x min maat staander
			var onderzijde_goot_berekende_lengte = this.bestelmaat_breedte_veranda - (2 * this.staander_breedte);
			if (onderzijde_goot_berekende_lengte) {
				document.getElementById('onderzijde_goot_berekende_lengte').value = parseFloat(onderzijde_goot_berekende_lengte);
				document.getElementById('vvk_fundatie_voorzijde_lengte').value = parseFloat(onderzijde_goot_berekende_lengte);
			}

			//  eind calculationOptions onderzijde goot //


			//*  begin calculationOptions VVK linkerzijkant //*
			// VVK linkerzijkant berekende lengte
			if(this.uitvuldiepte_zijwanden_links === 2) {
				this.vvk_linkerzijkant_berekende_lengte = this.diepte - this.staander_diepte + this.extra_diepte_overstek_zijwanden;
			} else {
				this.vvk_linkerzijkant_berekende_lengte = this.diepte - this.staander_diepte + this.extra_diepte_overstek_zijwanden + this.uitvulling_muurprofiel;
			}
			if (this.vvk_linkerzijkant_berekende_lengte) {
				document.getElementById('vvk_linkerzijkant_berekende_lengte').value = parseFloat(this.vvk_linkerzijkant_berekende_lengte);
				document.getElementById('vvk_fundatie_links_lengte').value = parseFloat(this.vvk_linkerzijkant_berekende_lengte);
			}
			// eind calculationOptions VVK linkerzijkant //


			//*  begin calculationOptions VVK rechterzijkant //*
			// VVK rechterzijkant berekende lengte
			if(this.uitvuldiepte_zijwanden_rechts === 2) {
				this.vvk_rechterzijkant_berekende_lengte = this.diepte - this.staander_diepte + this.extra_diepte_overstek_zijwanden_rechtspie;
			} else {
				this.vvk_rechterzijkant_berekende_lengte = this.diepte - this.staander_diepte + this.extra_diepte_overstek_zijwanden_rechtspie + this.uitvulling_muurprofiel_rechts;
			}
			if (this.vvk_rechterzijkant_berekende_lengte) {
				document.getElementById('vvk_rechterzijkant_berekende_lengte').value = parseFloat(this.vvk_rechterzijkant_berekende_lengte);
				document.getElementById('vvk_fundatie_rechts_lengte').value = parseFloat(this.vvk_rechterzijkant_berekende_lengte);
			}
			// eind calculationOptions VVK rechterzijkant //


			//*  begin calculationOptions Type voorwand //*
			// Type voorwand berekende lengte
			var type_voorwand_berekende_lengte = this.bestelmaat_breedte_veranda - (2 * this.staander_breedte) + this.voorwandCorrlengtemt;
			if (type_voorwand_berekende_lengte) {
				document.getElementById('type_voorwand_berekende_lengte').value = parseFloat(type_voorwand_berekende_lengte);
			}

			// Type voorwand berekende hoogte
			var type_voorwand_berekende_hoogte = this.doorloophoogte_voorzijde;
			if (type_voorwand_berekende_hoogte) {
				document.getElementById('type_voorwand_berekende_hoogte').value = parseFloat(type_voorwand_berekende_hoogte);
			}

			//  eind calculationOptions VVK linkerzijkant //


			//*  begin calculationOptions Type voorwand //*
			// Type voorwand berekende lengte
			// var vvk_fundatie_voorzijde_lengte = this.breedte;
			// if (vvk_fundatie_voorzijde_lengte) {
			// 	document.getElementById('vvk_fundatie_voorzijde_lengte').value = parseFloat(vvk_fundatie_voorzijde_lengte);
			// }

			//  eind calculationOptions VVK linkerzijkant //


			//*  begin calculationOptions Type zijwand //*
			// Type zijwand links en rechts
			if(this.uitvuldiepte_zijwanden_links === 2) {
				var type_zijwand_linkslengte = this.bestelmaat_diepte_diepte_veranda - this.staander_diepte + this.zijwand_linksCorrlengtemt + this.extra_diepte_overstek_zijwanden;
			} else {
				var type_zijwand_linkslengte = this.bestelmaat_diepte_diepte_veranda - this.staander_diepte + this.zijwand_linksCorrlengtemt + this.uitvulling_muurprofiel + this.extra_diepte_overstek_zijwanden;
			}
			if (type_zijwand_linkslengte) {
				document.getElementById('type_zijwand_linkslengte').value = parseFloat(type_zijwand_linkslengte);
			}
			
			this.type_zijwand_berekende_hoogte = this.HoogteMuur - this.glasspie - this.vvk_linkerzijkantHoogte;
			if (this.type_zijwand_berekende_hoogte) {
				document.getElementById('type_zijwand_berekende_hoogte').value = parseFloat(this.type_zijwand_berekende_hoogte);
			} else {
				document.getElementById('type_zijwand_berekende_hoogte').value = this.hoogte_onderkant_goot;
			}
			document.getElementById('type_zijwand_berekende_hoogte_achterzijde').value = this.HoogteMuur;
			
			if(this.uitvuldiepte_zijwanden_rechts === 2) {
				var type_zijwand_rechts_lengte = this.bestelmaat_diepte_diepte_veranda - this.staander_diepte + this.zijwand_rechtsCorrlengtemt + this.extra_diepte_overstek_zijwanden_rechtspie;
			} else {
				var type_zijwand_rechts_lengte = this.bestelmaat_diepte_diepte_veranda - this.staander_diepte + this.zijwand_rechtsCorrlengtemt + this.uitvulling_muurprofiel_rechts + this.extra_diepte_overstek_zijwanden_rechtspie;
			}
			if (type_zijwand_rechts_lengte) {
				document.getElementById('type_zijwand_rechts_lengte').value = parseFloat(type_zijwand_rechts_lengte);
			}
			
			this.type_zijwand_berekende_hoogte_rechts = this.HoogteMuur - this.glasspie_recht - this.vvk_rechterzijkantHoogte
			if (this.type_zijwand_berekende_hoogte_rechts) {
				document.getElementById('type_zijwand_berekende_hoogte_rechts').value = parseFloat(this.type_zijwand_berekende_hoogte_rechts);
			} else {
				document.getElementById('type_zijwand_berekende_hoogte_rechts').value = this.hoogte_onderkant_goot;
			}
			document.getElementById('type_zijwand_berekende_hoogte_rechts_achterzijde').value = this.HoogteMuur;
			
			
			
			// if (Number.isInteger(this.doorloophoogte_voorzijde)) {
			// 	document.getElementById('zijwand_recht_correctie_tussen').value = parseFloat(this.doorloophoogte_voorzijde);
			// }
			//  eind calculationOptions  Type zijwand  //

			//*  begin calculationOptions  VVK fundatie //*
			// VVK fundatie Linkerzijkant

			// var vvk_fundatie_rechts_lengte = this.diepte;
			// if (Number.isInteger(vvk_fundatie_rechts_lengte)) {
			// 	document.getElementById('vvk_fundatie_rechts_lengte').value = parseFloat(vvk_fundatie_rechts_lengte);
			// }
			// VVK fundatie Rechterzijkant
			// var vvk_fundatie_links_lengte = this.diepte;
			// if (Number.isInteger(vvk_fundatie_links_lengte)) {
			// 	document.getElementById('vvk_fundatie_links_lengte').value = parseFloat(vvk_fundatie_links_lengte);
			// }
			//  eind calculationOptions   VVK fundatie   //

			// * begin calculationOption Zonnewering //*
			//  zonnewering_breedte
			var zonnewering_breedte = this.bestelmaat_breedte_veranda;
			if (this.zijdlingse_montage === 1) {
				zonnewering_breedte = this.bestelmaat_breedte_veranda + this.corr_breedtemaat;
			}

			if (parseFloat(zonnewering_breedte) > 0) {
				document.getElementById('zonnewering_breedte').value = parseFloat(zonnewering_breedte);
			}

			// zonnewering_diepte
			var zonnewering_diepte = this.diepte + this.corr_dieptemaat;
			if (parseFloat(zonnewering_diepte) > 0) {
				document.getElementById('zonnewering_diepte').value = parseFloat(zonnewering_diepte);
			}

			//* eind calculationOption Zonnewering //*


			// * begin calculationOption Zonnedoeken //*

			// zonnedoeken_breedte

			let zonnedoekMidden = document.getElementById('zonnedoek_midden');
			let zonnedoekBuiten = document.getElementById('zonnedoek_buiten');

			if (zonnedoekMidden) {
				let zonnedoeken_middenplaten = this.dakvlakken_netto_breedte - $(zonnedoekMidden).attr('min');
				zonnedoekMidden.value = Math.round(zonnedoeken_middenplaten * 10) / 10;
			}

			if(zonnedoekBuiten){
				let zonnedoeken_breedte = this.dakvlakken_netto_breedte - $(zonnedoekBuiten).attr('min');
				zonnedoekBuiten.value = Math.round(zonnedoeken_breedte * 10) / 10;
			}

			//* eind calculationOption Zonnewering //*

		},
		setClickEvent: function () {
			
			var self = this;

			// next button and previous and show all button
			$('body').on('click', '#button-formfield-next', function() {
				var element = $('span.active.form-field')[0];
				$(element).toggleClass('active');
				$(element).nextAll('span.form-field:first').toggleClass('active');
			});

			$('body').on('click', '#button-formfield-prev', function() {
				var element = $('span.active.form-field')[0];
				$(element).toggleClass('active');
				$(element).prevAll('span.form-field:first').toggleClass('active');
			});
			
			$('body').on('click', '#deff-save-button', function() {
				
				var result = self.validateDeff()
				
				if(result) {
					
					document.getElementById('save_deff').value = 'true';
					
					$('#save-button').click();
				}
				
			});
			
		},
		
		format: function(format) {
		    var args = Array.prototype.slice.call(arguments, 1);
		    return format.replace(/{(\d+)}/g, function(match, number) { 
		      return typeof args[number] != 'undefined'
		        ? args[number] 
		        : match
		      ;
		    });
		  },

		warnings: function (value, param = false) {

			switch (value) {
				case 'profiline':
					if (this.type_glasAantal === '44.2' && this.diepte > 350) {
						$('.warning-keuze-glas').show();
					} else if (this.type_glasAantal === '55.2' && this.diepte < 350) {
						$('.warning-keuze-glas').show();
					} else {
						$('.warning-keuze-glas').hide();
					}
					break;
					
				case 'extra_hwa_toevoegen':
					var poer = false;
					$('.keuze_poeren:checked').each(function(index, value) {
						if($(value).val() == 'option-1' || $(value).val() == 'option-2') {
							poer = true;
						}
					});
					if((this.breedte * this.diepte) >= 120000 && this.typeProductDependencyValue == 'profiline' && poer) {
						$('.extra_hwa_toevoegen').show();
					} else {
						$('.extra_hwa_toevoegen').hide();
					}
					break;
				
				case 'hwa_warning':
					if (this.hoogtehwauitloop > this.hoogte_hwa_uitloop) {
						$('.hwa_warning').show();
					} else {
						$('.hwa_warning').hide();
					}
					break;
				case 'type_wand':

					if (!this.typevoorwandwarning) {
						alert("Max breedte is kleiner dan totale breedte");
						this.typevoorwandwarning = true;
					}
					break;
				case 'type_voorwand_maat':
					if(this.breedte > this.voorwandTot && parseInt(this.voorwandTot) > 0 && parseInt(this.breedte) > 0) {
						$('.type_voorwand_maat').show();
					} else {
						$('.type_voorwand_maat').hide();
					}
					break;
					
				case 'type_linkerwand_maat':
					if(this.diepte > this.zijwand_linksTot && parseInt(this.zijwand_linksTot) > 0 && parseInt(this.diepte) > 0) {
						$('.type_linkerwand_maat').show();
					} else {
						$('.type_linkerwand_maat').hide();
					}
					break;
					
				case 'type_rechterwand_maat':
					if(this.diepte > this.zijwand_rechtsTot && parseInt(this.zijwand_rechtsTot) > 0 && parseInt(this.diepte) > 0) {
						$('.type_rechterwand_maat').show();
					} else {
						$('.type_rechterwand_maat').hide();
					}
					break;
					
				case 'spie_opmerking_links':
					if(this.spie_links == 1 && this.zijwand_links_selected == 'option-1') {
						$('.spie_opmerking_links').show();
					} else {
						$('.spie_opmerking_links').hide();
					}
					break;
				
				case 'spie_opmerking_rechts':
					if(this.spie_rechts == 1 && this.zijwand_rechts_selected == 'option-1') {
						$('.spie_opmerking_rechts').show();
					} else {
						$('.spie_opmerking_rechts').hide();
					}
					break;
					
				case 'spie_links_null':
					if(!this.spie_links_null) {
						this.spie_links_null = $('.spie_links_null').html();
					}
					if(this.spie_links == 1 && this.hoogteVoorzijdeSpieLinks < this.minHoogteVoorzijdeSpieLinks) {
						$('.spie_links_null').html(this.format(this.spie_links_null, this.minHoogteVoorzijdeSpieLinks))
						$('.spie_links_null').show();
					} else {
						$('.spie_links_null').hide();
					}
					break;
					
				case 'spie_rechts_null':
					if(!this.spie_rechts_null) {
						this.spie_rechts_null = $('.spie_rechts_null').html();
					}
					if(this.spie_rechts == 1 && this.hoogteVoorzijdeSpieRechts < this.minHoogteVoorzijdeSpieRechts) {
						$('.spie_rechts_null').html(this.format(this.spie_rechts_null, this.minHoogteVoorzijdeSpieRechts))
						$('.spie_rechts_null').show();
					} else {
						$('.spie_rechts_null').hide();
					}
					break;
					
				case 'spie_links_goot':
					if(!this.spie_links_goot_warning) {
						this.spie_links_goot_warning = $('.spie_links_goot_warning').html();
					}
					if(this.onderzijde_gootHoogte > 0) {
						var vvkGoot = ' de VVK onder de '
					} else {
						var vvkGoot = ' '
					}
					var limit = this.afschot_veranda + this.onderzijde_gootHoogte;
					if(this.spie_links == 1 && this.glasspie > limit) {
						$('.spie_links_goot_warning').html(this.format(this.spie_links_goot_warning, Math.round(this.glasspie - limit, vvkGoot)));
						$('.spie_links_goot_warning').show();
					} else {
						$('.spie_links_goot_warning').hide();
					}
					break;
					
				case 'spie_rechts_goot':
					if(!this.spie_rechts_goot_warning) {
						this.spie_rechts_goot_warning = $('.spie_rechts_goot_warning').html();
					}
					var limit = this.afschot_veranda + this.onderzijde_gootHoogte;
					if(this.spie_rechts == 1 && this.glasspie_recht > limit) {
						$('.spie_rechts_goot_warning').html(this.format(this.spie_rechts_goot_warning, Math.round(this.glasspie_recht - limit)));
						$('.spie_rechts_goot_warning').show();
					} else {
						$('.spie_rechts_goot_warning').hide();
					}
					break;
				case 'zonnewering':
					if (!this.zonneweringwarning) {
						alert('Indien in één zijwand Spie aanwezig, dan controleren of Hoogte spie voorzijde minimaal 10cm is.');
						this.zonneweringwarning = true;
					}
					break;
				case 'zonnewering_licht':
					if(param == 'option-3') {
						$('.zonnewering_licht').show();
					} else {
						$('.zonnewering_licht').hide();
					}
					break;
				case 'aantal_staanders':
					if (this.aantal_staanders <= 2 && !this.aantal_staanderswarning) {
						alert('Controleer of er een VVK noodzakelijk is!');
						this.aantal_staanderswarning = true;
					}
					break;
			}
		},

	};
});