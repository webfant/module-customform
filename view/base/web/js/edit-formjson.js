define([
    "jquery",
    'jquery/ui',
    'mage/url',
    'formCalculation',
], function($, ui,url,formCalculation) {
    'use strict';



    $.widget('mage.editFormJson', {
        options: function (data, element){
            return data;
        },

        _create: function () {
            
            this.setDataFieldValue();

        },

        setDataFieldValue:function (){
            let self = this;

	        if(!typeof this.options.data){
	        	return;
	        }
	        
            let data = this.options.data;
            this._mediaUrl = this.options.dataUrl;

            $.each( data, function( key, value ) {
                $.each( value, function( k, val ) {
                    self.setCheckbox(val, k);
                });
            });
	        formCalculation._create();
	        formCalculation.calculation();
	        
	        $('.delete-file-button').on('click', function() {
	            
	            var rel = $(this).attr('ref');
	            $('#img-' + rel).remove();
	            $('#input-' + rel).remove();
	            $(this).remove();
	            
	        });

        },

        setCheckbox:function (val, checkElement) {
            let el = null;
            let element;

            switch (val.type) {
                case 'text':
                case 'textinput':
                case 'textarea':
                    el = document.getElementById(checkElement);
                    $(el).val(val.inputvalue).trigger("keyup");

                    break;
                case 'canvas':

                    break;
                case 'hone':

                    break;
                case 'number':
                    el = document.getElementById(checkElement);
	                $(el).val(val.inputvalue).trigger('keyup');

                    break;
                case 'dropdown':
                    el = document.getElementById(checkElement);
                    if(!el){
                        break;
                    }
                    // $(el).val(val.inputvalue);
	                let opts = el.options;

	                for (let j = 0; j <  opts.length; j++) {
		                if (opts[j].value === val.inputvalue) {
			                el.selectedIndex = j;
			                break;
		                }
	                }
                    $(el).change();
                    break;
                case 'checkbox':
                case 'checkboxtwo':
                case 'checkboxthree':

                    element = $('[name="'+checkElement+'[]"]');

                    if($.type(val.inputvalue) !== "array") {
                        let value = val.inputvalue;
                        if ($(checkElement).val() === value) {
                            $(checkElement).prop("checked", true);
                            $(checkElement).trigger("change");
                        }
                    }

                    $(element).each(function (k , d) {

                        let object = val.inputvalue;
                        $.each(object, function (k,v) {

                            if ($(d).val() === v) {
                                $(d).prop("checked", true);
                                $(d).trigger("change");
                            }
                        });
                    });
                    break;
                case 'radio':
                case 'radiotwo':

                    element = $('[name="'+checkElement+'"]');
                    $(element).each(function (k , d) {
                    let value = val.inputvalue;
                        if ($(d).val() === value) {
                            $(d).prop("checked", true);
                            // $(d).trigger("click");
                            $(d).trigger('change');
                        }
                    });
                    break;
                case 'file':
                    let file = document.getElementById(checkElement);
                    let imageUrl = this._mediaUrl+val.inputvalue;
                    if(file){
                        file.removeAttribute('required');
                        file.removeAttribute('validation');
                        
                        let e = document.createElement("input");
                            e.id = 'input-'+checkElement;
                            e.setAttribute('name',checkElement);
                            e.type = 'hidden';
                            e.value = val.inputvalue;
                        
                        let img = document.createElement("img");
                            img.setAttribute('src',imageUrl);
                            img.id = 'img-'+checkElement;
                            img.setAttribute('width', 500);
                        
                        let brEl = document.createElement('br');
                        
                        let delFile = document.createElement('button');
                            delFile.setAttribute('id','delete-' + checkElement);
                            delFile.setAttribute('ref',checkElement);
                            delFile.setAttribute('class', 'delete-file-button');
                            delFile.innerHTML = 'Verwijder foto';
                            delFile.type = 'button';
                            $('.field-'+checkElement).append(e,img,brEl,delFile);
                    }
                    break;
                case 'listbox':

                    break;
            }
            $('body').on('change','.container-left-static input',function(){

                $(this).css('border','blue');
            });

        }
    });
    return $.mage.editFormJson;
});