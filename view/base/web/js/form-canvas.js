define([
	"jquery",
	'jquery/ui',
	'Webfant_Customform/js/jquery.canvasjs.min',
	'domReady!'

], function($, ui, fabric) {
	'use strict';

	return {

		created: false,
		krinnerClick: false,
		lightplanClick: false,
		freedrawingClick: false,

		_create: function(e) {

			if (!this.created) {
				this.canvas = false;
				this.freecanvas = false;
				this.krinnerCanvas = false;
				this.aantal_spots = e.aantal_spots;
				this.diepte = (e.diepte) ? e.diepte : 0;
				this.breedte = (e.breedte) ? e.breedte : 0;
				this.bestelmaat_breedte_veranda = (e.bestelmaat_breedte_veranda) ? e.bestelmaat_breedte_veranda : 0;
				this.led_verlichting = (e.led_verlichting) ? e.led_verlichting : 0;
				this.hoogte_onderkant_goot = (e.hoogte_onderkant_goot) ? e.hoogte_onderkant_goot : 0;
				this.HoogteMuur = (e.HoogteMuur) ? e.HoogteMuur : 0;
				this.aantal_staanders = (e.aantal_staanders) ? e.aantal_staanders : 0;
				this.lights_value = e.lights_value;
				this.afmeeting_breedte = 0;
				this.canvasDrawLights();
				this.created = true;
			}

		},

		_lightPlan: function(e) {
			this.aantal_dakvlakken = Math.ceil((e.aantal_dakvlakken) ? e.aantal_dakvlakken : 0);
			this.canvasDrawLights();
		},

		_freeDrawing: function(e) {
			// body...
			this.setFreeDrawing();
			this.dataURItoBlob();
		},

		_krinners: function() {

			this.drawKrinnerCanvas();

		},
		
		_signature: function() {
		
			this.drawSignatureCanvas();
			
		},
		
		drawSignatureCanvas: function() {
		
			self = this;
			
			$('#signature-wrapper').appendTo('#amform-form-8 .insert-container');

			let el = $('.canvas_signature')[0];
			if (!this.signature) {
				this.signature = new fabric.Canvas(el, {
					width: 500,
					height: 200,
					selection: false,
					isDrawingMode: true,
				});
			}

			this.signature.setHeight(200);
			this.signature.setWidth(500);
			this.signature.renderAll();
			this.signature.clear();
			
			// var gridLen = options.width / options.distance;

			// for (var i = 0; i < gridLen; i++) {

			// 	var distance = i * options.distance,
			// 		horizontal = new fabric.Line([distance, 0, distance, options.width], options.param),
			// 		vertical = new fabric.Line([0, distance, options.width, distance], options.param);
			// 	this.freecanvas.add(horizontal);
			// 	this.freecanvas.add(vertical);
			// 	if (i % 5 === 0) {
			// 		horizontal.set({ stroke: '#cccccc' });
			// 		vertical.set({ stroke: '#cccccc' });
			// 	};
			// };


			if(!self.signatureClick) {
				$('body').on('click', '#build-save-signature', function() {
					let free_drawing = $('.canvas_signature')[0];
					let canvasURL = free_drawing.toDataURL("image/png", '0.5');
					canvasURL = self.dataURItoBlob(canvasURL, 'signature');
					$('#build-signature').css('display','none');
		    		$('#build-edit-signature').css('display','block');
				});
				self.signatureClick = true;
			}
			
		},

		drawKrinnerCanvas: function() {

			var target = $('.canvas-krinners');
			var self = this;
			if (!this.krinnerCanvas) {
				this.krinnerCanvas = new fabric.Canvas(target[0]);
			}

			this.krinnerCanvas.clear();
			var options = {
					distance: 10,
					width: this.krinnerCanvas.width,
					height: this.krinnerCanvas.height,
					param: {
						stroke: '#ebebeb',
						strokeWidth: 1,
						selectable: false,
						isDrawingMode: true,
					}
				},
				gridLen = options.width / options.distance;

			// for (var i = 0; i < gridLen; i++) {

			// 	var distance = i * options.distance,
			// 		horizontal = new fabric.Line([distance, 0, distance, options.width], options.param),
			// 		vertical = new fabric.Line([0, distance, options.width, distance], options.param);
			// 	this.krinnerCanvas.add(horizontal);
			// 	this.krinnerCanvas.add(vertical);
			// 	if (i % 5 === 0) {
			// 		horizontal.set({ stroke: '#cccccc' });
			// 		vertical.set({ stroke: '#cccccc' });
			// 	};
			// };

			if(!self.krinnerClick) {
				$('body').on('click', '#add-krinner', function() {
	
					var pos1 = { x: 30, y: 0 },
						pos2 = { x: 60, y: 0 },
						pos3 = { x: 45, y: 50 };
	
					self.krinnerCanvas.add(
						new fabric.Polygon([pos1, pos2, pos3], {
							stroke: '#aaf',
							strokeWidth: 1,
							fill: '#E7F609',
							selectable: true,
							lockScalingX: true,
							lockScalingY: true,
							lockRotation: true,
							lockMovementY: true,
							fill: 'black'
						})
					);
	
				});
	
				
				$('body').on('click', '#build-save-krinners', function() {
					let light_plan = $('.canvas-krinners')[0];
					self.krinnerCanvas.discardActiveObject().renderAll();
					let canvasURL = light_plan.toDataURL("image/png", '0.5');
					canvasURL = self.dataURItoBlob(canvasURL, 'krinner_map');
					$('#add-krinner').css('display','none');
					$('#add-krinner-edit').css('display','block');
				});
				self.krinnerClick = true;
			}
			

		},

		canvasDrawLights: function() {

			// boven aanzicht verlichtings plan
			// @var aanzicht_verlichtings
			var bovenaanzicht_canvas = $('.bovenaanzicht');
			if (!this.canvas) {
				this.canvas = new fabric.Canvas(bovenaanzicht_canvas[0]);
			}

			// empty canvas bij nieuw input// dit is om de canvas geupdate te houden
			this.canvas.clear();

			// aantal_dakvlakken totale dakplaten geplaates gaat worden.
			var count = parseInt(this.aantal_dakvlakken);
			var colm = this.canvas.width / this.aantal_dakvlakken;
			var height = this.canvas.height;

			for (let i = 0; i < count; i++) {

				this.canvas.add(new fabric.Line([colm, 5, colm, height], {
					left: colm,
					top: 0,
					stroke: 'red',
					lockMovementX: true,
					lockMovementY: true,
					selectable: false
				}));

				colm += this.canvas.width / this.aantal_dakvlakken;
			}

			this.setsLights();
			var brush = new fabric.PencilBrush(this.canvas);
			var points = [
				[0, 0]
			];
			$('#button-formfield-canvas').show();



		},
		setsLights: function() {

			if (!parseInt(this.aantal_dakvlakken)) {
				return;
			}
			var self = this;



			let colm = this.canvas.width / this.aantal_dakvlakken;

			let lights = $('.lights-value').val();
			if (lights == 'option-1') {
				lights = $('.nr_spots').val();
			}
			if (!lights) {
				lights = parseInt(this.aantal_dakvlakken - 1) * 2;
			}

			let rect;
			var lightsPerCol = lights / (this.aantal_dakvlakken - 1);
			var curLightsPerCol = lightsPerCol;
			var rows = this.aantal_dakvlakken - 1;
			var grid = this.canvas.width / this.aantal_dakvlakken;
			var totalLights = 0;

			for (let r = 1; r <= rows; r++) {

				if (r == rows && (lights - totalLights) > Math.floor(curLightsPerCol)) {
					curLightsPerCol++;
				}
				let height = (this.canvas.height / (Math.floor(curLightsPerCol) + 1));

				for (let i = 1; i <= curLightsPerCol; i++) {

					this.canvas.add(
						rect = new fabric.Rect({
							width: 20,
							height: 20,
							left: colm - 10,
							top: height * i,
							stroke: '#aaf',
							strokeWidth: 1,
							fill: '#E7F609',
							selectable: true,
							lockScalingX: true,
							lockScalingY: true,
							lockRotation: true
						})
					);

					totalLights++;

				}

				colm += this.canvas.width / this.aantal_dakvlakken;
				
				var curLightsPerColFloor = Math.floor(curLightsPerCol);
				curLightsPerCol = Math.round((curLightsPerCol + (lightsPerCol - curLightsPerColFloor)) * 10) / 10;

				if(!self.lightplanClick) {
					$('body').on('click', '#build-save-light', function() {
						let light_plan = $('.bovenaanzicht')[0];
						self.canvas.discardActiveObject().renderAll();
						let canvasURL = light_plan.toDataURL("image/png", '0.5');
						canvasURL = self.dataURItoBlob(canvasURL, 'light_plan');
						$('#build-light-plan').css('display','none');
			    		$('#build-edit-light').css('display','block');
					});
					self.lightplanClick = true;
				}

			};

			this.canvas.on('object:moving', function(options) {
				options.target.set({
					left: (Math.round(options.target.left / grid) * grid) - 10,
					top: options.target.top
				});
			});

		},

		setFreeDrawing() {

			self = this;

			let el = $('.canvas-5')[0];

			if (!this.freecanvas) {
				this.freecanvas = new fabric.Canvas(el, {
					selection: false,
					isDrawingMode: true,
				});
			}

			this.freecanvas.clear();

			var options = {
				distance: 10,
				width: this.freecanvas.width,
				height: this.freecanvas.height,
				param: {
					stroke: '#ebebeb',
					strokeWidth: 1,
					selectable: false,
					isDrawingMode: true,
				}
			};
			
			// var gridLen = options.width / options.distance;

			// for (var i = 0; i < gridLen; i++) {

			// 	var distance = i * options.distance,
			// 		horizontal = new fabric.Line([distance, 0, distance, options.width], options.param),
			// 		vertical = new fabric.Line([0, distance, options.width, distance], options.param);
			// 	this.freecanvas.add(horizontal);
			// 	this.freecanvas.add(vertical);
			// 	if (i % 5 === 0) {
			// 		horizontal.set({ stroke: '#cccccc' });
			// 		vertical.set({ stroke: '#cccccc' });
			// 	};
			// };


			if(!self.freedrawingClick) {
				$('body').on('click', '#build-save-drawing', function() {
					let free_drawing = $('.free_drawing')[0];
					let canvasURL = free_drawing.toDataURL("image/png", '0.5');
					canvasURL = self.dataURItoBlob(canvasURL, 'freedrawing_image');
					$('#build-free-drawing').css('display','none');
		    		$('#build-edit-drawing').css('display','block');
				});
				self.freedrawingClick = true;
			}
			
			 
		},

		dataURItoBlob(dataURI = false, textClass = false) {

			// convert base64/URLEncoded data component to raw binary data held in a string
			if (!dataURI) return;
			var byteString;
			//return  dataURI.replace(/^data:image\/(png|jpg);base64,/, "")

			if (dataURI.split(',')[0].indexOf('base64') >= 0)
				byteString = atob(dataURI.split(',')[1]);
			else
				byteString = unescape(dataURI.split(',')[1]);

			// separate out the mime component
			var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

			// write the bytes of the string to a typed array
			var ia = new Uint8Array(byteString.length);

			for (var i = 0; i < byteString.length; i++) {
				ia[i] = byteString.charCodeAt(i);
			}

			var blob = new Blob([ia], {
				type: mimeString
			});

			var today = new Date();
			var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			var dateTime = date + ' ' + time;

			var time = Date.parse(dateTime);

			var formData = new FormData();
			let image = document.getElementsByClassName(textClass)[0];
			image.value = time + '.png';
			formData.append('imagedata', blob, time + '.png');

			$.ajax({
				url: BASE_URL + "inmeet/form/uploadFile", // Url to which the request is send
				type: "POST", // Type of request to be send, called as method

				data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false, // The content type used when sending data to the server.
				cache: false, // To unable request pages to be cached
				processData: false, // To send DOMDocument or non processed data file it is set to false
				success: function(data) // A function to be called if request succeeds
				{
					let image = document.getElementsByClassName(textClass)[0];
					$('#' + textClass).css('display', 'flex');
					image.value = time + '.png';
					
					$('#'+textClass+'_image').attr('src',BASE_URL +"media/webfant/amcustomform/inmeet_images/"+image.value);
		    		$('#'+textClass+'_opslaan').css('display', 'flex');
		    		$('.'+textClass+'_canvas').closest('div.canvas-field').css('display','none');
		    		$('#'+textClass+'_image').css('border',"2px black solid");
		    					
				}
			});

		}
	}

});
