var config = {
    map: {
        '*': {
            'editFormJson': 'Webfant_Customform/js/edit-formjson',
            'formCalculation':  'Webfant_Customform/js/form-calculation',
        },
    shim:{
        'editFormJson':{
            'deps':['formCalculation']
        }
    }
    }
};

