define(
    	[
        	'jquery',
        	'ko',
        	'uiComponent'
    	],
    	function ($, ko, component, dataTables) {
        	"use strict";
                
        	return component.extend({
        	   
            	items: ko.observableArray([]),
            	columns: ko.observableArray([]),
            	query: ko.observable(''),
            	status: ko.observable(''),
            	seller: ko.observable(''),
            	
            	defaults: {
                	template: 'Webfant_Customform/grid',
            	},
                // options: function (data, element){
                //     console.log(data);
                //     return data;
                // },
            	initialize: function (data) {
            	    
                	var self = this;
            	    this.data = data.data;
            	    this.userData = data.users;
            	    this.currentUser = data.current_user;
            	    
                	this._super();
                	this._render();
                	
                	console.log(this.items);
                	
        	    	this.searchResults = ko.computed(function() {
                	    
                	    var q = self.query();
                	    var status = self.status();
                	    var selectedUser = self.selectedUser();
                	    return self.items.filter(function(i) {
                	        if(selectedUser) {
                	            return (i.name.toLowerCase().indexOf(q) >= 0 || i.customer_name.toLowerCase().indexOf(q) >= 0) && i.status == status && i.customer_id == selectedUser;
                	        } else {
                	            return (i.name.toLowerCase().indexOf(q) >= 0 || i.customer_name.toLowerCase().indexOf(q) >= 0) && i.status == status;
                	        }
                	        
                	    });
                	    
                	});
            	},
            	_render: function() {
                	this._prepareColumns();
                	this._prepareItems();     
                	this._prepareUsers();
            	},
            	_prepareItems: function () {
            	    
            	    var items = $.parseJSON(this.data);
                    this.addItems(items);
                    
            	},
            	_prepareColumns: function () {
                	this.addColumn({headerText: "ID", rowText: "id", renderer: ''});
                	this.addColumn({headerText: "Naam", rowText: "name", renderer: ''});
                	this.addColumn({headerText: "Ingevoerd door", rowText: "customer_name", renderer: ''});
                	this.addColumn({headerText: "Status", rowText: "status", renderer: ''}); 
                	this.addColumn({headerText: "Edit", rowText: "edit", renderer: ''});
                	this.addColumn({headerText: "PDF", rowText: "pdf", renderer: ''});
                	this.addColumn({headerText: "E-Mail", rowText: "sendMail", renderer: ''});
                	this.addColumn({headerText: "Delete", rowText: "delete", renderer: ''});
                },
                _prepareUsers: function() {
                    
                    this.users = ko.observableArray([]);
                    this.selectedUser = ko.observable(this.currentUser);
                    var self = this;
                    
                    var User = function(id, name) {
                        this.id = id;
                        this.name = name;
                    }
                    
                    $.each(this.userData, function(index, value) {
                        self.users.push(new User(value.id, value.name));
                    });
                    
                },
            	addItem: function (item) {
                	item.columns = this.columns;
                	this.items.push(item);
            	},
            	addItems: function (items) {
                	for (var i in items) {
                    	this.addItem(items[i]);
                	}
            	},
            	addColumn: function (column) {
                	this.columns.push(column);
            	}
        	});
    	}
);