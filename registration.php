<?php
/**
 * @author Webfant Team
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Webfant_Customform',
    __DIR__
);