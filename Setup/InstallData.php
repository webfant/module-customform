<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var \Webfant\Customform\Model\Template\ExampleForm
     */
    private $exampleForm;

    public function __construct(\Webfant\Customform\Model\Template\ExampleForm $exampleForm)
    {
        $this->exampleForm = $exampleForm;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // $this->exampleForm->createExampleForms();
    }
}
