<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var Operation\AddEmailResponse
     */
    private $addEmailResponse;

    /**
     * @var Operation\AddResponseStatus
     */
    private $addResponseStatus;

    public function __construct(
        \Webfant\Customform\Setup\Operation\AddEmailResponse $addEmailResponse,
        Operation\AddResponseStatus $addResponseStatus,
        Operation\AddCustomerSubmitData $addCustomerSubmitData
    ) {
        $this->addEmailResponse = $addEmailResponse;
        $this->addResponseStatus = $addResponseStatus;
        $this->addCustomerSubmitData = $addCustomerSubmitData;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.5.0', '<')) {
            $this->addEmailResponse->execute($setup);
        }

        if (version_compare($context->getVersion(), '1.7.0', '<')) {
            $this->addResponseStatus->execute($setup);
        }
        
        if (version_compare($context->getVersion(), '1.9.1', '<')) {
            $this->addCustomerSubmitData->execute($setup);
        }

        $setup->endSetup();
    }
}
