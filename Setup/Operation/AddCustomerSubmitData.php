<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Setup\Operation;

use Webfant\Customform\Api\Data\AnswerInterface;

class AddCustomerSubmitData
{
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function execute(\Magento\Framework\Setup\SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable('wf_customform_submit');
        
        $setup->getConnection()->addColumn(
            $tableName,
            'customer_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'default' => 0,
                'comment' => 'dustomer_id'
            ]
        );
        
        $setup->getConnection()->addColumn(
            $tableName,
            'customer_name',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'size' => 255,
                'nullable' => false,
                'default' => '',
                'comment' => 'customer_name'
            ]
        );
        
        $setup->getConnection()->addColumn(
            $tableName,
            'status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'size' => 255,
                'nullable' => false,
                'comment' => 'status'
            ]
        );
    }
}
