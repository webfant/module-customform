<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Setup\Operation;

use Webfant\Customform\Api\Data\AnswerInterface;

class AddResponseStatus
{
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function execute(\Magento\Framework\Setup\SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable('wf_customform_answer');
        $setup->getConnection()->addColumn(
            $tableName,
            AnswerInterface::ADMIN_RESPONSE_STATUS,
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'default' => 0,
                'comment' => 'Response Status',
            ]
        );
    }
}
