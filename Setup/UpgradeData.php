<?php
/**
 * @author Webfant Team
 */


namespace Webfant\Customform\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Webfant\Customform\Api\AnswerRepositoryInterface;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Webfant\Customform\Model\Config\Source\Status;
use Magento\Framework\DB\Ddl\Table;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var AnswerRepositoryInterface
     */
    private $answerRepository;

    /**
     * @var ConfigInterface
     */
    private $resourceConfig;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        AnswerRepositoryInterface $answerRepository,
        ConfigInterface $resourceConfig
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->answerRepository = $answerRepository;
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        if (version_compare($context->getVersion(), '1.7.0', '<')) {
            $this->upgradeResponseStatus($setup);
            $this->clearBookmarks($setup);
        }
         if (version_compare($context->getVersion(), '1.8.0', '<')) {
              $setup->startSetup();
            $this->upgradeResponseSumitButton($setup);
            $setup->endSetup();
            $this->clearBookmarks($setup);
            
        }
         if (version_compare($context->getVersion(), '1.9.0', '<')) {
            $setup->startSetup();
          $setup->endSetup();          
      }
    }
    // id_collection
    
    private function upgradeResponseSumitButton($setup){
        
        $table = $setup->getTable('wf_customform_form');
        $connection = $setup->getConnection();
     
        $connection->addColumn(
            $table,
             'submit_enabled',
                Table::TYPE_SMALLINT,
                null,
                ['default' => 0, 'nullable' => false],
                'Submit Button enabled/disabled'
        );
        
    }
 

    private function addNewTable($setup){

        $table = $setup->getConnection()
        ->newTable($setup->getTable('wf_customform_submit'))
        ->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )
        ->addColumn(
            'firstname',
            Table::TYPE_TEXT,
            200,
            ['nullable' => false, 'primary' => true],
            'firstname'
        )
        ->addColumn(
            'lastname',
            Table::TYPE_TEXT,
            200,
            ['nullable' => false, 'primary' => true],
            'lastname'
        )
        ->addColumn(
            'submit_json',
            Table::TYPE_TEXT,
                null,
                ['default' => '', 'nullable' => false],
                'Submit json'
        );

        $setup->getConnection()->createTable($table);
    }

    /**
     * @param $setup
     */
    private function upgradeResponseStatus($setup)
    {
        $answers = $this->answerRepository->getList();
        foreach ($answers as $answer) {
            if (!$answer->getAdminResponseEmail()) {
                $answer->setAdminResponseStatus(Status::PENDING);
            } else {
                $answer->setAdminResponseStatus(Status::ANSWERED);
            }
            $this->answerRepository->save($answer);
        }
    }

    /**
     * @param $setup
     */
    private function clearBookmarks($setup)
    {
        /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
        $connection = $this->resourceConfig->getConnection();

        $connection->delete(
            $this->resourceConfig->getTable('ui_bookmark'),
            'namespace = "webfant_customform_forms_listing"'
        );
    }
}
